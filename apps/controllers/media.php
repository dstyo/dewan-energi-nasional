<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class media extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('custom');
        $this->load->model('Model_data');
    }

    public function index()
    {
        redirect('welcome/error404');
    }

    public function download($name = '', $id = '', $cc)
    {
        $this->Model_data->edt_data('t_file', array('downloader' => $cc + 1), array('id_file' => $id));

        $path = './uploads/media_file/'.$name;
        $data = file_get_contents($path); // Read the file's contents
        force_download($name, $data);
    }

    public function foto($offset = '')
    {
        $url = base_url('media/foto');
        $num_link = 5;
        $table = 't_fotoalbum';
        $limit = 2;
        $kondisi = array('aktif' => 1);
        $konten = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, 'id_album desc');
        $totalrow = $this->Model_data->get_num_row($table, $kondisi);

        //create paging
        $this->custom->pagination_umum($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        //------
        $subjudul = 'Album Foto';

        $identitas = $this->Model_data->get_one_data('t_identitas', array());
        $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
        $sosmed = $this->Model_data->get_one_data('t_alamat', array());

        $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
        $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');

        $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
        $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
        $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 10, 'dibaca desc');

        $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
        $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

        /* Header */
        $head_konten = array(
            'title' => $subjudul,
            'identitas' => $identitas,
            'breadcrumb' => $breadcrumb,
        );

        /* Body */
        $body_konten = array(
            'subjudul' => $subjudul,
            'konten' => $konten,
            'paging' => $paging,
        );

        /* Side bar */
        $side_koneten = array(
            'banner' => $banner,
            'sosmed' => $sosmed,
            'berita_populer' => $berita_populer,
        );

        /* Footer */
        $foot_konten = array(
            'publikasi' => $publikasi,
            'foto' => $foto,
            'sosmed' => $sosmed,
            'banner' => $banner2,
        );

        $this->load->view('umum/header', $head_konten);
        $this->load->view('umum/media-foto', $body_konten);
        $this->load->view('umum/side-bar', $side_koneten);
        $this->load->view('umum/footer', $foot_konten);
    }

    public function album($kode = '')
    {
        $id = '';
        if ($kode != '') {
            $newkode = explode('-', $kode);
            $id = $newkode[0];
        }

        $kondisi = array('t_fotogaleri.id_album' => $id);
        $konten = $this->Model_data->get_join('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', $kondisi, 'id_gallery desc');

        $totalbum = $konten->num_rows();
        if ($totalbum < 1) {
            redirect('welcome/error404');
        }
        $rr = $konten->row();
        $subjudul = $rr->jdl_album;

        $identitas = $this->Model_data->get_one_data('t_identitas', array());
        $breadcrumb = array(anchor('', 'Beranda'), anchor('media/foto', 'Album'), $subjudul);
        $sosmed = $this->Model_data->get_one_data('t_alamat', array());
        $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 10, 'dibaca desc');
        $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
        $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
        $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
        $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');

        $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
        $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

        /* Header */
        $head_konten = array(
            'title' => $subjudul,
            'identitas' => $identitas,
            'breadcrumb' => $breadcrumb,
        );

        /* Body */
        $body_konten = array(
            'subjudul' => $subjudul,
            'konten' => $konten,
            'totalbum' => $totalbum,
        );

        /* Side bar */
        $side_koneten = array(
            'banner' => $banner,
            'sosmed' => $sosmed,
            'berita_populer' => $berita_populer,
        );

        /* Footer */
        $foot_konten = array(
            'publikasi' => $publikasi,
            'foto' => $foto,
            'sosmed' => $sosmed,
            'banner' => $banner2,
        );

        $this->load->view('umum/header', $head_konten);
        $this->load->view('umum/media-album', $body_konten);
        $this->load->view('umum/side-bar', $side_koneten);

        $this->load->view('umum/footer', $foot_konten);
    }

    public function video($offset = 0)
    {
        $url = base_url('media/video');
        $num_link = 5;
        $table = 't_video';
        $limit = 9;
        $kondisi = array();
        $konten = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, 'id_video desc');
        $totalrow = $this->Model_data->get_num_row($table, $kondisi);

        //create paging
        $this->custom->pagination_umum($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $subjudul = 'Media Video';

        $identitas = $this->Model_data->get_one_data('t_identitas', array());
        $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
        $sosmed = $this->Model_data->get_one_data('t_alamat', array());
        $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori',                array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 10, 'dibaca desc');
        $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
        $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
        $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
        $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');

        $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
        $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

        /* Header */
        $head_konten = array(
            'title' => $subjudul,
            'identitas' => $identitas,
            'breadcrumb' => $breadcrumb,
        );

        /* Body */
        $body_konten = array(
            'subjudul' => $subjudul,
            'konten' => $konten,
            'paging' => $paging,
        );
/* Side bar */
        $side_koneten = array(
            'banner' => $banner,
            'sosmed' => $sosmed,
            'berita_populer' => $berita_populer,
        );

        /* Footer */
        $foot_konten = array(
            'publikasi' => $publikasi,
            'foto' => $foto,
            'sosmed' => $sosmed,
            'banner' => $banner2,
        );

        $this->load->view('umum/header', $head_konten);
        $this->load->view('umum/media-video', $body_konten);
        $this->load->view('umum/side-bar', $side_koneten);

        $this->load->view('umum/footer', $foot_konten);
    }

    public function dokumen($offset = '')
    {
        $url = base_url('media/dokumen');
        $num_link = 5;
        $table = 't_file';
        $limit = 25;
        $kondisi = array();
        $konten = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, 'id_file desc');
        $totalrow = $this->Model_data->get_num_row($table, $kondisi);

        //create paging
        $this->custom->pagination_umum($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        //------
        $subjudul = 'Media File';

        $identitas = $this->Model_data->get_one_data('t_identitas', array());
        $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
        $sosmed = $this->Model_data->get_one_data('t_alamat', array());

        $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
        $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');

        $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
        $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
        $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 10, 'dibaca desc');

        $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
        $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

        /* Header */
        $head_konten = array(
            'title' => $subjudul,
            'identitas' => $identitas,
            'breadcrumb' => $breadcrumb,
        );

        /* Body */
        $body_konten = array(
            'subjudul' => $subjudul,
            'konten' => $konten,
            'paging' => $paging,
        );

        /* Side bar */
        $side_koneten = array(
            'banner' => $banner,
            'sosmed' => $sosmed,
            'berita_populer' => $berita_populer,
        );

        /* Footer */
        $foot_konten = array(
            'publikasi' => $publikasi,
            'foto' => $foto,
            'sosmed' => $sosmed,
            'banner' => $banner2,
        );

        $this->load->view('umum/header', $head_konten);
        $this->load->view('umum/media-file', $body_konten);
        $this->load->view('umum/side-bar', $side_koneten);
        $this->load->view('umum/footer', $foot_konten);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
