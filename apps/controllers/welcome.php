<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class welcome extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');

    // KONTER PENGUNJUNG
    $this->custom->konter();
  }

  public function index()
  {
    $session_datax = array('id_menu' => 19, 'parent' => 0);
    $this->session->set_userdata($session_datax);
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $slider = $this->Model_data->get_data('t_berita', array('slider' => 1), 'id_berita desc');
    $pengumuman = $this->Model_data->get_join_limit('*', 't_pengumuman', 't_k_pengumuman', 't_k_pengumuman.id_kategori=t_pengumuman.id_kategori', array('t_pengumuman.id_kategori' => 1, 't_pengumuman.status_pengumuman' => 1), 4, 'id_pengumuman desc');
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $berita_terkini = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 4, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $video = $this->Model_data->get_where_limit('t_video', array(), 4, 'RAND()');
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $breadcrumb = array('Beranda');
    $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
    $banner = $this->Model_data->get_data('t_banner', $konbaner, 'id_banner asc');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');
    $sikat = $this->Model_data->get_data('t_situs_terkait', array(), 'nama asc');

    // HEADER
    $head_konten = array(
      'title' => 'Beranda',
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // BODY
    $body_konten = array(
      'slider' => $slider,
      'pengumuman' => $pengumuman,
      'banner' => $banner,
      'video' => $video,
      'berita_terkini' => $berita_terkini,
      'sikat' => $sikat,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/home', $body_konten);
    $this->load->view('umum/footer', $foot_konten);
  }

  public function error404()
  {
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $breadcrumb = array(anchor('', 'Home'), '404');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

    // HEADER
    $head_konten = array(
      'title' => 'Home',
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/404');
    $this->load->view('umum/footer', $foot_konten);
  }

  public function set_menu()
  {
    $id_menu = $this->input->post('id');
    $parent = $this->input->post('parent');
    $session_datax = array('id_menu' => $id_menu, 'parent' => $parent);
    $this->session->set_userdata($session_datax);
    exit('ok');
  }
}
