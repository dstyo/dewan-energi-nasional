<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class organisasi extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');
  }

  public function index()
  {
    $subjudul = 'Struktur Organisasi DEN';
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
    $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
    $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 5, 'dibaca desc');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

    // HEADER
    $head_konten = array(
      'title' => $subjudul,
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // BODY
    $body_konten = array(
      'subjudul' => $subjudul,
    );

    // SIDEBAR
    $side_koneten = array(
      'banner' => $banner,
      'sosmed' => $sosmed,
      'berita_populer' => $berita_populer,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/organisasi', $body_konten);
    $this->load->view('umum/footer', $foot_konten);
  }

  public function detail_organisasi()
  {
    $id = $this->input->post('id');
    if ($id != '') {
      $q = $this->Model_data->get_join('*', 't_organisasi_anggota', 't_organisasi', 't_organisasi.id_organisasi=t_organisasi_anggota.id_organisasi', array('t_organisasi_anggota.id_organisasi' => $id), 'nama asc');
      if (count($q->result()) > 0) {
        $rr = $q->row();
        echo '<hr/>
              <h2 style="color: #10b0a6;">'.$rr->nama_organisasi.'</h2>
              <table>';
              foreach ($q->result() as $row) {
                echo '<tr>
                        <td style="padding: 5px;">'.$this->custom->img_show('uploads/foto_anggota_thumbnail/', $row->foto, 'style = "width:150px;border:1px solid #10b0a6; border-radius:3px;"').'</td>
                        <td style="vertical-align: top;padding: 5px;">
                        Nama: '.$row->nama.'
                        <br/>
                        '.$row->keterangan.'
                        </td>
                      </tr>';
              }
                echo '</table>';
      } else {
        echo 'no';
      }
    } else {
      echo 'no';
    }
  }
}
