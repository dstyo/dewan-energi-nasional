<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class agenda extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');
  }

  public function index($bulan = '', $tahun = '')
  {
    if ($bulan == '') {
      $bulan = date('m');
    } if ($tahun == '') {
      $tahun = date('Y');
    }

    $row = $this->Model_data->get_one_data('t_agenda', array());
    $subjudul = 'Agenda';
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $breadcrumb = array(anchor('', 'Home'), $subjudul);
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
    $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
    $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 10, 'dibaca desc');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

    // HEADER
    $head_konten = array(
      'title' => $subjudul,
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // BODY
    $body_konten = array(
      'subjudul' => $subjudul,
      'bulan' => $bulan,
      'tahun' => $tahun,
      'row' => $row,
    );

    // SIDEBAR
    $side_koneten = array(
      'banner' => $banner,
      'sosmed' => $sosmed,
      'berita_populer' => $berita_populer,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/agenda', $body_konten);
    $this->load->view('umum/side-bar', $side_koneten);
    $this->load->view('umum/footer', $foot_konten);
  }

  public function detail_agenda()
  {
    $id = $this->input->post('id');
    if ($id != '') {
      $row = $this->Model_data->get_one_data('t_agenda', array('id_agenda' => $id));
      if (count($row) > 0) {
        echo '<div class="widget">
                <div class="pd-20 kopa-entry-post">
                <article class="entry-item">
                <h4 class="entry-title" style="color:#10b0a6;">'.$row->tema.'</h4>
                  <div class="entry-meta">
                    <span class="entry-date"><i class="fa fa-calendar"></i> Tanggal : '.$this->custom->format_tgl_show($row->tgl_mulai).' - '.$this->custom->format_tgl_show($row->tgl_selesai).'</span>
                  </div>
                  <div class="entry-meta">
                    <span class="entry-date"><i class="fa fa-clock-o"></i> Jam : '.$row->jam.'</span>
                  </div>
                  <div class="entry-meta">
                    <span class="entry-date"><i class="fa fa-location-arrow"></i> Tempat : '.$row->tempat.'</span>
                  </div>
                  <div class="entry-meta">
                    <span class="entry-date"><i class="fa fa-user"></i> Pengirim : '.$row->pengirim.'</span>
                  </div>
                  <div style="margin-top:20px;">'.$row->isi_agenda.'</div>
                </article>
                </div>
              </div>';
      } else {
        echo 'no';
      }
    } else {
      echo 'no';
    }
  }
}
