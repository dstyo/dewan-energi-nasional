<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class statispage extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');
  }

  public function index($kode = '')
  {
    $id = '';
    if ($kode != '') {
      $newkode = explode('-', $kode);
      $id = $newkode[0];
    }
    $row = $this->Model_data->get_one_data('t_statispage', array('id_statispage' => $id, 'status' => 1));
    if (count($row) < 1) {
      redirect('welcome/error404');
    }

    $subjudul = $row->judul;
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
    $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
    $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 5, 'dibaca desc');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

    // HEADER
    $head_konten = array(
      'title' => $subjudul,
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // BODY
    $body_konten = array(
      'subjudul' => $subjudul,
      'konten' => $row,
    );

    // SIDEBAR
    $side_koneten = array(
      'banner' => $banner,
      'sosmed' => $sosmed,
      'berita_populer' => $berita_populer,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/statis-page', $body_konten);
    $this->load->view('umum/side-bar', $side_koneten);
    $this->load->view('umum/footer', $foot_konten);
  }
}
