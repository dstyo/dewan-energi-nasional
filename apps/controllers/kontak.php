<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class kontak extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');
  }

  public function index()
  {
    $subjudul = 'Kontak Kami';
    $identitas = $this->Model_data->get_one_data('t_identitas', array());
    $breadcrumb = array(anchor('', 'Beranda'), $subjudul);
    $sosmed = $this->Model_data->get_one_data('t_alamat', array());
    $publikasi = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 2, 't_berita.status_berita' => 1), 5, 'id_berita desc');
    $foto = $this->Model_data->get_join_limit('*', 't_fotogaleri', 't_fotoalbum', 't_fotoalbum.id_album=t_fotogaleri.id_album', array('t_fotoalbum.aktif' => 1), 7, 'RAND()');
    $konbaner = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'side');
    $banner = $this->Model_data->get_data('t_banner', $konbaner, 'RAND()');
    $berita_populer = $this->Model_data->get_join_limit('*', 't_berita', 't_kategori', 't_kategori.id_kategori=t_berita.id_kategori', array('t_berita.id_kategori' => 1, 't_berita.status_berita' => 1), 5, 'dibaca desc');
    $konbaner2 = array('tgl_mulai <=' => $this->custom->tanggal_ini(), 'tgl_selesai >=' => $this->custom->tanggal_ini(), 'posisi' => 'buttom');
    $banner2 = $this->Model_data->get_data('t_banner', $konbaner2, 'id_banner asc');

    // HEADER
    $head_konten = array(
      'title' => $subjudul,
      'identitas' => $identitas,
      'breadcrumb' => $breadcrumb,
    );

    // BODY
    $body_konten = array(
      'subjudul' => $subjudul,
      'sosmed' => $sosmed,
    );

    // SIDEBAR
    $side_koneten = array(
      'banner' => $banner,
      'sosmed' => $sosmed,
      'berita_populer' => $berita_populer,
    );

    // FOOTER
    $foot_konten = array(
      'publikasi' => $publikasi,
      'foto' => $foto,
      'sosmed' => $sosmed,
      'banner' => $banner2,
    );

    $this->load->view('umum/header', $head_konten);
    $this->load->view('umum/kontak', $body_konten);
    $this->load->view('umum/side-bar', $side_koneten);
    $this->load->view('umum/footer', $foot_konten);
  }

  public function proses()
  {
    $name = $this->input->post('contactName');
    $email = $this->input->post('email');
    $website = $this->input->post('contactSubject');
    $message = $this->input->post('comments');

    if ($name != '' && $email != '' && $message != '' && !preg_match('/^[a-zA-Z ]*$/', $email)) {
      $data = array(
        'nama' => $name,
        'email' => $email,
        'subjek' => $website,
        'pesan' => $message,
        'tanggal' => date('Y-m-d'),
        'jam' => date('H:i'),
      );
      $this->Model_data->ins_data('t_hubungi', $data, false);
      exit('ok');
    } else {
      exit('gagal');
      }
  }
}
