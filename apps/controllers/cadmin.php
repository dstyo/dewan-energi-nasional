<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/**
 *
 */
class cadmin extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('custom');
    $this->load->model('Model_data');
    $this->clear_cache();
    session_start();
  }

  public function clear_cache()
  {
    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0');
    $this->output->set_header('Pragma: no-cache');
  }

  // REDIRECT FUNCTION FOR THIS CLASS
  public function return_uri($obj)
  {
    redirect(cadmin.'/'.$obj, 'refresh');
  }

  // DEFAULT PAGE
  public function index()
  {
    $this->return_uri('home');
  }

  // LOGOUT
  public function logout()
  {
    $this->session->sess_destroy();
    session_destroy();
    $this->return_uri('login');
  }

  // CHECK LOG_IN
  public function check_logged_in()
  {
    if ($this->session->userdata('is_login') != true) {
      $this->return_uri('logout');
    }
  }

  // LOG_IN PAGE
  public function login($msg = '')
  {
    $this->form_validation->set_error_delimiters('', '<br/>');
    $this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
    $this->form_validation->set_message('required', '%s harus diisi.');

    if ($this->form_validation->run()) {
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      if ($username == 'mimin' && $password == 'm1m1n') {
        $kondisi = array('level' => 'A');
      } else {
        $kondisi = array('username' => $username, 'password' => md5($password), 'blokir' => 'N');
      }

      $row = $this->Model_data->get_one_array('t_users', $kondisi);

      if ($row) {
        $_SESSION['KCFINDER'] = array();
        $_SESSION['KCFINDER']['disabled'] = false;
        //$_SESSION['KCFINDER']['uploadURL'] = base_url('static/tinymce/dir');

        $session_data = array(
          'id_user' => $row['id_user'],
          'nama_lengkap' => $row['nama_lengkap'],
          'username' => $row['username'],
          'email' => $row['email'],
          'no_telp' => $row['no_telp'],
          'level' => $row['level'],
          'is_login' => true,
        );
        $this->session->set_userdata($session_data);
        $this->return_uri('home');
      } else {
        $this->return_uri('login/invalid');
      }
    } else {
      $konten = array('msg' => $msg);
      $this->load->view('cadmin/login', $konten);
    }
  }


  // START FULL STRUKTUR ADMIN

  // HOMEPAGE
  public function home()
  {
    $this->check_logged_in();

    $tot_berita = $this->Model_data->get_num_row('t_berita', array());
    $tot_page = $this->Model_data->get_num_row('t_statispage', array());
    $tot_agenda = $this->Model_data->get_num_row('t_agenda', array());
    $tot_hubungi = $this->Model_data->get_num_row('t_hubungi', array());
    $tot_foto = $this->Model_data->get_num_row('t_fotogaleri', array());
    $tot_video = $this->Model_data->get_num_row('t_video', array());
    $tot_file = $this->Model_data->get_num_row('t_file', array());
    $tot_user = $this->Model_data->get_num_row('t_users', array());

    $konten = array(
      'tot_berita' => $tot_berita,
      'tot_page' => $tot_page,
      'tot_agenda' => $tot_agenda,
      'tot_hubungi' => $tot_hubungi,
      'tot_foto' => $tot_foto,
      'tot_video' => $tot_video,
      'tot_file' => $tot_file,
      'tot_user' => $tot_user,
    );

    $this->load->view('cadmin/header');
    $this->load->view('cadmin/left-menu');
    $this->load->view('cadmin/home', $konten);
    $this->load->view('cadmin/footer');
  }

  // START KATEGORI BERITA
  // KATEGORI PAGE BERITA
  public function kategori_table($offset = 0)
  {
    $this->check_logged_in();

    $table = 't_kategori';
    $order = 'id_kategori desc, nama_kategori asc';
    $url = base_url(cadmin.'/kategori_table');
    $num_link = 5;

    if (isset($_POST['q'])) {
      $q = $this->input->post('q');
      $limit = $this->input->post('limit');
      $this->session->set_userdata(array('cari_kategori' => $q, 'limit_kategori' => $limit));
    } else {
      $q = $this->session->userdata('cari_kategori');
      $limit = $this->session->userdata('limit_kategori');
      if ($limit == '') {
        $limit = 10;
      }
    } if ($q == '') {
      $kondisi = array();
      $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
      $totalrow = $this->Model_data->get_num_row($table, $kondisi);
    } else {
      $kondisi = array('nama_kategori' => $q);
      $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
      $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
    }

    // CREATE PAGING
    $this->custom->pagination($url, $totalrow, $limit, $num_link);
    $paging = $this->pagination->create_links();
    $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

    $this->load->view('cadmin/header');
    $this->load->view('cadmin/left-menu');
    $this->load->view('cadmin/kategori-table', $konten);
    $this->load->view('cadmin/footer');
  }

  // KATEGORI FORM BERITA
  public function kategori_form($id = 'n', $msg = '')
  {
    $this->check_logged_in();

    $table = 't_kategori';
    $kondisi = array('id_kategori' => $id);
    $res = $this->Model_data->get_one_data($table, $kondisi);

    if (count($res) > 0) {
      $edited = true;
      $subjudul = 'Form Edit Kategori';
    } else {
      $edited = false;
      $subjudul = 'Form Tambah Kategori';
    }

    $this->form_validation->set_error_delimiters('', '<br/>');
    $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'required|xss_clean');
    $this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
    $this->form_validation->set_message('required', '%s harus diisi.');

    if ($this->form_validation->run()) {
      $nama_kategori = $this->input->post('nama_kategori');
      $status = $this->input->post('status');
      $kategori_seo = $this->custom->seo_title($nama_kategori);
      $data = array('nama_kategori' => $nama_kategori, 'kategori_seo' => $kategori_seo, 'status' => $status);
      $feedback = $id.'/error';

      if ($edited) {
        // EDIT DATA
        if ($this->Model_data->edt_data($table, $data, $kondisi)) {
          $feedback = $id.'/success';
        }
      } else {
        // INSERT DATA
        if ($this->Model_data->ins_data($table, $data, false)) {
          $feedback = $id.'/success';
        }
      }
      $this->return_uri('kategori_form/'.$feedback);
    } else {
      $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/kategori-form', $konten);
      $this->load->view('cadmin/footer');
    }
  }
  // END KATEGORI BERITA

  // START BERITA
  // BERITA PAGE
  public function berita_table($offset = 0)
  {
    $this->check_logged_in();

    $table = 't_berita';
    $table2 = 't_kategori';
    $relasi = 't_kategori.id_kategori=t_berita.id_kategori';
    $order = 'id_berita desc, judul asc';
    $url = base_url(cadmin.'/berita_table');
    $num_link = 5;

    if (isset($_POST['q'])) {
      $q = $this->input->post('q');
      $limit = $this->input->post('limit');
      $this->session->set_userdata(array('cari_berita' => $q, 'limit_berita' => $limit));
    } else {
      $q = $this->session->userdata('cari_berita');
      $limit = $this->session->userdata('limit_berita');
      if ($limit == '') {
        $limit = 10;
      }
    } if ($q == '') {
      $kondisi = array();
      $res = $this->Model_data->get_pagination_join($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
      $totalrow = $this->Model_data->get_num_row_join($table, $table2, $relasi, $kondisi);
    } else {
      $kondisi = array('judul' => $q);
      $res = $this->Model_data->get_pagination_join_like($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
      $totalrow = $this->Model_data->get_num_row_join_like($table, $table2, $relasi, $kondisi);
    }

    // CREATE PAGING
    $this->custom->pagination($url, $totalrow, $limit, $num_link);
    $paging = $this->pagination->create_links();
    $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

    $this->load->view('cadmin/header');
    $this->load->view('cadmin/left-menu');
    $this->load->view('cadmin/berita-table', $konten);
    $this->load->view('cadmin/footer');
  }

  // DELETE BERITA
  public function berita_delete($id = '')
  {
    $this->check_logged_in();

    $table = 't_berita';
    $kondisi = array('id_berita' => $id);
    $r = $this->Model_data->get_one_data($table, $kondisi);

    if (count($r) > 0) {
      if ($r->gambar != '') {
        unlink('./uploads/berita_gambar_original/'.$r->gambar);
        unlink('./uploads/berita_gambar_thumbnail/'.$r->gambar);
      }
    }

    $this->Model_data->del_data($table, $kondisi);
    $this->return_uri('berita_table');
  }

  // BERITA FORM
  public function berita_form($id = 'n', $msg = '')
  {
    $this->check_logged_in();

    $table = 't_berita';
    $kondisi = array('id_berita' => $id);
    $res = $this->Model_data->get_one_data($table, $kondisi);

    if (count($res) > 0) {
      $edited = true;
      $subjudul = 'Form Edit Halaman';
    } else {
      $edited = false;
      $subjudul = 'Form Tambah Halaman';
    }

    $this->form_validation->set_error_delimiters('', '<br/>');
    $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
    $this->form_validation->set_rules('id_kategori', 'Kategori', 'required|xss_clean');
    $this->form_validation->set_rules('isi_berita', 'Isi berita', 'required|xss_clean');
    $this->form_validation->set_message('required', '%s harus diisi.');

    if ($this->form_validation->run()) {
      $judul = $this->input->post('judul');
      $id_kategori = $this->input->post('id_kategori');
      $isi_berita = $this->input->post('isi_berita');
      $headline = $this->input->post('headline');
      $tag = $this->input->post('tag');
      $gambar_old = $this->input->post('gambar_old');
      $judul_seo = $this->custom->seo_title($judul);
      $hari = $this->custom->hari_ini();
      $tanggal = $this->custom->tanggal_ini();
      $jam = $this->custom->jam_ini();
      $id_user = $this->session->userdata('id_user');
      $status = $this->input->post('status');
      $komen = $this->input->post('komen');
      $runningtext = $this->input->post('runningtext');
      $slider = $this->input->post('slider');
      $document = $this->input->post('document');

      $isi_berita0 = str_replace('src="../../../', 'src="'.base_url(), $isi_berita);
      $isi_berita1 = str_replace('src="../../', 'src="'.base_url(), $isi_berita0);
      $isi_berita2 = str_replace('src="../', 'src="'.base_url(), $isi_berita1);

      $fixtag = '';
      if ($tag != '') {
        foreach ($tag as $newtag) {
          $fixtag .= $newtag.' ';
        }
      }

      $data = array(
        'judul' => $judul,
        'id_kategori' => $id_kategori,
        'isi_berita' => $isi_berita2,
        'headline' => $headline,
        'tag' => $fixtag,
        'judul_seo' => $judul_seo,
        'hari' => $hari,
        'tanggal' => $tanggal,
        'jam' => $jam,
        'id_user' => $id_user,
        'komen' => $komen,
        'runningtext' => $runningtext,
        'slider' => $slider,
        'document' => $document,
      );

      $low_ext = 'gif|jpg|png';
      $max_size = '2048';
      $thum_width = 150;
      $thum_height = 150;
      $resize = true;
      $ori_path = 'berita_gambar_original';
      $tum_path = 'berita_gambar_thumbnail';

      $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

      $gambar = '';
      if ($unggah['kon']) {
        $gambar = $unggah['msg']['file_name'];
      }
      $feedback = $id.'/error';
      if ($edited) {
        // EDIT DATA
        if ($this->session->userdata('level') != 'D') {
          $data['id_editor'] = $id_user;
          $data['status_berita'] = $status;
        } else {
          $data['status_berita'] = 0;
        } if ($gambar != '') {
          $data['gambar'] = $gambar;
          unlink('./uploads/'.$ori_path.'/'.$gambar_old);
          unlink('./uploads/'.$tum_path.'/'.$gambar_old);
        } if ($this->Model_data->edt_data($table, $data, $kondisi)) {
          $feedback = $id.'/success';
        }
      } else {
        // INSERT DATA
        $data['id_drafter'] = $id_user;
        $data['gambar'] = $gambar;
        if ($this->Model_data->ins_data($table, $data, false)) {
          $feedback = $id.'/success';
        }
      }
      $this->return_uri('berita_form/'.$feedback);
    } else {
      $kategori = $this->Model_data->get_data('t_kategori', array(), 'nama_kategori asc');
      $tag_label = $this->Model_data->get_data('t_tag', array(), 'nama_tag asc');
      $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul, 'kategori' => $kategori, 'tag_label' => $tag_label,);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/berita-form', $konten);
      $this->load->view('cadmin/footer');
    }
  }
  // END BERITA

  // START KATEGORI PENGUMUMAN
  public function kategori_pengumuman($offset = 0)
  {
      $this->check_logged_in();

      $table = 't_k_pengumuman';
      $order = 'id_kategori desc, nama_kategori asc';
      $url = base_url(cadmin.'/kategori_pengumuman');
      $num_link = 5;

      if (isset($_POST['q'])) {
          $q = $this->input->post('q');
          $limit = $this->input->post('limit');
          $this->session->set_userdata(array('cari_kategori' => $q, 'limit_kategori' => $limit));
      } else {
          $q = $this->session->userdata('cari_kategori');
          $limit = $this->session->userdata('limit_kategori');
          if ($limit == '') {
              $limit = 10;
          }
      }

      if ($q == '') {
          $kondisi = array();
          $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row($table, $kondisi);
      } else {
          $kondisi = array('nama_kategori' => $q);
          $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
      }

      //create paging
      $this->custom->pagination($url, $totalrow, $limit, $num_link);
      $paging = $this->pagination->create_links();

      $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/kategori-pengumuman', $konten);
      $this->load->view('cadmin/footer');
  }

  // ADD AND EDIT KATEGORI PENGUMUMAN
  public function kategori_pengumuman_form($id = 'n', $msg = '')
  {
      $this->check_logged_in();

      $table = 't_k_pengumuman';
      $kondisi = array('id_kategori' => $id);

      $res = $this->Model_data->get_one_data($table, $kondisi);
      if (count($res) > 0) {
          $edited = true;
          $subjudul = 'Form Edit Kategori';
      } else {
          $edited = false;
          $subjudul = 'Form Tambah Kategori';
      }

      $this->form_validation->set_error_delimiters('', '<br/>');
      $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'required|xss_clean');
      $this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
      $this->form_validation->set_message('required', '%s harus diisi.');

      if ($this->form_validation->run()) {
          $nama_kategori = $this->input->post('nama_kategori');
          $status = $this->input->post('status');
          $kategori_seo = $this->custom->seo_title($nama_kategori);

          $data = array('nama_kategori' => $nama_kategori, 'kategori_seo' => $kategori_seo, 'status' => $status);

          $feedback = $id.'/error';
          if ($edited) {
              //edit data
              if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                  $feedback = $id.'/success';
              }
          } else {
              //insert data
              if ($this->Model_data->ins_data($table, $data, false)) {
                  $feedback = $id.'/success';
              }
          }

          $this->return_uri('kategori_pengumuman_form/'.$feedback);
      } else {
          $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

          $this->load->view('cadmin/header');
          $this->load->view('cadmin/left-menu');
          $this->load->view('cadmin/kategori-pengumuman-form', $konten);
          $this->load->view('cadmin/footer');
      }
  }
  // END KATEGORI PENGUMUMAN

  // START PENGUMUMAN
  // PENGUMUMAN
  public function pengumuman_table($offset = 0)
  {
      $this->check_logged_in();

      $table = 't_pengumuman';
      $table2 = 't_k_pengumuman';
      $relasi = 't_k_pengumuman.id_kategori=t_pengumuman.id_kategori';
      $order = 'id_pengumuman desc, judul asc';
      $url = base_url(cadmin.'/pengumuman_table');
      $num_link = 5;

      if (isset($_POST['q'])) {
          $q = $this->input->post('q');
          $limit = $this->input->post('limit');
          $this->session->set_userdata(array('cari_pengumuman' => $q, 'limit_pengumuman' => $limit));
      } else {
          $q = $this->session->userdata('cari_pengumuman');
          $limit = $this->session->userdata('limit_pengumuman');
          if ($limit == '') {
              $limit = 10;
          }
      }

      if ($q == '') {
          $kondisi = array();
          $res = $this->Model_data->get_pagination_join($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row_join($table, $table2, $relasi, $kondisi);
      } else {
          $kondisi = array('judul' => $q);
          $res = $this->Model_data->get_pagination_join_like($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row_join_like($table, $table2, $relasi, $kondisi);
      }

      //create paging
      $this->custom->pagination($url, $totalrow, $limit, $num_link);
      $paging = $this->pagination->create_links();

      $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/pengumuman-table', $konten);
      $this->load->view('cadmin/footer');
  }

  // PENGUMUMAN ADD AND EDIT
  public function pengumuman_form($id = 'n', $msg = '')
  {
      $this->check_logged_in();

      $table = 't_pengumuman';
      $kondisi = array('id_pengumuman' => $id);

      $res = $this->Model_data->get_one_data($table, $kondisi);
      if (count($res) > 0) {
          $edited = true;
          $subjudul = 'Form Edit Pengumuman';
      } else {
          $edited = false;
          $subjudul = 'Form Tambah Pengumuman';
      }

      $this->form_validation->set_error_delimiters('', '<br/>');
      $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
      $this->form_validation->set_rules('id_kategori', 'Kategori', 'required|xss_clean');
      $this->form_validation->set_rules('isi_pengumuman', 'Isi pengumuman', 'required|xss_clean');
      $this->form_validation->set_message('required', '%s harus diisi.');

      if ($this->form_validation->run()) {
          $judul = $this->input->post('judul');
          $id_kategori = $this->input->post('id_kategori');
          $isi_pengumuman = $this->input->post('isi_pengumuman');
          $headline = $this->input->post('headline');
          $tag = $this->input->post('tag');
          $gambar_old = $this->input->post('gambar_old');

          $judul_seo = $this->custom->seo_title($judul);
          $hari = $this->custom->hari_ini();
          $tanggal = $this->custom->tanggal_ini();
          $jam = $this->custom->jam_ini();

          $id_user = $this->session->userdata('id_user');

          $status_pengumuman = $this->input->post('status_pengumuman');
          $komen = $this->input->post('komen');
          $runningtext = $this->input->post('runningtext');
          $slider = $this->input->post('slider');


          $isi_pengumuman0 = str_replace('src="../../../', 'src="'.base_url(), $isi_pengumuman);
          $isi_pengumuman1 = str_replace('src="../../', 'src="'.base_url(), $isi_pengumuman0);
          $isi_pengumuman2 = str_replace('src="../', 'src="'.base_url(), $isi_pengumuman1);

          $fixtag = '';
          if ($tag != '') {
              foreach ($tag as $newtag) {
                  $fixtag .= $newtag.' ';
              }
          }

          $data = array(
              'judul' => $judul,
              'id_kategori' => $id_kategori,
              'isi_pengumuman' => $isi_pengumuman2,
              'headline' => $headline,
              'tag' => $fixtag,
              'judul_seo' => $judul_seo,
              'hari' => $hari,
              'tanggal' => $tanggal,
              'jam' => $jam,
              'id_user' => $id_user,
              'komen' => $komen,
              'runningtext' => $runningtext,
              'slider'=>$slider,
          );

          $low_ext = 'gif|jpg|png';
          $max_size = '2048';
          $thum_width = 150;
          $thum_height = 150;
          $resize = true;
          $ori_path = 'pengumuman_gambar_original';
          $tum_path = 'pengumuman_gambar_thumbnail';

          $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

          $gambar = '';
          if ($unggah['kon']) {
              $gambar = $unggah['msg']['file_name'];
          }

          $feedback = $id.'/error';
          if ($edited) {
              //edit data
              if ($this->session->userdata('level') != 'D') {
                  $data['id_editor'] = $id_user;
                  $data['status_pengumuman'] = $status_pengumuman;
              } else {
                  $data['status_pengumuman'] = 0;
              }

              if ($gambar != '') {
                  $data['gambar'] = $gambar;
                  unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                  unlink('./uploads/'.$tum_path.'/'.$gambar_old);
              }
              if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                  $feedback = $id.'/success';
              }
          } else {
              //insert data
              $data['id_drafter'] = $id_user;
              $data['gambar'] = $gambar;
              if ($this->Model_data->ins_data($table, $data, false)) {
                  $feedback = $id.'/success';
              }
          }

          $this->return_uri('pengumuman_form/'.$feedback);
      } else {
          $kategori = $this->Model_data->get_data('t_k_pengumuman', array(), 'nama_kategori asc');
          $tag_label = $this->Model_data->get_data('t_tag', array(), 'nama_tag asc');
          $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul,
              'kategori' => $kategori, 'tag_label' => $tag_label,
          );

          $this->load->view('cadmin/header');
          $this->load->view('cadmin/left-menu');
          $this->load->view('cadmin/pengumuman-form', $konten);
          $this->load->view('cadmin/footer');
      }
  }

  // HAPUS PENGUMUMAN
  public function delete_pengumuman($id = '')
  {
      $this->check_logged_in();

      $table = 't_pengumuman';
      $kondisi = array('id_pengumuman' => $id);

      $r = $this->Model_data->get_one_data($table, $kondisi);
      if (count($r) > 0) {
          if ($r->gambar != '') {
              unlink('./uploads/pengumuman_gambar_original/'.$r->gambar);
              unlink('./uploads/pengumuman_gambar_thumbnail/'.$r->gambar);
          }
      }

      $this->Model_data->del_data($table, $kondisi);
      $this->return_uri('pengumuman_table');
  }
  // END PENGUMUMAN

  // START KATEGORI PUBLIKASI
  // KATEGORI PUBLIKASI
  public function kategori_publikasi($offset = 0)
  {
      $this->check_logged_in();

      $table = 'publikasi_kategori';
      $order = 'id_publikasi_kategori desc, publikasi_kategori asc';
      $url = base_url(cadmin.'/kategori_publikasi');
      $num_link = 5;

      if (isset($_POST['q'])) {
          $q = $this->input->post('q');
          $limit = $this->input->post('limit');
          $this->session->set_userdata(array('cari_kategori' => $q, 'limit_kategori' => $limit));
      } else {
          $q = $this->session->userdata('cari_kategori');
          $limit = $this->session->userdata('limit_kategori');
          if ($limit == '') {
              $limit = 10;
          }
      }

      if ($q == '') {
          $kondisi = array();
          $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row($table, $kondisi);
      } else {
          $kondisi = array('publikasi_kategori' => $q);
          $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
          $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
      }

      // CREATE PAGING
      $this->custom->pagination($url, $totalrow, $limit, $num_link);
      $paging = $this->pagination->create_links();

      $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/publikasi-kategori-table', $konten);
      $this->load->view('cadmin/footer');
  }

  // KATEGORI FORM PUBLIKASI
  public function kategori_publikasi_form($id = 'n', $msg = '')
  {
    $this->check_logged_in();

    $table = 'publikasi_kategori';
    $kondisi = array('id_publikasi_kategori' => $id);
    $res = $this->Model_data->get_one_data($table, $kondisi);

    if (count($res) > 0) {
      $edited = true;
      $subjudul = 'Form Edit Kategori';
    } else {
      $edited = false;
      $subjudul = 'Form Tambah Kategori';
    }

    $this->form_validation->set_error_delimiters('', '<br/>');
    $this->form_validation->set_rules('publikasi_kategori', 'Nama kategori', 'required|xss_clean');
    $this->form_validation->set_message('required', '%s harus diisi.');

    if ($this->form_validation->run()) {
      $publikasi_kategori = $this->input->post('publikasi_kategori');
      $data = array('publikasi_kategori' => $publikasi_kategori);
      $feedback = $id.'/error';

      if ($edited) {
        // EDIT DATA
        if ($this->Model_data->edt_data($table, $data, $kondisi)) {
          $feedback = $id.'/success';
        }
      } else {
        // INSERT DATA
        if ($this->Model_data->ins_data($table, $data, false)) {
          $feedback = $id.'/success';
        }
      }
      $this->return_uri('kategori_publikasi_form/'.$feedback);
    } else {
      $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

      $this->load->view('cadmin/header');
      $this->load->view('cadmin/left-menu');
      $this->load->view('cadmin/publikasi-kategori-form', $konten);
      $this->load->view('cadmin/footer');
    }
  }
  // END KATEGORI PUBLIKASI

    /* Tag - Label Page */
    public function tag_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_tag';
        $order = 'id_tag desc, nama_tag asc';
        $url = base_url(cadmin.'/tag_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_tag' => $q, 'limit_tag' => $limit));
        } else {
            $q = $this->session->userdata('cari_tag');
            $limit = $this->session->userdata('limit_tag');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama_tag' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/tag-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Tag Delete */
    public function tag_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_tag';
        $kondisi = array('id_tag' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('tag_table');
    }

    /* Tag Form Add/Edit Page */
    public function tag_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_tag';
        $kondisi = array('id_tag' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Tag/Label';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Tag/Label';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_tag', 'Nama Tag/Label', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_tag = $this->input->post('nama_tag');
            $tag_seo = $this->custom->seo_title($nama_tag);

            $data = array('nama_tag' => $nama_tag, 'tag_seo' => $tag_seo);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('tag_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/tag-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Statis Page */
    public function statis_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_statispage';
        $order = 'id_statispage desc, judul asc';
        $url = base_url(cadmin.'/statis_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_statis' => $q, 'limit_statis' => $limit));
        } else {
            $q = $this->session->userdata('cari_statis');
            $limit = $this->session->userdata('limit_statis');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('judul' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/statis-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Statis Delete */
    public function statis_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_statispage';
        $kondisi = array('id_statispage' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->gambar != '') {
                unlink('./uploads/page_gambar_original/'.$r->gambar);
                unlink('./uploads/page_gambar_thumbnail/'.$r->gambar);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('statis_table');
    }

    /* Statis Form Add/Edit Page */
    public function statis_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_statispage';
        $kondisi = array('id_statispage' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Halaman Statis';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Halaman Statis';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
        $this->form_validation->set_rules('isi_halaman', 'Isi Halaman', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $judul = $this->input->post('judul');
            $isi_halaman = $this->input->post('isi_halaman');
            $gambar_old = $this->input->post('gambar_old');

            $judul_seo = $this->custom->seo_title($judul);
            $tanggal = $this->custom->tanggal_ini();

            $id_user = $this->session->userdata('id_user');

            $status = $this->input->post('status');
            $komen = $this->input->post('komen');

            $isi_halaman0 = str_replace('src="../../../', 'src="'.base_url(), $isi_halaman);
            $isi_halaman1 = str_replace('src="../../', 'src="'.base_url(), $isi_halaman0);
            $isi_halaman2 = str_replace('src="../', 'src="'.base_url(), $isi_halaman1);

            $data = array(
                'judul' => $judul,
                'isi_halaman' => $isi_halaman2,
                'judul_seo' => $judul_seo,
                'tanggal' => $tanggal,
                'komen' => $komen,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 150;
            $thum_height = 150;
            $resize = true;
            $ori_path = 'page_gambar_original';
            $tum_path = 'page_gambar_thumbnail';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            }

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->session->userdata('level') != 'D') {
                    $data['id_editor'] = $id_user;
                    $data['status'] = $status;
                } else {
                    $data['status'] = 0;
                }

                if ($gambar != '') {
                    $data['gambar'] = $gambar;
                    unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                    unlink('./uploads/'.$tum_path.'/'.$gambar_old);
                }
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                $data['gambar'] = $gambar;
                $data['id_drafter'] = $id_user;
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('statis_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/statis-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Identitas Page */
    public function identitas($msg = '')
    {
        $this->check_logged_in();

        $id = 1;
        $table = 't_identitas';
        $kondisi = array('id_identitas' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        $subjudul = 'Profil Website';

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_website', 'Nama website', 'required|xss_clean');
        $this->form_validation->set_rules('meta_deskripsi', 'Meta deskripsi', 'required|xss_clean');
        $this->form_validation->set_rules('meta_keyword', 'Meta keyword', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_website = $this->input->post('nama_website');
            $meta_deskripsi = $this->input->post('meta_deskripsi');
            $meta_keyword = $this->input->post('meta_keyword');
            $favicon_old = $this->input->post('favicon_old');

            $data = array('nama_website' => $nama_website, 'meta_deskripsi' => $meta_deskripsi, 'meta_keyword' => $meta_keyword);

            $low_ext = 'png';
            $max_size = '2048';
            $thum_width = 32;
            $thum_height = 32;
            $resize = true;
            $ori_path = 'favicon/';
            $tum_path = 'favicon/favicon.png';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $feedback = 'success';

            if ($unggah['kon']) {
                $favicon = $unggah['msg']['file_name'];
                $data['favicon'] = 'favicon.png';
                unlink('./uploads/'.$ori_path.'/'.$favicon);
            } else {
                if ($unggah['msg'] != 'You did not select a file to upload.') {
                    $feedback = $unggah['msg'];
                }
            }

            //edit data
            if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                $this->return_uri('identitas/'.$feedback);
            }
        } else {
            $konten = array('msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/identitas', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Alamat dan Sosmed */
    public function alamat_table($msg = '')
    {
        $this->check_logged_in();

        $id = 1;
        $table = 't_alamat';
        $kondisi = array('id_alamat' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        $subjudul = 'Alamat & Sosial Media';

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('simpan', 'Simpan', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $alamat = $this->input->post('alamat');
            $facebook = $this->input->post('facebook');
            $twitter = $this->input->post('twitter');
            $googleplus = $this->input->post('googleplus');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $fax = $this->input->post('fax');

            $data = array('alamat' => $alamat, 'email' => $email, 'phone' => $phone, 'fax' => $fax, 'facebook' => $facebook, 'twitter' => $twitter, 'googleplus' => $googleplus);

            $feedback = 'success';

            //edit data
            if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                $this->return_uri('alamat_table/'.$feedback);
            }
        } else {
            $konten = array('msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/alamat-table', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Agenda Page */
    public function agenda_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_agenda';
        $order = 'id_agenda desc, tema asc';
        $url = base_url(cadmin.'/agenda_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_genda' => $q, 'limit_agenda' => $limit));
        } else {
            $q = $this->session->userdata('cari_genda');
            $limit = $this->session->userdata('limit_agenda');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('tema' => $q, 'pengirim' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/agenda-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Agenda Delete */
    public function agenda_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_agenda';
        $kondisi = array('id_agenda' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('agenda_table');
    }

    /* Agenda Form Add/Edit Page */
    public function agenda_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_agenda';
        $kondisi = array('id_agenda' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Agenda';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Agenda';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('tema', 'Tema', 'required|xss_clean');
        $this->form_validation->set_rules('isi_agenda', 'Isi agenda', 'required|xss_clean');
        $this->form_validation->set_rules('tempat', 'Tempat', 'required|xss_clean');
        $this->form_validation->set_rules('jam', 'Jam', 'required|xss_clean');
        $this->form_validation->set_rules('pengirim', 'Pengirim', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $tema = $this->input->post('tema');
            $tema_seo = $this->custom->seo_title($tema);
            $isi_agenda = $this->input->post('isi_agenda');
            $tempat = $this->input->post('tempat');
            $jam = $this->input->post('jam');
            $pengirim = $this->input->post('pengirim');
            $runningtext = $this->input->post('runningtext');

            $tanggal = $this->input->post('tanggal');
            $tgl = explode(' - ', $tanggal);
            $tgl_mulai = $this->custom->format_tgl($tgl[0]);
            $tgl_selesai = $this->custom->format_tgl($tgl[1]);

            $tgl_posting = $this->custom->tanggal_ini();
            $id_user = $this->session->userdata('id_user');

            $isi_agenda0 = str_replace('src="../../../', 'src="'.base_url(), $isi_agenda);
            $isi_agenda1 = str_replace('src="../../', 'src="'.base_url(), $isi_agenda0);
            $isi_agenda2 = str_replace('src="../', 'src="'.base_url(), $isi_agenda1);

            $data = array('tema' => $tema, 'tema_seo' => $tema_seo, 'isi_agenda' => $isi_agenda2, 'tempat' => $tempat,
                'jam' => $jam, 'pengirim' => $pengirim, 'tgl_mulai' => $tgl_mulai, 'tgl_selesai' => $tgl_selesai,
                'tgl_posting' => $tgl_posting, 'id_user' => $id_user, 'runningtext' => $runningtext,
            );

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('agenda_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/agenda-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Hubungi Page */
    public function hubungi_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_hubungi';
        $order = 'id_hubungi desc, tanggal desc';
        $url = base_url(cadmin.'/hubungi');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_hubungi' => $q, 'limit_hubungi' => $limit));
        } else {
            $q = $this->session->userdata('cari_hubungi');
            $limit = $this->session->userdata('limit_hubungi');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama' => $q, 'email' => $q, 'subjek' => $q, 'tanggal' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/hubungi-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Hubungi Delete */
    public function hubungi_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_hubungi';
        $kondisi = array('id_hubungi' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('hubungi_table');
    }

    /* Hubungi Form */
    public function hubungi_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_hubungi';
        $kondisi = array('id_hubungi' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $subjudul = 'Form Balas Pesan';
            $this->Model_data->edt_data($table, array('status' => 1), $kondisi);
        } else {
            $this->return_uri('hubungi_table');
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('kepada', 'Kepada', 'required|xss_clean');
        $this->form_validation->set_rules('subjek', 'Subjek', 'required|xss_clean');
        $this->form_validation->set_rules('pesan', 'Pesan', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $kepada = $this->input->post('kepada');
            $subjek = $this->input->post('subjek');
            $pesan = $this->input->post('pesan');

            $feedback = $id.'/error';

            $this->load->library('email');

            $config = array(
                  'mailtype' => 'html',
                  'charset' => 'utf-8',
                  'priority' => '1',
                  'wordwrap' => true,
                   );
            $this->email->initialize($config);

            $this->email->from($this->session->userdata('email'), $this->session->userdata('nama_lengkap'));
            $this->email->to($kepada);

            $this->email->subject($subjek);
            $this->email->message($pesan);

            $send = $this->email->send();

            if ($send) {
                $feedback = $id.'/success';
                $this->Model_data->edt_data($table, array('status' => 2, 'pesan' => $pesan), $kondisi);
            }

            return $this->return_uri('hubungi_form/'.$feedback);
        } else {
            $konten = array('id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/hubungi-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Album Foto Page */
    public function album_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_fotoalbum';
        $order = 'id_album desc, jdl_album asc';
        $url = base_url(cadmin.'/album_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_album' => $q, 'limit_album' => $limit));
        } else {
            $q = $this->session->userdata('cari_album');
            $limit = $this->session->userdata('limit_album');
            if ($limit == '') {
                $limit = 8;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('jdl_album' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/album-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Album Foto Form */
    public function album_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_fotoalbum';
        $kondisi = array('id_album' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Album';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Album';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('jdl_album', 'Judul Album', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $jdl_album = $this->input->post('jdl_album');
            $gbr_album_old = $this->input->post('gbr_album_old');
            $album_seo = $this->custom->seo_title($jdl_album);

            $data = array(
                'jdl_album' => $jdl_album,
                'album_seo' => $album_seo,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 233;
            $thum_height = 187;
            $resize = true;
            $ori_path = 'media_foto/album/original';
            $tum_path = 'media_foto/album';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($gambar != '') {
                        $data['gbr_album'] = $gambar;
                        unlink('./uploads/'.$ori_path.'/'.$gbr_album_old);
                        unlink('./uploads/'.$tum_path.'/'.$gbr_album_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['gbr_album'] = $gambar;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('album_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/album-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Album delete */
    public function album_delete($id = '')
    {
        $this->check_logged_in();

        $g = $this->Model_data->get_data('t_fotogaleri', array('id_album' => $id), 'id_gallery asc');
        foreach ($g->result() as $r) {
            if ($r->gbr_gallery != '') {
                unlink('./uploads/media_foto/galeri/original/'.$r->gbr_gallery);
                unlink('./uploads/media_foto/galeri/'.$r->gbr_gallery);
            }
        }
        $this->Model_data->del_data('t_fotogaleri', array('id_album' => $id));

        $g2 = $this->Model_data->get_data('t_fotoalbum', array('id_album' => $id), 'id_album asc');
        foreach ($g2->result() as $r2) {
            if ($r2->gbr_album != '') {
                unlink('./uploads/media_foto/album/original/'.$r2->gbr_album);
                unlink('./uploads/media_foto/album/'.$r2->gbr_album);
            }
        }
        $this->Model_data->del_data('t_fotoalbum', array('id_album' => $id));

        $this->return_uri('album_table');
    }

    /* Album AKtif/Nonaktif */
    public function album_noni($id, $kon)
    {
        $this->check_logged_in();

        if ($id != '' and ($kon == 1 or $kon == 0)) {
            $this->Model_data->edt_data('t_fotoalbum', array('aktif' => $kon), array('id_album' => $id));
        }

        $this->return_uri('album_table');
    }

    /* Galeri Page */
    public function galeri_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_fotogaleri';
        $table2 = 't_fotoalbum';
        $relasi = 't_fotoalbum.id_album=t_fotogaleri.id_album';
        $order = 'id_gallery desc, jdl_gallery asc';
        $url = base_url(cadmin.'/galeri_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_galeri' => $q, 'limit_galeri' => $limit));
        } else {
            $q = $this->session->userdata('cari_galeri');
            $limit = $this->session->userdata('limit_galeri');
            if ($limit == '') {
                $limit = 8;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_join($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_join($table, $table2, $relasi, $kondisi);
        } else {
            $kondisi = array('jdl_gallery' => $q, 'keterangan' => $q, 'jdl_album' => $q);
            $res = $this->Model_data->get_pagination_join_like($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_join_like($table, $table2, $relasi, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/galeri-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Galeri Form Add/Edit Page */
    public function galeri_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_fotogaleri';
        $kondisi = array('id_gallery' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Foto';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Foto';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('jdl_gallery', 'Judul Foto', 'required|xss_clean');
        $this->form_validation->set_rules('id_album', 'Album Foto', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $jdl_gallery = $this->input->post('jdl_gallery');
            $gbr_gallery_old = $this->input->post('gbr_gallery_old');
            $gallery_seo = $this->custom->seo_title($jdl_gallery);
            $keterangan = $this->input->post('keterangan');
            $id_album = $this->input->post('id_album');

            $data = array(
                'jdl_gallery' => $jdl_gallery,
                'gallery_seo' => $gallery_seo,
                'keterangan' => $keterangan,
                'id_album' => $id_album,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 233;
            $thum_height = 187;
            $resize = true;
            $ori_path = 'media_foto/galeri/original';
            $tum_path = 'media_foto/galeri';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($gambar != '') {
                        $data['gbr_gallery'] = $gambar;
                        unlink('./uploads/'.$ori_path.'/'.$gbr_gallery_old);
                        unlink('./uploads/'.$tum_path.'/'.$gbr_gallery_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['gbr_gallery'] = $gambar;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('galeri_form/'.$feedback);
        } else {
            $album = $this->Model_data->get_data('t_fotoalbum', array(), 'jdl_album asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul, 'album' => $album);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/galeri-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Galeri Delete */
    public function galeri_delete($id = '')
    {
        $this->check_logged_in();

        $g = $this->Model_data->get_data('t_fotogaleri', array('id_gallery' => $id), 'id_gallery asc');
        foreach ($g->result() as $r) {
            if ($r->gbr_gallery != '') {
                unlink('./uploads/media_foto/galeri/original/'.$r->gbr_gallery);
                unlink('./uploads/media_foto/galeri/'.$r->gbr_gallery);
            }
        }
        $this->Model_data->del_data('t_fotogaleri', array('id_gallery' => $id));

        $this->return_uri('galeri_table');
    }

    /* Show galeri by album */
    public function foto_album($q = '')
    {
        $this->check_logged_in();

        $newq = str_replace('%20', ' ', $q);
        $this->session->set_userdata(array('cari_galeri' => $newq));

        $this->return_uri('galeri_table');
    }

    /* */

    /* User Page */
    public function user_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_users';
        $order = 'id_user desc, username asc';
        $url = base_url(cadmin.'/user_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_user' => $q, 'limit_user' => $limit));
        } else {
            $q = $this->session->userdata('cari_user');
            $limit = $this->session->userdata('limit_user');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('username' => $q, 'nama_lengkap' => $q, 'email' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/user-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* User Add/Edit Page */
    public function user_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_users';
        $kondisi = array('id_user' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit User';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah User';
            $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
        }

        $username = $this->input->post('username');
        $username_old = $this->input->post('username_old');
        if ($username != $username_old) {
            $this->form_validation->set_rules('username', 'Username', 'required|is_unique['.$table.'.username]');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|xxs_clean');
        }

        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, silahkan coba yang lain.');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run()) {
            $nama_lengkap = $this->input->post('nama_lengkap');
            $email = $this->input->post('email');
            $no_telp = $this->input->post('no_telp');
            $password = $this->input->post('password');

            $data = array('username' => $username, 'nama_lengkap' => $nama_lengkap, 'email' => $email, 'no_telp' => $no_telp);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($password != '') {
                    $data['password'] = md5($password);
                }
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                $data['password'] = md5($password);
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('user_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/user-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* User Blokir / Unblokir */
    public function user_blok($id = '', $kon)
    {
        $this->check_logged_in();
        if ($kon == 'Y' || $kon == 'N') {
            $table = 't_users';
            $kondisi = array('id_user' => $id);
            $this->Model_data->edt_data($table, array('blokir' => $kon), $kondisi);
        }

        $this->return_uri('user_table');
    }

    /* User Editor */
    public function user_editor($id = '')
    {
        $this->check_logged_in();

        $table = 't_users';
        $kondisi = array('id_user' => $id);
        if ($this->Model_data->edt_data($table, array('level' => 'D'), array('level !=' => 'A'))) {
            $this->Model_data->edt_data($table, array('level' => 'E'), $kondisi);
        }

        $this->return_uri('user_table');
    }

    /* Video Page */
    public function video_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_video';
        $order = 'id_video desc, jdl_video asc';
        $url = base_url(cadmin.'/video_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_video' => $q, 'limit_video' => $limit));
        } else {
            $q = $this->session->userdata('cari_video');
            $limit = $this->session->userdata('limit_video');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('jdl_video' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/video-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Video Delete */
    public function video_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_video';
        $kondisi = array('id_video' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->file_video != '') {
                unlink('./uploads/media_video/'.$r->file_video);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('video_table');
    }

    /* Video Form Add/Edit Page */
    public function video_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_video';
        $kondisi = array('id_video' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Video';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Video';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('jdl_video', 'Judul Video', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $jdl_video = $this->input->post('jdl_video');
            $file_video_old = $this->input->post('file_video_old');
            $video_seo = $this->custom->seo_title($jdl_video);

            $data = array(
                'jdl_video' => $jdl_video,
                'video_seo' => $video_seo,
            );

            $low_ext = 'mp4';
            $max_size = '102048';
            $thum_width = '';
            $thum_height = '';
            $resize = false;
            $ori_path = 'media_video';
            $tum_path = '';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $video = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $video = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($video != '') {
                        $data['file_video'] = $video;
                        unlink('./uploads/'.$ori_path.'/'.$file_video_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['file_video'] = $video;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('video_form/'.$feedback);
            //var_dump($unggah);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/video-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* File Page */
    public function file_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_file';
        $table2 = 't_kategori_file';
        $relasi = 't_kategori_file.id_kategori_file=t_file.id_kategori_file';
        $order = 'id_file desc, jdl_file asc';
        $url = base_url(cadmin.'/file_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_file' => $q, 'limit_file' => $limit));
        } else {
            $q = $this->session->userdata('cari_file');
            $limit = $this->session->userdata('limit_file');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_join($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_join($table, $table2, $relasi, $kondisi);
        } else {
            $kondisi = array('jdl_file' => $q, 'nama_kategori' => $q);
            $res = $this->Model_data->get_pagination_join_like($table, $table2, $relasi, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_join_like($table, $table2, $relasi, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/file-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* File Delete */
    public function file_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_file';
        $kondisi = array('id_file' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->file != '') {
                unlink('./uploads/media_file/'.$r->file);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('file_table');
    }

    /* File Form Add/Edit Page */
    public function file_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_file';
        $kondisi = array('id_file' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit File';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah File';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('jdl_file', 'Judul File', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|xss_clean');
        $this->form_validation->set_rules('id_kategori_file', 'Kategori', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $jdl_file = $this->input->post('jdl_file');
            $tanggal = $this->custom->format_tgl($this->input->post('tanggal'));
            $id_kategori_file = $this->input->post('id_kategori_file');
            $file_old = $this->input->post('file_old');
            $file_seo = $this->custom->seo_title($jdl_file);

            $data = array(
                'jdl_file' => $jdl_file,
                'file_seo' => $file_seo,
                'tanggal' => $tanggal,
                'id_kategori_file' => $id_kategori_file,
            );

            $low_ext = 'doc|docx|pdf|zip';
            $max_size = '51024';
            $thum_width = '';
            $thum_height = '';
            $resize = false;
            $ori_path = 'media_file';
            $tum_path = '';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $video = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $video = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($video != '') {
                        $data['file'] = $video;
                        unlink('./uploads/'.$ori_path.'/'.$file_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['file'] = $video;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('file_form/'.$feedback);
            //var_dump($unggah);
        } else {
            $kategori = $this->Model_data->get_data('t_kategori_file', array(), 'nama_kategori ASC');
            $konten = array('edited' => $edited, 'kategori' => $kategori, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/file-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Menu Page */
    public function menu_table($offset = 0)
    {
        $this->check_logged_in();

        $res = $this->Model_data->get_data('t_menu', array(), 'parent asc, sort asc');

        $konten = array('res' => $res);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/menu-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Menu Aktif/Nonaktif */
    public function menu_blok($id = '', $kon = '')
    {
        $this->check_logged_in();
        if ($kon == 1 || $kon == 0) {
            $table = 't_menu';
            $kondisi = array('id_menu' => $id);
            $this->Model_data->edt_data($table, array('aktif' => $kon), $kondisi);
        }

        $this->return_uri('menu_table');
    }

    /* Memnu Delete Page */
    public function menu_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_menu';
        $kondisi = array('id_menu' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('menu_table');
    }

    /* Menu Form Page */
    public function menu_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_menu';
        $kondisi = array('id_menu' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Menu';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Menu';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('menu', 'Label Menu', 'required|xss_clean');
        $this->form_validation->set_rules('link', 'Link', 'required|xss_clean');
        $this->form_validation->set_rules('parent', 'Parent', 'required|xss_clean');
        $this->form_validation->set_rules('sort', 'Urutan', 'required|numeric');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $menu = $this->input->post('menu');
            $link = $this->input->post('link');
            $parent = $this->input->post('parent');
            $sort = $this->input->post('sort');
            $keterangan = $this->input->post('keterangan');

            $data = array('menu' => $menu, 'keterangan' => $keterangan, 'link' => $link, 'parent' => $parent, 'sort' => $sort);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('menu_form/'.$feedback);
        } else {
            $menu_list = $this->Model_data->get_data('t_menu', array(), 'parent asc, menu asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul, 'menu_list' => $menu_list);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/menu-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Slider Page */
    public function slider_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_slider';
        $order = 'id_slider desc, judul asc';
        $url = base_url(cadmin.'/slider_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_slider' => $q, 'limit_slider' => $limit));
        } else {
            $q = $this->session->userdata('cari_slider');
            $limit = $this->session->userdata('limit_slider');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('judul' => $q, 'link' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/slider-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Slider Delete */
    public function slider_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_slider';
        $kondisi = array('id_slider' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->gambar != '') {
                unlink('./uploads/slider_original/'.$r->gambar);
                unlink('./uploads/slider_thumbnail/'.$r->gambar);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('slider_table');
    }

    /* Slider Form Add/Edit Page */
    public function slider_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_slider';
        $kondisi = array('id_slider' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Slider';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Slider';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
        $this->form_validation->set_rules('link', 'Link', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $judul = $this->input->post('judul');
            $link = $this->input->post('link');
            $gambar_old = $this->input->post('gambar_old');
            $judul_seo = $this->custom->seo_title($judul);
            $id_user = $this->session->userdata('id_user');

            $data = array(
                'judul' => $judul,
                'link' => $link,
                'judul_seo' => $judul_seo,
                'id_user' => $id_user,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 900;
            $thum_height = 450;
            $resize = true;
            $ori_path = 'slider_original';
            $tum_path = 'slider_thumbnail';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($gambar != '') {
                        $data['gambar'] = $gambar;
                        unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                        unlink('./uploads/'.$tum_path.'/'.$gambar_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['gambar'] = $gambar;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('slider_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/slider-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Banner Page */
    public function banner_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_banner';
        $order = 'id_banner desc, judul asc';
        $url = base_url(cadmin.'/banner_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_banner' => $q, 'limit_banner' => $limit));
        } else {
            $q = $this->session->userdata('cari_banner');
            $limit = $this->session->userdata('limit_banner');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('judul' => $q, 'link' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/banner-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Banner Delete */
    public function banner_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_banner';
        $kondisi = array('id_banner' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->gambar != '') {
                unlink('./uploads/banner_original/'.$r->gambar);
                unlink('./uploads/banner_thumbnail/'.$r->gambar);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('banner_table');
    }

    /* Banner Form Add/Edit Page */
    public function banner_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_banner';
        $kondisi = array('id_banner' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Banner';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Banner';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $judul = $this->input->post('judul');
            $link = $this->input->post('link');
            $gambar_old = $this->input->post('gambar_old');
            $konten = $this->input->post('konten');
            $posisi = $this->input->post('posisi');
            $id_user = $this->session->userdata('id_user');

            $tanggal = $this->input->post('tanggal');
            $tgl = explode(' - ', $tanggal);
            $tgl_mulai = $this->custom->format_tgl($tgl[0]);
            $tgl_selesai = $this->custom->format_tgl($tgl[1]);

            $data = array(
                'judul' => $judul,
                'link' => $link,
                'konten' => $konten,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'posisi' => $posisi,
                'id_user' => $id_user,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 900;
            $thum_height = 450;
            $resize = true;
            $ori_path = 'banner_original';
            $tum_path = 'banner_thumbnail';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($gambar != '') {
                        $data['gambar'] = $gambar;
                        unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                        unlink('./uploads/'.$tum_path.'/'.$gambar_old);
                    }
                    $this->Model_data->edt_data($table, $data, $kondisi);
                    $feedback = $id.'/success';
                } else {
                    //insert data
                    $data['gambar'] = $gambar;
                    $this->Model_data->ins_data($table, $data, true);
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('banner_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/banner-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /*
     *
     <a class="custom-button" style="color: blue; background-color: green;" title="Campus tour" href="#"> <span class="custom-button-wrap"> <span class="custom-button-title" style="color: blue;">Sirup</span> <span class="custom-button-tagline">Sistem Informasi Rencana Umum Pengadaan</span> </span> </a>
     */

    public function banner_form2($msg = '')
    {
        $this->check_logged_in();

        $table = 't_banner';
        $subjudul = 'Form Tambah Banner';

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('judul', 'Judul', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|xss_clean');
        $this->form_validation->set_rules('bgcolor', 'Warna Latar', 'required|xss_clean');
        $this->form_validation->set_rules('fgcolor', 'Warna Tulisan', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $judul = $this->input->post('judul');
            $link = $this->input->post('link');
            $konten = $this->input->post('konten');
            $bgcolor = $this->input->post('bgcolor');
            $fgcolor = $this->input->post('fgcolor');
            $id_user = $this->session->userdata('id_user');

            $tanggal = $this->input->post('tanggal');
            $tgl = explode(' - ', $tanggal);
            $tgl_mulai = $this->custom->format_tgl($tgl[0]);
            $tgl_selesai = $this->custom->format_tgl($tgl[1]);

            $newkonten = '<a class="custom-button" style="color: '.$fgcolor.'; background-color: '.$bgcolor.';" title="" href="'.$link.'"> <span class="custom-button-wrap"> <span class="custom-button-title" style="color: '.$fgcolor.';">'.$judul.'</span> <span class="custom-button-tagline" style="color: '.$fgcolor.';">'.$konten.'</span> </span> </a>';

            $data = array(
                'judul' => $judul,
                'link' => $link,
                'konten' => $newkonten,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'posisi' => 'side',
                'id_user' => $id_user,
            );

            //insert data
            $this->Model_data->ins_data($table, $data, true);
            $feedback = '/success';

            $this->return_uri('banner_form2/'.$feedback);
        } else {
            $konten = array('msg' => $msg, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/banner-form2', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Akun Page */
    public function akun($msg = '')
    {
        $this->check_logged_in();

        $id = $this->session->userdata('id_user');
        $table = 't_users';
        $kondisi = array('id_user' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        $subjudul = 'Form Edit Akun';

        $username = $this->input->post('username');
        $username_old = $this->input->post('username_old');
        if ($username != $username_old) {
            $this->form_validation->set_rules('username', 'Username', 'required|is_unique['.$table.'.username]');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|xxs_clean');
        }

        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');
        $this->form_validation->set_message('is_unique', '%s sudah digunakan, silahkan coba yang lain.');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run()) {
            $nama_lengkap = $this->input->post('nama_lengkap');
            $email = $this->input->post('email');
            $no_telp = $this->input->post('no_telp');
            $password = $this->input->post('password');

            $data = array('username' => $username, 'nama_lengkap' => $nama_lengkap, 'email' => $email, 'no_telp' => $no_telp);

            $feedback = 'user_form/'.$id.'/error';
            //edit data
            if ($password != '') {
                $data['password'] = md5($password);
            }
            if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                $session_data = array(
                    'nama_lengkap' => $nama_lengkap,
                    'username' => $username,
                    'email' => $email,
                    'no_telp' => $no_telp,
                );

                $this->session->set_userdata($session_data);

                $feedback = 'user_form/'.$id.'/success';
            }

            $this->return_uri($feedback);
        } else {
            $konten = array('msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/akun', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Struktur Organisasi Page */
    public function struktur_table($offset = 0)
    {
        $this->check_logged_in();

        $res = $this->Model_data->get_data('t_organisasi', array(), 'parent asc, sort asc');

        $konten = array('res' => $res);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/struktur-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Struktur Organisasi Aktif/Nonaktif */
    public function struktur_blok($id = '', $kon = '')
    {
        $this->check_logged_in();
        if ($kon == 1 || $kon == 0) {
            $table = 't_organisasi';
            $kondisi = array('id_organisasi' => $id);
            $this->Model_data->edt_data($table, array('aktif' => $kon), $kondisi);
        }

        $this->return_uri('struktur_table');
    }

    /* Struktur Organisasi Delete Page */
    public function struktur_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_organisasi';
        $kondisi = array('id_organisasi' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('struktur_table');
    }

    /* Struktur Organisasi Form Page */
    public function struktur_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_organisasi';
        $kondisi = array('id_organisasi' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Struktur Organisasi';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Struktur Organisasi';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_organisasi', 'Nama Jabatan', 'required|xss_clean');
        $this->form_validation->set_rules('parent', 'Posisi', 'required|xss_clean');
        $this->form_validation->set_rules('sort', 'Urutan', 'required|numeric');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_organisasi = $this->input->post('nama_organisasi');
            $parent = $this->input->post('parent');
            $sort = $this->input->post('sort');

            $data = array('nama_organisasi' => $nama_organisasi, 'parent' => $parent, 'sort' => $sort);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('struktur_form/'.$feedback);
        } else {
            $menu_list = $this->Model_data->get_data('t_organisasi', array(), 'parent asc, sort asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul, 'menu_list' => $menu_list);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/struktur-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /*============================ Organisasi DEN Page */
    public function anggota_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 'organisasi_den';
        $order = 'id_organisasi_den ASC';
        $url = site_url(cadmin.'/anggota_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_anggota' => $q, 'limit_anggota' => $limit));
        } else {
            $q = $this->session->userdata('cari_anggota');
            $limit = $this->session->userdata('limit_anggota');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama_pejabat' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/anggota-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Anggota Organisasi Delete */
    public function anggota_delete($id = '')
    {
        $this->check_logged_in();

        $table = 'organisasi_den';
        $kondisi = array('id_organisasi_den' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->foto != '') {
                unlink('./uploads/foto_anggota_original/'.$r->foto);
                unlink('./uploads/foto_anggota_thumbnail/'.$r->foto);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('anggota_table');
    }

    /* Anggota Organisasi Form Add/Edit Page */
    public function anggota_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 'organisasi_den';
        $kondisi = array('id_organisasi_den' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Anggota DEN';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Anggota DEN';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required|xss_clean');
        $this->form_validation->set_rules('nama_pejabat', 'Nama Pejabat', 'required|xss_clean');
        $this->form_validation->set_rules('kategori_jabatan', 'Kategori Jabatan', 'required|xss_clean');
        $this->form_validation->set_rules('urutan', 'Urutan', 'required|xss_clean');
        $this->form_validation->set_rules('periode', 'Periode', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama = $this->input->post('nama');
            $id_organisasi = $this->input->post('id_organisasi');
            $keterangan = $this->input->post('keterangan');
            $gambar_old = $this->input->post('gambar_old');

            $jabatan = $this->input->post('jabatan');
            $nama_pejabat = $this->input->post('nama_pejabat');
            $kategori_jabatan = $this->input->post('kategori_jabatan');
            $urutan = $this->input->post('urutan');
            $periode = $this->input->post('periode');
            $alamat_tautan = $this->input->post('alamat_tautan');
            $profil = $this->input->post('profil');

            $data = array(
                'jabatan' => $jabatan,
                'nama_pejabat' => $nama_pejabat,
                'kategori_jabatan' => $kategori_jabatan,
                'urutan' => $urutan,
                'periode' => $periode,
                'alamat_tautan' => $alamat_tautan,
                'profil' => $profil,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 150;
            $thum_height = 150;
            $resize = true;
            $ori_path = 'foto_anggota_original';
            $tum_path = 'foto_anggota_thumbnail';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            }

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($gambar != '') {
                    $data['namafile'] = $gambar;
                    unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                    unlink('./uploads/'.$tum_path.'/'.$gambar_old);
                }
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                $data['namafile'] = $gambar;
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('anggota_form/'.$feedback);
        } else {
            $kategori = $this->Model_data->get_data('organisasi_den', array(), 'jabatan asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul,
                'kategori' => $kategori, );

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/anggota-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    public function sikat_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_situs_terkait';
        $order = 'nama asc';
        $url = base_url(cadmin.'/sikat_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_sikat' => $q, 'limit_sikat' => $limit));
        } else {
            $q = $this->session->userdata('cari_sikat');
            $limit = $this->session->userdata('limit_sikat');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama' => $q, 'nama_organisasi' => $q, 'keterangan' => $q);
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/sikat-table', $konten);
        $this->load->view('cadmin/footer');
    }

    public function sikat_delete($id = '')
    {
        $this->check_logged_in();

        $table = 't_situs_terkait';
        $kondisi = array('id_sikat' => $id);

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('sikat_table');
    }

    public function sikat_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_situs_terkait';
        $kondisi = array('id_sikat' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Situs Terkait';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Situs Terkait';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama', 'Nama Situs', 'required|xss_clean');
        $this->form_validation->set_rules('link', 'Link', 'required|prep_url|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama = $this->input->post('nama');
            $link = $this->input->post('link');

            $data = array(
                'nama' => $nama,
                'link' => $link,
            );

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('sikat_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/sikat-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    public function strogr()
    {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().'static/organisasi_chart.css">';
        echo '<div class="tree">';
        echo $this->custom->organisasi_one(0, 0);
        echo '</div>';
    }

    /* Struktur Organisasi Page */
    public function struktur_table2($offset = 0)
    {
        $this->check_logged_in();

        $res = $this->Model_data->get_data('t_organisasi2', array(), 'parent asc, sort asc');

        $konten = array('res' => $res);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/struktur-table2', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Struktur Organisasi Aktif/Nonaktif */
    public function struktur_blok2($id = '', $kon = '')
    {
        $this->check_logged_in();
        if ($kon == 1 || $kon == 0) {
            $table = 't_organisasi2';
            $kondisi = array('id_organisasi' => $id);
            $this->Model_data->edt_data($table, array('aktif' => $kon), $kondisi);
        }

        $this->return_uri('struktur_table2');
    }

    /* Struktur Organisasi Delete Page */
    public function struktur_delete2($id = '')
    {
        $this->check_logged_in();

        $table = 't_organisasi2';
        $kondisi = array('id_organisasi' => $id);
        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('struktur_table2');
    }

    /* Struktur Organisasi Form Page */
    public function struktur_form2($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_organisasi2';
        $kondisi = array('id_organisasi' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Struktur Organisasi Sekjen';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Struktur Organisasi Sekjen';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_organisasi', 'Nama Jabatan', 'required|xss_clean');
        $this->form_validation->set_rules('parent', 'Posisi', 'required|xss_clean');
        $this->form_validation->set_rules('sort', 'Urutan', 'required|numeric');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_organisasi = $this->input->post('nama_organisasi');
            $parent = $this->input->post('parent');
            $sort = $this->input->post('sort');

            $data = array('nama_organisasi' => $nama_organisasi, 'parent' => $parent, 'sort' => $sort);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('struktur_form2/'.$feedback);
        } else {
            $menu_list = $this->Model_data->get_data('t_organisasi', array(), 'parent asc, sort asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul, 'menu_list' => $menu_list);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/struktur-form2', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Anggota Organisasi Page */
    public function anggota_table2($offset = 0)
    {
        $this->check_logged_in();

        $table = 'organisasi_setjen';
        $order = 'id_organisasi_setjen ASC';
        $url = site_url(cadmin.'/anggota_table2');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_anggota' => $q, 'limit_anggota' => $limit));
        } else {
            $q = $this->session->userdata('cari_anggota');
            $limit = $this->session->userdata('limit_anggota');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama_pejabat' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/anggota-table2', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Anggota Organisasi Delete */
    public function anggota_delete2($id = '')
    {
        $this->check_logged_in();

        $table = 'organisasi_setjen';
        $kondisi = array('id_organisasi_setjen' => $id);

        $r = $this->Model_data->get_one_data($table, $kondisi);
        if (count($r) > 0) {
            if ($r->foto != '') {
                unlink('./uploads/foto_setjen_original/'.$r->foto);
                unlink('./uploads/foto_setjen_thumbnail/'.$r->foto);
            }
        }

        $this->Model_data->del_data($table, $kondisi);
        $this->return_uri('anggota_table2');
    }

    /* Anggota Organisasi Form Add/Edit Page */
    public function anggota_form2($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 'organisasi_setjen';
        $kondisi = array('id_organisasi_setjen' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Organisasi Setjen';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Organisasi Setjen';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('id_organisasi_setjen', 'Kode Pejabat', 'required|xss_clean');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $id_organisasi_setjen = $this->input->post('id_organisasi_setjen');
            $jabatan = $this->input->post('jabatan');
            $nama_pejabat = $this->input->post('nama_pejabat');
            $deskripsi = $this->input->post('deskripsi');
            $gambar_old = $this->input->post('gambar_old');

            $data = array(
                'id_organisasi_setjen' => $id_organisasi_setjen,
                'jabatan' => $jabatan,
                'nama_pejabat' => $nama_pejabat,
                'deskripsi' => $deskripsi,
            );

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 150;
            $thum_height = 150;
            $resize = true;
            $ori_path = 'foto_anggota_original';
            $tum_path = 'foto_anggota_thumbnail';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            }

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($gambar != '') {
                    $data['namafile'] = $gambar;
                    unlink('./uploads/'.$ori_path.'/'.$gambar_old);
                    unlink('./uploads/'.$tum_path.'/'.$gambar_old);
                }
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                $data['namafile'] = $gambar;
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('anggota_form2/'.$feedback);
        } else {
            $kategori = $this->Model_data->get_data('organisasi_setjen', array(), 'nama_pejabat asc');
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul,
                'kategori' => $kategori, );

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/anggota-form2', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    public function strogr2()
    {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().'static/organisasi_chart.css">';
        echo '<div class="tree">';
        echo $this->custom->organisasi_one2(0, 0);
        echo '</div>';
    }

    /* Kategori Page */
    public function filekategori_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_kategori_file';
        $order = 'id_kategori_file desc, nama_kategori asc';
        $url = base_url(cadmin.'/kategori_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_kategori_file' => $q, 'limit_kategori_file' => $limit));
        } else {
            $q = $this->session->userdata('cari_kategori_file');
            $limit = $this->session->userdata('limit_kategori_file');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama_kategori' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/file-kategori-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Kategori Form Add/Edit Page */
    public function filekategori_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_kategori_file';
        $kondisi = array('id_kategori_file' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Kategori';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Kategori';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'required|xss_clean');
        $this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_kategori = $this->input->post('nama_kategori');
            $status = $this->input->post('status');
            $kategori_seo = $this->custom->seo_title($nama_kategori);

            $data = array('nama_kategori' => $nama_kategori, 'kategori_seo' => $kategori_seo, 'status' => $status);

            $feedback = $id.'/error';
            if ($edited) {
                //edit data
                if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                    $feedback = $id.'/success';
                }
            } else {
                //insert data
                if ($this->Model_data->ins_data($table, $data, false)) {
                    $feedback = $id.'/success';
                }
            }

            $this->return_uri('filekategori_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/file-kategori-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }

    /* Kategori Page */
    public function menu_header_table($offset = 0)
    {
        $this->check_logged_in();

        $table = 't_menu_header';
        $order = 'no_urut asc';
        $url = base_url(cadmin.'/menu_header_table');
        $num_link = 5;

        if (isset($_POST['q'])) {
            $q = $this->input->post('q');
            $limit = $this->input->post('limit');
            $this->session->set_userdata(array('cari_mh' => $q, 'limit_mh' => $limit));
        } else {
            $q = $this->session->userdata('cari_mh');
            $limit = $this->session->userdata('limit_mh');
            if ($limit == '') {
                $limit = 10;
            }
        }

        if ($q == '') {
            $kondisi = array();
            $res = $this->Model_data->get_pagination_where($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row($table, $kondisi);
        } else {
            $kondisi = array('nama_kategori' => $q);
            $res = $this->Model_data->get_pagination_like($table, $kondisi, $limit, $offset, $order);
            $totalrow = $this->Model_data->get_num_row_like($table, $kondisi);
        }

        //create paging
        $this->custom->pagination($url, $totalrow, $limit, $num_link);
        $paging = $this->pagination->create_links();

        $konten = array('res' => $res, 'q' => $q, 'paging' => $paging, 'limit' => $limit, 'offset' => $offset);

        $this->load->view('cadmin/header');
        $this->load->view('cadmin/left-menu');
        $this->load->view('cadmin/menu-headeri-table', $konten);
        $this->load->view('cadmin/footer');
    }

    /* Kategori Form Add/Edit Page */
    public function menu_header_form($id = 'n', $msg = '')
    {
        $this->check_logged_in();

        $table = 't_menu_header';
        $kondisi = array('id_kategori' => $id);

        $res = $this->Model_data->get_one_data($table, $kondisi);
        if (count($res) > 0) {
            $edited = true;
            $subjudul = 'Form Edit Menu';
        } else {
            $edited = false;
            $subjudul = 'Form Tambah Menu';
        }

        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules('nama_kategori', 'Judul Menu', 'required|xss_clean');
        $this->form_validation->set_rules('kategori_seo', 'Link', 'required|xss_clean');
        $this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
        $this->form_validation->set_rules('no_urut', 'No Urut', 'required|xss_clean');
        $this->form_validation->set_message('required', '%s harus diisi.');

        if ($this->form_validation->run()) {
            $nama_kategori = $this->input->post('nama_kategori');
            $status = $this->input->post('status');
            $no_urut = $this->input->post('no_urut');
            $kategori_seo = $this->input->post('kategori_seo');
            $gambar_old = $this->input->post('gambar_old');

            $data = array('nama_kategori' => $nama_kategori, 'kategori_seo' => $kategori_seo, 'status' => $status, 'no_urut' => $no_urut);

            $low_ext = 'gif|jpg|png';
            $max_size = '2048';
            $thum_width = 24;
            $thum_height = 24;
            $resize = true;
            $ori_path = '';
            $tum_path = '';

            $unggah = $this->custom->do_upload($low_ext, $max_size, $ori_path, $tum_path, $resize, $thum_width, $thum_height);

            $gambar = '';
            $feedback = $id.'/error';
            $pros = true;

            if ($unggah['kon']) {
                $gambar = $unggah['msg']['file_name'];
            } else {
                if ($edited) {
                    if ($unggah['msg'] != 'You did not select a file to upload.') {
                        $feedback = $id.'/'.$unggah['msg'];
                        $pros = false;
                    }
                } else {
                    $feedback = $id.'/'.$unggah['msg'];
                    $pros = false;
                }
            }

            if ($pros) {
                if ($edited) {
                    //edit data
                    if ($gambar != '') {
                        $data['gambar'] = $gambar;
                        unlink('./uploads/'.$gambar_old);
                        unlink('./uploads/'.$gambar_old);
                    }

                    if ($this->Model_data->edt_data($table, $data, $kondisi)) {
                        $feedback = $id.'/success';
                    }
                } else {
                    //insert data
                    $data['gambar'] = $gambar;
                    if ($this->Model_data->ins_data($table, $data, false)) {
                        $feedback = $id.'/success';
                    }
                }
            }

            $this->return_uri('menu_header_form/'.$feedback);
        } else {
            $konten = array('edited' => $edited, 'id' => $id, 'msg' => $msg, 'res' => $res, 'subjudul' => $subjudul);

            $this->load->view('cadmin/header');
            $this->load->view('cadmin/left-menu');
            $this->load->view('cadmin/menu-header-form', $konten);
            $this->load->view('cadmin/footer');
        }
    }
}

/* End of file Cadmin.php */
