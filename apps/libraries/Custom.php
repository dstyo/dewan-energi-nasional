<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Custom
{
    public function __construct()
    {
    }

    //conversi bulan dari angka ke huruf
    public function cbulan($val)
    {
        if ($val == 1 || $val == '01') {
            $bln = 'Januari';
        } elseif ($val == 2 || $val == '02') {
            $bln = 'Februari';
        } elseif ($val == 3 || $val == '03') {
            $bln = 'Maret';
        } elseif ($val == 4 || $val == '04') {
            $bln = 'April';
        } elseif ($val == 5 || $val == '05') {
            $bln = 'Mei';
        } elseif ($val == 6 || $val == '06') {
            $bln = 'Juni';
        } elseif ($val == 7 || $val == '07') {
            $bln = 'Juli';
        } elseif ($val == 8 || $val == '08') {
            $bln = 'Agustus';
        } elseif ($val == 9 || $val == '09') {
            $bln = 'September';
        } elseif ($val == 10 || $val == '10') {
            $bln = 'Oktober';
        } elseif ($val == 11 || $val == '11') {
            $bln = 'November';
        } elseif ($val == 12 || $val == '22') {
            $bln = 'Desember';
        } else {
            $bln = '-';
        }

        return $bln;
    }

    //konversi bulan dari angka ke huruf singkat
    public function cbulanshort($val)
    {
        if ($val == 1 || $val == '01') {
            $bln = 'Januari';
        } elseif ($val == 2 || $val == '02') {
            $bln = 'Februari';
        } elseif ($val == 3 || $val == '03') {
            $bln = 'Maret';
        } elseif ($val == 4 || $val == '04') {
            $bln = 'April';
        } elseif ($val == 5 || $val == '05') {
            $bln = 'Mei';
        } elseif ($val == 6 || $val == '06') {
            $bln = 'Juni';
        } elseif ($val == 7 || $val == '07') {
            $bln = 'Juli';
        } elseif ($val == 8 || $val == '08') {
            $bln = 'Agustus';
        } elseif ($val == 9 || $val == '09') {
            $bln = 'September';
        } elseif ($val == 10 || $val == '10') {
            $bln = 'Oktober';
        } elseif ($val == 11 || $val == '11') {
            $bln = 'November';
        } elseif ($val == 12 || $val == '22') {
            $bln = 'Desember';
        } else {
            $bln = '-';
        }

        return $bln;
    }

    //format tanggal Y-m-d to d-m-Y atau kebalikannya
    public function format_tgl($val)
    {
        if ($val != '') {
            $val = str_replace('/', '-', $val);
            $e = explode('-', $val);
            $ndate = $e[2].'-'.$e[1].'-'.$e[0];

            return $ndate;
        }
    }

    //format tanggal Y-m-d to d-m-Y atau kebalikannya dan mengganti - menjadi /
    public function format_tgl_show($val)
    {
        if ($val != '') {
            $e = explode('-', $val);
            $ndate = $e[2].'/'.$e[1].'/'.$e[0];

            return $ndate;
        }
    }

    //format tanggal Y-m-d to d text m Y
    public function format_tgl_text($val)
    {
        if ($val != '') {
            $e = explode('-', $val);
            $ndate = $e[2].' '.$this->cbulanshort($e[1]).' '.$e[0];

            return $ndate;
        }
    }

    public function hari_ini()
    {
        $seminggu = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
        $hari = date('w');
        $hari_ini = $seminggu[$hari];

        return $hari_ini;
    }

    public function tanggal_ini()
    {
        return date('Y-m-d');
    }

    public function jam_ini()
    {
        return date('H:i:s');
    }

    public function nex_pre_date($bln, $thn, $kode)
    {
        if ($kode == 'nex') {
            $bln = $bln + 1;
        } else {
            $bln = $bln - 1;
        }

        if ($bln > 12) {
            $url = '01/'.$thn + 1;
        } elseif ($bln < 1) {
            $url = '12/'.$thn - 1;
        } else {
            $url = $bln.'/'.$thn;
        }

        return $url;
    }

    /* draws a calendar */
    public function draw_calendar($month, $year)
    {

        /* draw table */
        $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

        /* table headings */
        $headings = array('Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab');
        $calendar .= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">', $headings).'</td></tr>';

        /* days and weeks vars now ... */
        $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dates_array = array();

        /* row for week one */
        $calendar .= '<tr class="calendar-row">';

        /* print "blank" days until the first of the current week */
        for ($x = 0; $x < $running_day; ++$x):
            $calendar .= '<td class="calendar-day-np"> </td>';
        ++$days_in_this_week;
        endfor;

        /* keep going with days.... */
        for ($list_day = 1; $list_day <= $days_in_month; ++$list_day):
            $calendar .= '<td valign="top" class="calendar-day">';
            /* add in the day number */
            $calendar .= '<div class="day-number">'.$list_day.'</div>';

            /* QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
            $nono = $year.'-'.$month.'-'.$list_day;
        $roro = $this->get_data('t_agenda', array('tgl_mulai <=' => $nono, 'tgl_selesai >=' => $nono), 'id_agenda desc');
        $calendar .= '<ul>';
        foreach ($roro->result() as $row) {
            $calendar .= '<li class="ccb" onclick="return detailagenda(\''.$row->id_agenda.'\')">'.$row->tema.'</li>';
        }
        $calendar .= '</ul>';

        $calendar .= '</td>';
        if ($running_day == 6):
                $calendar .= '</tr>';
        if (($day_counter + 1) != $days_in_month):
                    $calendar .= '<tr class="calendar-row">';
        endif;
        $running_day = -1;
        $days_in_this_week = 0;
        endif;
        ++$days_in_this_week;
        ++$running_day;
        ++$day_counter;
        endfor;

        /* finish the rest of the days in the week */
        if ($days_in_this_week < 8):
            for ($x = 1; $x <= (8 - $days_in_this_week); ++$x):
                $calendar .= '<td class="calendar-day-np"> </td>';
        endfor;
        endif;

        /* final row */
        $calendar .= '</tr>';

        /* end the table */
        $calendar .= '</table>';

        /* all done, return result */
        return $calendar;
    }

    /* draws a kunjungan */
    public function draw_kunjungan($month, $year)
    {

        /* draw table */
        $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

        /* table headings */
        $headings = array('Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab');
        $calendar .= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">', $headings).'</td></tr>';

        /* days and weeks vars now ... */
        $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dates_array = array();

        /* row for week one */
        $calendar .= '<tr class="calendar-row">';

        /* print "blank" days until the first of the current week */
        for ($x = 0; $x < $running_day; ++$x):
            $calendar .= '<td class="calendar-day-np"> </td>';
        ++$days_in_this_week;
        endfor;

        /* keep going with days.... */
        for ($list_day = 1; $list_day <= $days_in_month; ++$list_day):
            $calendar .= '<td valign="top" class="calendar-day">';
            /* add in the day number */
            $calendar .= '<div class="day-number">'.$list_day.'</div>';

            /* QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
            $nono = $year.'-'.$month.'-'.$list_day;
        $roro = $this->get_data('t_kunjungan_sekolah', array('tgl_mulai <=' => $nono, 'tgl_selesai >=' => $nono), 'id_kunjungan desc');
        $calendar .= '<ul>';
        foreach ($roro->result() as $row) {
            if ($row->status == 1) {
                $calendar .= '<li class="ccb" onclick="return detailkunjungan(\''.$row->id_kunjungan.'\')">'.$row->nama_sekolah.'</li>';
            } else {
                $calendar .= '<li class="ccbx">'.$row->keterangan.'</li>';
            }
        }
        $calendar .= '</ul>';

        $calendar .= '</td>';
        if ($running_day == 6):
                $calendar .= '</tr>';
        if (($day_counter + 1) != $days_in_month):
                    $calendar .= '<tr class="calendar-row">';
        endif;
        $running_day = -1;
        $days_in_this_week = 0;
        endif;
        ++$days_in_this_week;
        ++$running_day;
        ++$day_counter;
        endfor;

        /* finish the rest of the days in the week */
        if ($days_in_this_week < 8):
            for ($x = 1; $x <= (8 - $days_in_this_week); ++$x):
                $calendar .= '<td class="calendar-day-np"> </td>';
        endfor;
        endif;

        /* final row */
        $calendar .= '</tr>';

        /* end the table */
        $calendar .= '</table>';

        /* all done, return result */
        return $calendar;
    }

    //fungsi untuk pagination
    public function pagination($url, $totalrow, $per_page, $num_links)
    {
        $CI = &get_instance();

        $CI->load->library('pagination');

        $config['base_url'] = $url;
        $config['total_rows'] = $totalrow;
        $config['per_page'] = $per_page;
        $config['num_links'] = $num_links;

        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '<i class="fa fa-fast-backward"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = '<i class="fa fa-fast-forward"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-forward"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-backward"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        return $CI->pagination->initialize($config);
    }

    //fungsi untuk pagination
    public function pagination_umum($url, $totalrow, $per_page, $num_links)
    {
        $CI = &get_instance();

        $CI->load->library('pagination');

        $config['base_url'] = $url;
        $config['total_rows'] = $totalrow;
        $config['per_page'] = $per_page;
        $config['num_links'] = $num_links;

        $config['full_tag_open'] = '<ul class="pagination pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '<i class="fa fa-fast-backward"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = '<i class="fa fa-fast-forward"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-forward"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-backward"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        return $CI->pagination->initialize($config);
    }

    //fungsi untuk mengambil satu row dari tabel db
    public function get_one_item($table, $kondisi)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $ndata = $CI->Model_data->get_one_data($table, $kondisi);

        return $ndata;
    }

    public function get_data($table, $kondisi, $order)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $ndata = $CI->Model_data->get_data($table, $kondisi, $order);

        return $ndata;
    }

    //fungsi untuk mengambil jumlah row dari tabel db
    public function get_count_item($table, $kondisi)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $ndata = $CI->Model_data->get_num_row($table, $kondisi);

        return $ndata;
    }

    //cek level yang login (method array)
    public function cek_level($level)
    {
        $CI = &get_instance();
        if (in_array($CI->session->userdata('level'), $level)) {
            return true;
        } else {
            return false;
        }
    }

    //conversi level to label level
    public function level_user($lv)
    {
        if ($lv == 'A') {
            $level = 'Admin';
        } elseif ($lv == 'D') {
            $level = 'Drafter';
        } elseif ($lv == 'E') {
            $level = 'Editor';
        } else {
            $level = '-';
        }

        return $level;
    }

    //fungsi upload file
    public function do_upload($low_ext, $max_size, $ori_path, $tum_path, $slide, $resize, $thum_width, $thum_height)
    {
        //$low_ext = 'gif|jpg|png'
        //$max_size = '2048'
        //$thum_width = 90;
        //$thum_height = 90;
        //$resize = true/false
        //$ori_path = original/path
        //$tum_path = thumbs/path

        $CI = &get_instance();

        $config['upload_path'] = './uploads/'.$ori_path;
        $config['allowed_types'] = $low_ext;
        $config['max_size'] = $max_size;

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload()) {
            $msg = $CI->upload->display_errors();
            $msg = str_replace('<p>', '', $msg);
            $msg = str_replace('</p>', '', $msg);

            $msg2 = array();
            $data = array('msg' => $msg, 'msg2' => $msg2, 'kon' => false);
        } else {
            $msg = $CI->upload->data();
            $msg2 = array();

            if ($resize) {
                $config['image_library'] = 'gd2';
                $config['source_image'] = $msg['full_path'];
                $config['new_image'] = './uploads/'.$tum_path;
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = true;
                $config['width'] = $thum_width;
                $config['height'] = $thum_height;

                $CI->load->library('image_lib', $config);

                if (!$CI->image_lib->resize()) {
                    $msg2 = $CI->image_lib->display_errors();
                }
            }

            $data = array('msg' => $msg, 'msg2' => $msg2, 'kon' => true);
        }

        return $data;
    }

    //conversi status 1/0 to label
    public function status_aktif($val)
    {
        if ($val == 1) {
            $n = '<span class="label label-success">Aktif</span>';
        } else {
            $n = '<span class="label label-danger">Nonaktif</span>';
        }

        return $n;
    }

    //conversi status 1/0 to label
    public function blokir($val)
    {
        if ($val == 'N') {
            $n = '<span class="label label-success">NO</span>';
        } else {
            $n = '<span class="label label-danger">YA</span>';
        }

        return $n;
    }

    //conversi status pesan hubungi kami 0=baru 1=dibaca 2=dibalas
    public function status_pesan($val)
    {
        if ($val == 1) {
            $n = '<span class="label label-info">Dibaca</span>';
        } elseif ($val == 2) {
            $n = '<span class="label label-success">Dibalas</span>';
        } else {
            $n = '<span class="label label-danger">Baru</span>';
        }

        return $n;
    }

    //conversi karakter untuk field seo
    public function seo_title($s)
    {
        $c = array(' ');
        $d = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');

        $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d

        $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
        return $s;
    }

    //show limit form
    public function limit_form($limit, $list = '')
    {
        $form_limit = '<select class="form-control input-sm" name="limit" id="limit" onchange="return submit();">';
        if ($list == '') {
            $list = array(5, 10, 25, 50, 100);
        }
        foreach ($list as $r) {
            if ($limit == $r) {
                $form_limit .= '<option selected value="'.$r.'">'.$r.'</option>';
            } else {
                $form_limit .= '<option value="'.$r.'">'.$r.'</option>';
            }
        }
        $form_limit .= '</select>';

        return $form_limit;
    }

    //generete link seo
    public function link_seo($id, $seo)
    {
        return $id.'-'.$seo.'.html';
    }

    public function posisi_banner($pos)
    {
        if ($pos == 'side') {
            $n = 'Disamping';
        } else {
            $n = 'Dibawah';
        }

        return $n;
    }

    //cut text
    public function cut_text($text, $jum)
    {
        $pnjg = strlen($text);
        if ($pnjg > $jum) {
            $n = substr($text, 0, $jum).'...';
        } else {
            $n = $text;
        }

        return $n;
    }

    //Image generator
    public function img_show($lokasi, $file, $css)
    {
        if ($file != '') {
            $n = '<img src="'.base_url().$lokasi.$file.'" '.$css.' />';
        } else {
            $n = '<img src="'.base_url().'static/images/no-image.png" '.$css.' />';
        }

        return $n;
    }

    public function tag($val)
    {
        return str_replace(' ', ', ', $val);
    }

    public function menu_one($parent, $level)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_menu($parent);

        echo '<ul>';
        foreach ($nq->result() as $row) {
            if ($row->Count > 0) {
                echo '<li>';
                echo $row->menu;
                $this->menu_one($row->id_menu, $level + 1);
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li>';
                echo $row->menu;
                echo '</li>';
            } else ;
        }
        echo '</ul>';
    }

    public function menu_two($parent)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_menu($parent);

        $no = 0;
        foreach ($nq->result() as $row) {
            $act = '';

            if ($CI->session->userdata('parent') == 0) {
                if ($CI->session->userdata('id_menu') == $row->id_menu) {
                    $act = 'active';
                }
            } else {
                if ($CI->session->userdata('parent') == $row->id_menu) {
                    $act = 'active';
                }
            }

            if ($row->Count > 0) {
                echo '<li class="'.$act.'">';
                echo anchor($row->link, $row->menu.' ▼', 'title="" class="mmnu" data-link="'.$row->link.'" data-id="'.$row->id_menu.'" data-parent="'.$row->parent.'"');
                echo '<ul class="sub-menu">';
                $this->menu_two($row->id_menu);
                echo '</ul>';
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li data-id="'.$row->id_menu.'" class="'.$act.'">';
                echo anchor($row->link, $row->menu, 'title="" class="mmnu" data-link="'.$row->link.'" data-id="'.$row->id_menu.'" data-parent="'.$row->parent.'"');
                echo '</li>';
            } else ;

            ++$no;
        }
    }

    public function parent_menu($id)
    {
        if ($id == 0) {
            $n = 'Main Menu';
        } else {
            $CI = &get_instance();
            $CI->load->model('Model_data');
            $d = $CI->Model_data->get_one_data('t_menu', array('id_menu' => $id));
            $n = $d->menu;
        }

        return $n;
    }

    public function organisasi_one($parent, $level)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_organisasi($parent);

        echo '<ul>';
        foreach ($nq->result() as $row) {
            if ($row->Count > 0) {
                echo '<li>';
                echo '<a href="#">'.$row->nama_organisasi.'</a>';
                $this->organisasi_one($row->id_organisasi, $level + 1);
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li>';
                echo '<a href="#">'.$row->nama_organisasi.'</a>';
                echo '</li>';
            } else ;
        }
        echo '</ul>';
    }

    public function organisasi_one2($parent, $level)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_organisasi2($parent);

        echo '<ul>';
        foreach ($nq->result() as $row) {
            if ($row->Count > 0) {
                echo '<li>';
                echo '<a href="#">'.$row->nama_organisasi.'</a>';
                $this->organisasi_one2($row->id_organisasi, $level + 1);
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li>';
                echo '<a href="#">'.$row->nama_organisasi.'</a>';
                echo '</li>';
            } else ;
        }
        echo '</ul>';
    }

    public function organisasi_two($parent, $level)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_organisasi($parent);

        echo '<ul>';
        foreach ($nq->result() as $row) {
            if ($row->Count > 0) {
                echo '<li>';
                echo '<a onclick="return detailorganisasi(\''.$row->id_organisasi.'\');">'.$row->nama_organisasi.'</a>';
                $this->organisasi_two($row->id_organisasi, $level + 1);
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li>';
                echo '<a onclick="return detailorganisasi(\''.$row->id_organisasi.'\');">'.$row->nama_organisasi.'</a>';
                echo '</li>';
            } else ;
        }
        echo '</ul>';
    }

    public function organisasi_two2($parent, $level)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $nq = $CI->Model_data->get_organisasi2($parent);

        echo '<ul>';
        foreach ($nq->result() as $row) {
            if ($row->Count > 0) {
                echo '<li>';
                echo '<a onclick="return detailorganisasi2(\''.$row->id_organisasi.'\');">'.$row->nama_organisasi.'</a>';
                $this->organisasi_two($row->id_organisasi, $level + 1);
                echo '</li>';
            } elseif ($row->Count == 0) {
                echo '<li>';
                echo '<a onclick="return detailorganisasi2(\''.$row->id_organisasi.'\');">'.$row->nama_organisasi.'</a>';
                echo '</li>';
            } else ;
        }
        echo '</ul>';
    }

    public function posisi_organisasi($id)
    {
        if ($id == 0) {
            $n = 'Teratas';
        } else {
            $CI = &get_instance();
            $CI->load->model('Model_data');
            $d = $CI->Model_data->get_one_data('t_organisasi', array('id_organisasi' => $id));
            $n = 'Dibawah '.$d->nama_organisasi;
        }

        return $n;
    }

    public function status_post($val)
    {
        if ($val == 0) {
            $n = '<span class="label label-info">Baru</span>';
        } elseif ($val == 1) {
            $n = '<span class="label label-success">Terbit</span>';
        } elseif ($val == 2) {
            $n = '<span class="label label-danger">Tolak</span>';
        }

        return $n;
    }

    public function status_kunjungan($val)
    {
        if ($val == 0) {
            $n = '<span class="label label-info">Permohonan</span>';
        } elseif ($val == 1) {
            $n = '<span class="label label-success">Terjadwal</span>';
        }

        return $n;
    }

    public function get_username($id, $filed)
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $ndata = $CI->Model_data->get_one_data('t_users', array('id_user' => $id));
        if (count($ndata) > 0) {
            $un = $ndata->$filed;
        } else {
            $un = '-';
        }

        return $un;
    }

    public function konter()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $ipx = $_SERVER['REMOTE_ADDR'];
        $tanggalx = date('Ymd');
        $waktux = time();

        $konuse = array('ip' => $ipx, 'tanggal' => $tanggalx);
        $suse = $CI->Model_data->cek_konfild('t_web_counter', $konuse);
        if ($suse > 0) {
            $CI->Model_data->update_konter($waktux, $ipx, $tanggalx);
        } else {
            $datax = array(
                'ip' => $ipx ,
                'tanggal' => $tanggalx ,
                'hits' => 1,
                'online' => $waktux,
            );

            $CI->Model_data->ins_data('t_web_counter', $datax, false);
        }
    }

    public function konkemarin()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $blnx = date('m');
        $tglx = date('d');
        $thnx = date('Y');
        $tglkx = $tglx - 1;

        if ($tglkx == '1' | $tglkx == '2' | $tglkx == '3' | $tglkx == '4' | $tglkx == '5' | $tglkx == '6' | $tglkx == '7' | $tglkx == '8' | $tglkx == '9') {
            $tg = $thnx.'-'.$blnx.'-0'.$tglkx;
        } else {
            $tg = $thnx.'-'.$blnx.'-'.$tglkx;
        }

        return $CI->Model_data->konter_kemarin($tg);
    }

    public function konbulan()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $blanx = date('Y-m');

        return $CI->Model_data->konter_bulan_tahun($blanx);
    }

    public function kontahun()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $thnx = date('Y');

        return $CI->Model_data->konter_bulan_tahun($thnx);
    }

    public function konharini()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $tanggalx = date('Y-m-d');

        return $CI->Model_data->konter_pengunjung($tanggalx);
    }

    public function kontotalhits()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        return $CI->Model_data->konter_totalhits();
    }

    public function konhits()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        $tanggalx = date('Y-m-d');

        return $CI->Model_data->konter_hits($tanggalx);
    }

    public function nowonline()
    {
        $CI = &get_instance();

        $CI->load->model('Model_data');

        return $CI->Model_data->konter_now_online();
    }
}
