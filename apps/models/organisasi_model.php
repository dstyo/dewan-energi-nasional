<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Organisasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function organisasisetjen($id_organisasi)
    {
        $query = "SELECT * FROM organisasi_setjen WHERE id_organisasi_setjen='$id_organisasi'";
        $data = $this->db->query($query);

        return $data;
    }

    public function organisasiden($katjabatan)
    {
        $query = "SELECT * FROM organisasi_den WHERE kategori_jabatan='$katjabatan' ORDER BY urutan ASC";
        $data = $this->db->query($query);

        return $data;
    }

    public function profilanggotaden($idanggota)
    {
        $query = "SELECT * FROM organisasi_den WHERE id_organisasi_den='$idanggota'";
        $data = $this->db->query($query);

        return $data;
    }
}
