<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_data extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //fungsi untuk mengambil data dari satu table dan mengambilkanya dalam bentuk array
    public function get_one_array($table, $kondisi)
    {
        $query = $this->db->get_where($table, $kondisi, 1);
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    //fungsi untuk mengambil satu data dgn kondisi dari salah satu tabel
    public function get_one_data($table, $kondisi)
    {
        $query = $this->db->get_where($table, $kondisi, 1);
        $row = $query->row();

        return $row;
    }

    //fungsi untuk mengambil data dari satu tabel dengan kondisi
    public function get_data($table, $kondisi, $order)
    {
        $this->db->from($table);
        $this->db->where($kondisi);
        $this->db->order_by($order);

        $query = $this->db->get();

        return $query;
    }

    //fungsi untuk mengambil id terakhir dari salah satu tabel
    public function get_newid($table, $kondisi, $order)
    {
        $this->db->order_by($order);
        $query = $this->db->get_where($table, $kondisi, 1);
        $row = $query->row();

        return $row;
    }

    //mengambil data dengan dari salah satu tabel dengan kondisi like
    public function get_or_like($table, $kondisi)
    {
        $this->db->from($table);
        $this->db->or_like($kondisi);

        $query = $this->db->get();

        return $query;
    }

    //fungsi untuk mengambil data dari satu tabel dengan kondisi dan limit
    public function get_where_limit($table, $kondisi, $limit, $order)
    {
        $this->db->from($table);
        $this->db->where($kondisi);
        $this->db->limit($limit);
        $this->db->order_by($order);

        $query = $this->db->get();

        return $query;
    }

    //fungsi untuk mengambil data dan menggrup data dari satu tabel
    public function get_where_group($table, $kondisi, $group)
    {
        $this->db->from($table);
        $this->db->where($kondisi);
        $this->db->group_by($group);

        $query = $this->db->get();

        return $query;
    }

    //fungsi untuk mengambil seluruh isi salah satu table
    public function get_all($table, $order)
    {
        $this->db->order_by($order);
        $query = $this->db->get($table);

        return $query;
    }

    //fungsi untuk insert data kedalam salah satu tabel -> $retid true/false for get new id
    public function ins_data($table, $data, $retid)
    {
        if ($retid) {
            $this->db->insert($table, $data);

            return $this->db->insert_id();
        } else {
            return $this->db->insert($table, $data);
        }
    }

    public function ins_data_batch($table, $data)
    {
        $res = $this->db->insert_batch($table, $data);

        return $res;
    }

    //fungsi untuk edit data dari salah satu tabel
    public function edt_data($table, $data, $kondisi)
    {
        $this->db->where($kondisi);
        $res = $this->db->update($table, $data);

        return $res;
    }

    //fungsi untuk menghapus item dari salah satu tabel
    public function del_data($table, $kondisi)
    {
        $res = $this->db->delete($table, $kondisi);

        return $res;
    }

    //fungsi untuk cek null pada salah satu tabel
    public function cek_null($table, $kondisi)
    {
        $this->db->from($table);
        $this->db->where($kondisi);

        $query = $this->db->get();
        $num = $query->num_rows();

        // return false jika tidak null
        if ($num > 0) {
            return false;
        } else {
            //return true jika null
            return true;
        }
    }

    //fungsi untuk mengambil total item dari salah satu tabel
    public function get_sumfield($item, $table, $kondisi)
    {
        $this->db->select_sum($item);
        $this->db->where($kondisi);
        $query = $this->db->get($table);

        return $query->row_array();
    }

    //fungsi untuk select data dengan pagination
    public function get_pagination_where($table, $kondisi, $limit, $offset, $order)
    {
        $this->db->where($kondisi);
        $this->db->order_by($order);
        $query = $this->db->get($table, $limit, $offset);

        return $query;
    }

    //fungsi untuk select data dengan pagination dan pencarian
    public function get_pagination_like($table, $kondisi, $limit, $offset, $order)
    {
        $this->db->or_like($kondisi);
        $this->db->order_by($order);
        $query = $this->db->get($table, $limit, $offset);

        return $query;
    }

    //get data pagination dengan jon
    public function get_pagination_join($table, $table2, $relasi, $kondisi, $limit, $offset, $order)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $this->db->order_by($order);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        return $query;
    }

    //get data pagination dengan jon kondisi or
    public function get_pagination_join_like($table, $table2, $relasi, $kondisi, $limit, $offset, $order)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->or_like($kondisi);
        $this->db->order_by($order);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        return $query;
    }

    //get data join dengan kondisi like
    public function get_like_join($table, $table2, $relasi, $kondisi, $order)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->or_like($kondisi);
        $this->db->order_by($order);
        $query = $this->db->get();

        return $query;
    }

    //get data join dari dua tabel
    public function get_join($sel, $table, $table2, $relasi, $kondisi, $order)
    {
        $this->db->select($sel);
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $this->db->order_by($order);
        $query = $this->db->get();

        return $query;
    }

    public function get_join_limit($sel, $table, $table2, $relasi, $kondisi, $limit, $order)
    {
        $this->db->select($sel);
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $this->db->limit($limit);
        $this->db->order_by($order);
        $query = $this->db->get();

        return $query;
    }

    //get data join dari dua tabel
    public function get_join_outer($sel, $table, $table2, $relasi, $kondisi, $order)
    {
        $this->db->select($sel);
        $this->db->from($table);
        $this->db->join($table2, $relasi, 'right');
        $this->db->where($kondisi);
        $this->db->order_by($order);
        $query = $this->db->get();

        return $query;
    }

    //mnghitung jumlah row salah satu tabel dengan kondisi tertentu
    public function get_num_row($table, $kondisi)
    {
        $this->db->from($table);
        $this->db->where($kondisi);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_num_row_like($table, $kondisi)
    {
        $this->db->from($table);
        $this->db->or_like($kondisi);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_num_row_join($table, $table2, $relasi, $kondisi)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_num_row_join_like($table, $table2, $relasi, $kondisi)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->or_like($kondisi);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_pagination_join_like_where($table, $table2, $relasi, $kondisi, $kondisi2, $limit, $offset, $order)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $this->db->or_like($kondisi2);
        $this->db->order_by($order);
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        return $query;
    }

    public function get_num_row_join_like_where($table, $table2, $relasi, $kondisi, $kondisi2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join($table2, $relasi);
        $this->db->where($kondisi);
        $this->db->or_like($kondisi2);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_pagination_like_where($table, $kondisi, $kondisi2, $limit, $offset, $order)
    {
        $this->db->where($kondisi);
        $this->db->or_like($kondisi2);
        $this->db->order_by($order);
        $query = $this->db->get($table, $limit, $offset);

        return $query;
    }

    public function get_num_row_like_where($table, $table, $kondisi, $kondisi2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($kondisi);
        $this->db->or_like($kondisi2);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_menu($parent)
    {
        $q = $this->db->query('SELECT a.*, Deriv1.Count FROM t_menu a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM t_menu GROUP BY parent) Deriv1 ON a.id_menu = Deriv1.parent WHERE a.parent='.$parent." AND aktif='1' ORDER BY a.sort asc");

        return $q;
    }

    public function get_organisasi($parent)
    {
        $q = $this->db->query('SELECT a.*, Deriv1.Count FROM t_organisasi a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM t_organisasi GROUP BY parent) Deriv1 ON a.id_organisasi = Deriv1.parent WHERE a.parent='.$parent.' ORDER BY a.sort asc');

        return $q;
    }

    public function get_organisasi2($parent)
    {
        $q = $this->db->query('SELECT a.*, Deriv1.Count FROM t_organisasi2 a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM t_organisasi2 GROUP BY parent) Deriv1 ON a.id_organisasi = Deriv1.parent WHERE a.parent='.$parent.' ORDER BY a.sort asc');

        return $q;
    }

    public function cek_konfild($tabel, $kondisi)
    {
        $this->db->from($tabel);
        $this->db->where($kondisi);

        return $this->db->count_all_results();
    }

    public function update_konter($waktu, $ip, $tanggal)
    {
        $this->db->query("UPDATE t_web_counter SET hits=hits+1, online='".$waktu."' WHERE ip='".$ip."' AND tanggal='".$tanggal."'");
    }

    public function get_arsip()
    {
        return $this->db->query("SELECT * FROM t_berita WHERE status_berita = 1 AND id_kategori = 1 GROUP BY DATE_FORMAT(tanggal, '%Y%')");
    }

    public function get_arsip_detail($thn)
    {
        return $this->db->query("SELECT * FROM t_berita WHERE tanggal LIKE '%".$thn."%'");
    }

    public function konter_bulan_tahun($kon)
    {
        $this->db->like('tanggal', $kon);
        $this->db->from('t_web_counter');

        return $this->db->count_all_results();
    }

    public function konter_kemarin($kon)
    {
        $this->db->where('tanggal', $kon);
        $this->db->from('t_web_counter');

        return $this->db->count_all_results();
    }

    public function konter_pengunjung($tanggal)
    {
        $this->db->where('tanggal', $tanggal);
        $this->db->from('t_web_counter');
        $this->db->group_by('ip');

        return $this->db->count_all_results();
    }

    public function konter_totalhits()
    {
        $query = $this->db->query('SELECT SUM(hits) AS totalhits FROM t_web_counter');
        $r = $query->row();

        return $r->totalhits;
    }

    public function konter_hits($tanggal)
    {
        $query = $this->db->query("SELECT SUM(hits) AS hitstoday FROM t_web_counter WHERE tanggal='".$tanggal."' GROUP BY tanggal");
        $r = $query->row();

        return $r->hitstoday;
    }

    public function konter_now_online()
    {
        $bataswaktu = time() - 1000;
        $this->db->where('online >', $bataswaktu);
        $this->db->from('t_web_counter');

        return $this->db->count_all_results();
    }

    public function organisasisetjen($id_organisasi)
    {
        $query = "SELECT * FROM organisasi_setjen WHERE id_organisasi_setjen='$id_organisasi'";
        $data = $this->db->query($query);

        return $data;
    }

    public function get_slider($table1, $table2, $kondisi, $order)
    {
        $this->db->from($table1, $table2);
        $this->db->where($kondisi);
        $this->db->order_by($order);

        $query = $this->db->get();

        return $query;
    }
}
