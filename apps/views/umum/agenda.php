<div class="row no-gutter gray-col-ono">
  <div class="col-lg-8 col-md-8">
    <div class="col-padded_ono" style="min-height: 1200px !important">
      <div class="row gutter">
        <div class="col-lg-12 col-md-12">
          <div class="news-title-meta">
            <h1 class="page-title"><?= $subjudul; ?></h1>
          </div>
          <div class="news-body">
            <div class="row style1">
              <div class="col-md-2 text-center">
                <?= anchor('agenda/index/'.$this->custom->nex_pre_date($bulan, $tahun, 'pre'), '<i class="fa fa-arrow-left"></i> Prev', 'class="kopa-button color-button color-blue"'); ?>
              </div>
              <div class="col-md-8 text-center">
                <h3><?= $this->custom->cbulan($bulan).' '.$tahun; ?></h3>
              </div>
              <div class="col-md-2 text-center">
                <?= anchor('agenda/index/'.$this->custom->nex_pre_date($bulan, $tahun, 'nex'), 'Next <i class="fa fa-arrow-right"></i>', 'class="kopa-button color-button color-blue"'); ?>
              </div>
            </div>
            <div class="table-responsive">
              <div class="treedua" style="min-width: 500px;">
                <?php echo $this->custom->draw_calendar($bulan, $tahun); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row gutter">
        <div class="col-lg-12 col-md-12">
          <div style="color: #10B0A6" id="detail-agenda-loading">

          </div>
          <div id="detail-agenda">

          </div>
        </div>
      </div>
    </div>
  </div>
