<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="col-padded">
      <h1 class="page-title text-center">404 Error</h1>
      <div class="news-body">
        <div class="row">
          <div class="col-lg-12">
            <figure class="thumb-404">
              <img src="<?=base_url();?>static/umum/img/404_error_image.png" alt="Error Image" class="img-responsive aligncenter" />
            </figure>
          </div>
          <div class="col-lg-12">
            <h6 class="text-center">Ooops!</h6>
            <p class="text-center">
              Halaman yang kamu cari tidak ditemukan.<b/>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
