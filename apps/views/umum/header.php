<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $identitas->nama_website.' | '.$title; ?></title>
    <meta name="description" content="<?= $identitas->meta_deskripsi; ?>">
    <meta name="keyword" content="<?= $identitas->meta_keyword; ?>">
    <meta name="author" content="<?= $identitas->nama_website; ?>">
    <meta property="og:url" content="http://www.den.go.id" />
	  <meta property="og:type" content="website ini adalah website" />
	  <meta property="og:title" content="DEWAN ENERGY NASIONAL" />
	  <meta property="og:description" content="WEBSITE INI ADALAH WEBSITE" />
	  <meta property="og:image" content="http://design.ubuntu.com/wp-content/uploads/ubuntu-logo32.png" />

    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url(); ?>uploads/favicon/favicon.png" type="image/x-icon">

    <!-- STYLESHEET -->
    <link rel="stylesheet" id="temptation-fonts-css" href="<?= base_url(); ?>static/umum/assets/style/css" type="text/css" media="all">
	  <link rel="stylesheet" id="temptation-basic-style-css" href="<?= base_url(); ?>static/umum/assets/style/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="temptation-basic-style-css" href="<?= base_url(); ?>static/umum/assets/style/pool.css" type="text/css" media="all">
  	<link rel="stylesheet" id="temptation-main-style-css" href="<?= base_url(); ?>static/umum/assets/style/main.css" type="text/css" media="all">
	  <link href="<?= base_url(); ?>static/umum/assets/dropdownstyle/css/theme_3_animation_3.css" rel="stylesheet">
	  <link href="<?= base_url(); ?>static/umum/assets/dropdownstyle/css/theme_3_responsive.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Anaheim' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css1?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>static/organisasi_chart.css">
    <!-- FONTAWESOME -->
    <link href="<?= base_url(); ?>static/umum/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- DROPDOWN MENU -->
    <link href="<?= base_url(); ?>static/umum/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css">
    <!-- BOOTSTRAP -->
    <link href="<?= base_url(); ?>static/umum/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- FANCYBOX -->
    <link href="<?= base_url(); ?>static/umum/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <!-- AUDIOPLAYER -->
    <link href="<?= base_url(); ?>static/umum/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css">
    <!-- THEMESTYLE -->
    <link href="<?= base_url(); ?>static/umum/style.css" rel="stylesheet" type="text/css">
    <!-- FONT GUEH -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <!-- JAVASCRIPT -->

    <!-- CUSTOM STYLESHEET -->
    <style type="text/css">
        @font-face {
            font-family: 'Open Sans', sans-serif !important;';
            font-weight: normal !important;
        }
        a.mmnu{
          font-weight: normal !important;
        }
        h1, h2, h3, h4, h5, h6{
          font-weight: normal !important;
        }
    </style>

    <link href="<?= base_url(); ?>static/umum/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- font-awesome -->
    <link href="<?= base_url(); ?>static/umum/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css">
    <!-- dropdown-menu -->
    <link href="<?= base_url(); ?>static/umum/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap -->
    <link href="<?= base_url(); ?>static/umum/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <!-- Fancybox -->
    <link href="<?= base_url(); ?>static/umum/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css">
    <!-- Audioplayer -->
    <link href="<?= base_url(); ?>static/umum/style.css" rel="stylesheet" type="text/css">
    <!-- theme styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>static/organisasi_chart.css">

    <link rel='stylesheet' href='<?= base_url(); ?>static/umum/unitegallery/css/unite-gallery.css' type='text/css' />
    <link rel='stylesheet' href='<?= base_url(); ?>static/umum/unitegallery/themes/video/skin-right-thumb.css' type='text/css' />
  </head>
  <body role="document">
    <div id="fb-root">

    </div>
    <span class="visible-xs"></span>
    <span class="visible-sm"></span>
    <span class="visible-md"></span>
    <span class="visible-lg"></span>
    <div id="k-head" class="container-head">
      <div class="row">
        <div class="col-lg-6" style="padding: 0px;">
          <a href="<?=base_url();?>" title="Home Page">
            <img style="float: left;" src="<?=base_url();?>static/images/logo-den.png" alt="DEN" width="110" height="110" />
          </a>
          <strong style="float: left; margin: 40px 0px 0px 5px; font-size:23px; color:#10B0A6; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif">DEWAN ENERGI NASIONAL</strong>
        </div>
        <div class="col-lg-6">
          <nav class="k-functional-navig">
            <ul class="list-inline pull-right">
              <?php
                $vnm = $this->Model_data->get_data('t_menu_header', array('status' => 1), 'no_urut ASC');
                foreach ($vnm->result() as $rcmh) {
                  $imgxx = '';
                  if ($rcmh->gambar != '') {
                    $imgxx = '<img src="'.base_url().'uploads/'.$rcmh->gambar.'" />';
                  }
                  //echo '<li><a href="'.$rcmh->kategori_seo.'">'.$imgxx.' '.$rcmh->nama_kategori.'</a></li>';
                  echo '<li><a href="'.$rcmh->kategori_seo.'">'.$rcmh->icon.' '.$rcmh->nama_kategori.'</a></li>';
                }
              ?>
            </ul>
          </nav>
          <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->
          <div class="hidden-xs" style="clear:both; text-align: right; margin-right: 0px; margin-left:200px;">
            <form role="search" method="post" id="course-finder" action="<?= site_url('cari/index'); ?>">
              <div class="input-group">
                <input type="text" placeholder="Pencarian..." autocomplete="off" class="form-control" id="find-item" name="keywords" />
                <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="new-menu">
      <div class="container-head">
        <nav id="k-menu">
          <ul id="drop-down-left" class="k-dropdown-menu">
            <?php
              echo $this->custom->menu_two(0);
            ?>
          </ul>
        </nav>
      </div>
    </div>
    <div id="k-body">
      <div class="container">
