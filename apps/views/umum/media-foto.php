<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <h1 class="page-title"><?= $subjudul; ?></h1><!-- category title -->

                </div>

            </div>
            <!-- row end -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <?php
                    foreach ($konten->result() as $row) {
                        ?>
                    <div class="gallery-wrapper"><!-- gallery single wrap -->

                        <figure class="gallery-last-photo clearfix">
                            <?=anchor('media/album/'.$this->custom->link_seo($row->id_album, $row->album_seo), $this->custom->img_show('uploads/media_foto/album/original/', $row->gbr_album, 'style="width:100%;"'));
                        ?>
                        </figure>

                        <div class="gallery-info">
                            <span class="gallery-photos-num">
                                <?php
                                $q = $this->Model_data->get_data('t_fotogaleri', array('id_album' => $row->id_album), 'id_gallery');
                        echo number_format($q->num_rows());
                        ?>
                            </span>
                            <span class="gallery-photos-tag">photo</span>
                        </div>

                        <div class="gallery-meta">
                            <p class="gallery-title">
                                <?=anchor('media/album/'.$this->custom->link_seo($row->id_album, $row->album_seo), $row->jdl_album);
                        ?>
                            </p>
                        </div>

                    </div>
                    <!-- gallery single wrap end -->
                    <?php

                    }
                    ?>

                </div>

            </div>
            <!-- row end -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12">

                    <?=$paging;?>

                </div>

            </div>
            <!-- row end -->

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->