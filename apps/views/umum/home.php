<div style="min-height: 1200px;">
<?php
  if (count($slider->result()) > 0) {
?>
  <div class="row no-gutter1 fullwidth">
    <div class="col-lg-12 clearfix">
      <div id="carousel-featured" class="carousel slide" data-interval="4000" data-ride="carousel">
        <ol class="carousel-indicators">
          <?php
            $nos = 0;
            foreach ($slider->result() as $rslide) {
              $acts = '';
              if ($nos == 0) {
                $acts = 'class="active"';
              }
              echo '<li data-target="#carousel-featured" data-slide-to="'.$nos.'" '.$acts.' ></li>';
              ++$nos;
            }
          ?>
        </ol>
        <div class="carousel-inner">
          <?php
            $nos = 0;
            foreach ($slider->result() as $rslide) {
              $acts = '';
              if ($nos == 0) {
                $acts = 'active';
              }
          ?>
          <div class="item <?= $acts; ?>">
          <!-- <img style="width: 100%; height: auto;" src="<?= base_url(); ?>uploads/berita_gambar_original/<?= $rslide->gambar; ?>" alt=""/>  -->
          <img style="width: 100%; height: auto;" src="<?= base_url(); ?>uploads/berita_gambar_original/<?= $rslide->gambar; ?>" alt=""/>
            <div class="k-carousel-caption pos-1-3-right scheme-dark">
              <div class="caption-content">
                <a href="<?= $rslide->link; ?>"><h3 class="caption-title"><?= $rslide->judul; ?></h3></a>
              </div>
            </div>
          </div>
          <?php
          ++$nos;
            }
            ?>

                </div>
                <!-- Wrapper for slides end -->

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-featured" data-slide="prev"><i
                        class="fa fa-chevron-left"></i></a>
                <a class="right carousel-control" href="#carousel-featured" data-slide="next"><i
                        class="fa fa-chevron-right"></i></a>
                <!-- Controls end -->

            </div>
            <!-- featured posts slider wrapper end -->

        </div>
        <!-- featured posts slider end -->

    </div>
    <!-- row end -->
<?php

}
?>

<div class="row no-gutter"><!-- row -->

    <div class="col-lg-4 col-md-4 gray-col"><!-- upcoming events wrapper -->

        <div class="col-padded col-shaded"><!-- inner custom column -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <li class="widget-container widget_up_events"><!-- widgets list -->

                    <h1 class="title-widget">Pengumuman</h1>

                    <ul class="list-unstyled">
                        <?php
                        foreach ($pengumuman->result() as $row4) {
                            ?>
                            <li class="up-event-wrap">

                                <h1 class="title-median">
                                	<?php echo $row4->judul;
                            ?>
                                    <?php //echo anchor('dinamispage/index/' . $this->custom->link_seo($row4->id_berita, $row4->judul_seo), $row4->judul, 'title="' . $row4->judul . '"'); ?>
                                </h1>

                                <div class="up-event-meta clearfix">
                                    <div
                                        class="up-event-date"><?= $this->custom->format_tgl_text($row4->tanggal);
                            ?></div>
                                </div>

                                <p>
                                    <?= substr(strip_tags($row4->isi_pengumuman), 0, 100).'.';
                            ?>
                                    <?php //echo anchor('dinamispage/index/' . $this->custom->link_seo($row4->id_berita, $row4->judul_seo), 'Selengkapnya', 'title="Baca lebih lengkap" class="moretag"'); ?>
                                </p>

                            </li>
                        <?php

                        }
                        ?>
                    </ul>

                </li>
                <!-- widgets list end -->

            </ul>
            <!-- widgets end -->

        <!-- inner custom column end -->
        </div>

    </div>
    <!-- upcoming events wrapper end -->

    <div class="col-lg-4 col-md-4 white-col1"><!-- recent news wrapper -->

        <div class="col-padded1 col-naked"><!-- inner custom column -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <li class="widget-container widget_recent_news"><!-- widgets list -->

                    <h1 class="title-widget">Berita</h1>

                    <ul class="list-unstyled">

                        <?php foreach ($berita_terkini->result() as $row33) {
    ?>
                            <li class="recent-news-wrap">

                                <h1 class="title-median">
                                	<?php echo $row33->judul;
    ?>
                                    <?php //echo anchor('dinamispage/index/' . $this->custom->link_seo($row33->id_berita, $row33->judul_seo), $row33->judul, 'title="' . $row33->judul . '"'); ?>
                                </h1>

                                <div class="recent-news-meta">
                                    <div
                                        class="recent-news-date"><?= $this->custom->format_tgl_text($row33->tanggal);
    ?></div>
                                </div>

                                <div class="recent-news-content clearfix">
                                    <figure class="recent-news-thumb">
                                        <?= anchor('dinamispage/index/'.$this->custom->link_seo($row33->id_berita, $row33->judul_seo), $this->custom->img_show('uploads/berita_gambar_thumbnail/', $row33->gambar, 'class="attachment-thumbnail wp-post-image" title="'.$row33->judul.'"'));
    ?>
                                    </figure>
                                    <div class="recent-news-text">
                                        <p>
										<?= substr(strip_tags($row33->isi_berita), 0, 90).'...';
    ?>
                                        <?php echo anchor('dinamispage/index/'.$this->custom->link_seo($row33->id_berita, $row33->judul_seo), 'Selengkapnya', 'title="Baca lebih lengkap" class="moretag"');
    ?>
                                        </p>

                                    </div>
                                </div>

                            </li>
                        <?php

}
                        ?>
                    </ul>

                </li>
                <!-- widgets list end -->

            </ul>
            <!-- widgets end -->
            <a class="custom-button2" style="color: #ffffff; background-color: #F6994A;" title="" href="<?= site_url()?>dinamispage/kategori/1-berita.html">
            <span class="custom-button2-title" style="color: #ffffff;">Berita Lainnya</span></a>

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- recent news wrapper end -->

    <div class="col-lg-4 col-md-4 gray-col"><!-- misc wrapper -->

        <div class="col-padded col-naked"><!-- inner custom column -->

            <ul class="list-unstyled clear-margins"><!-- widgets -->

                <!--
                <h1 class="title-titan">DEN Finder</h1>

                <form role="search" method="post" id="course-finder" action="<?= site_url('cari/index'); ?>">
                    <div class="input-group">
                        <input type="text" placeholder="Temukan Pengumuman, Berita..." autocomplete="off"
                               class="form-control" id="find-item" name="s"/>
                        <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                    </div>
                    <span class="help-block">* Masukkan kata kunci Pengumuman atau Berita</span>
                </form>
                -->

                <?php
                foreach ($banner->result() as $rb) {
                    ?>
                <li class="widget-container widget_text"><!-- widget -->
                    <?php
                    if ($rb->gambar != '') {
                        echo '<a href="'.$rb->link.'">'.$this->custom->img_show('uploads/banner_original/', $rb->gambar, '').'</a>';
                    }
                    echo $rb->konten;
                    ?>
                </li>
                <!-- widget end -->
                <?php

                }
                ?>



                <?php
                if (count($sikat->result() > 0)) {
                    ?>
                <li class="widget-container widget_text">
                    <h1 class="title-widget-ono">Situs Terkait</h1>

                    <div style="padding: 0px 20px;">
                        <ul class="linkedsite" style="margin:0 !important ">
                            <?php
                            foreach ($sikat->result() as $rsik) {
                                ?>
                            <li><a href="<?=$rsik->link;
                                ?>" target="_blank"><?=$rsik->nama;
                                ?></a></li>
                            <?php

                            }
                    ?>
                        </ul>
                    </div>
                </li>
                <?php

                }
                ?>

                <li class="widget-container widget_text"><!-- widget -->

                    <h1 class="title-widget-ono">Statistik Pengunjung</h1>

                    <div class="text-left">
                    	<button class="btn btn-default" type="submit">Total Hits: <b><?=number_format($this->custom->kontotalhits());?></b></button>
                        <button class="btn btn-default" type="submit">Hits Hari Ini: <b><?=number_format($this->custom->konharini());?></b></button>
                        <button class="btn btn-default" type="submit">Hits Bulan Ini: <b><?=number_format($this->custom->konbulan());?></b></button>
                        <button class="btn btn-default" type="submit">Pengguna Online: <b><?=number_format($this->custom->nowonline());?></b></button>
                        <!--<ul class="list-unstyled" style="margin:0 !important">
                            <li><img src="<?=base_url('static/images/k3.gif');?>" /> Hari ini : <?=number_format($this->custom->konharini());?></li>
                            <li><img src="<?=base_url('static/images/k3.gif');?>" /> Kemarin : <?=number_format($this->custom->konkemarin());?></li>
                            <li><img src="<?=base_url('static/images/k1.gif');?>" /> Bulan ini : <?=number_format($this->custom->konbulan());?></li>
                            <li><img src="<?=base_url('static/images/k1.gif');?>" /> Tahun ini : <?=number_format($this->custom->kontahun());?></li>
                            <li><img src="<?=base_url('static/images/k1.gif');?>" /> Total Hits : <?=number_format($this->custom->kontotalhits());?></li>
                            <li><img src="<?=base_url('static/images/k1.gif');?>" /> Hits Count : <?=number_format($this->custom->konhits());?></li>
                            <li><img src="<?=base_url('static/images/k2.gif');?>" /> Now Online : <?=number_format($this->custom->nowonline());?></li>
                        </ul>-->
                     <div>

                </li>

            </ul>
            <!-- widgets end -->

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- misc wrapper end -->

</div>
<!-- row end -->
</div>
