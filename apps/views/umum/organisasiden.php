<style type="text/css">
.org-table h3 {
    font-size: 13px;
    margin: 0;
    line-height: 16px;
    color: #02A393;
}

.org-table, .org-inner-table {
  border-collapse: inherit;
  font-size: 11px;
  line-height: 11px;
  font-family: "Open Sans", sans-serif;
  text-align: center;
  color: #756E6E;
}

.org-inner-table-wrapper {
  border: none;
  background: none;
}

.org-inner-table {
  width: 200px;
}

.org-table tr {
    vertical-align: top;
}

.org-table td {
  /*height: 30px;
/*  max-width: 200px;*/
  text-align: center;
  padding: 0 0 0 0;
  background-color: #EFEFEF;
}

.org-table td.org-item {
  padding: 8px;
  background: #EFEFEF;
  border:solid 1px #cdcdcd;
  width: 180px;
}

.org-table td.org-item h3 {
    padding-bottom: 10px;
}

.org-inner-td {
  background: white;
  padding: 8px;
}

.org-inner-table {
  padding: 8px;
  background: #E5E9ED;
}

.org-inner-title {
  border-top: 4px solid #E5E9ED;
  background: #386192;
}

.org-inner-title h3 {
  color: white;
}

.org-inner-subtitle {
  border-bottom: 1px solid #E5E9ED;
}

.org-inner-td:not(.org-inner-subtitle) {
  border-bottom: 8px solid #E5E9ED;
}

.org-bold {
  font-weight: bold;
}

.org-table-subtitle {
  border-bottom: none;
}

.org-border-top {
  border-top: 2px solid #cdcdcdcd;
}

.org-border-right {
  border-right: 2px solid #cdcdcd;
}

.org-border-bottom {
  border-bottom: 2px solid #cdcdcd;
}

.org-border-left {
  border-left: 2px solid #cdcdcd;
}


.org-table input[type="text"],.org-table input[type="file"] {
    text-align: center;
    border: 0;
    border-left: 1px solid #386192;
    border-bottom: 1px solid #386192;
    padding: 3px;
    margin: 2px;
    background: white;
}

.org-table .org-inner-title input[type="file"]{
    background: #386192;
    border-left: 1px solid white;
    border-bottom: 1px solid white;
}

#pass-foto {
    width: 150px !important;
    height: 200px !important;
    padding: 5px;
    margin: 1px auto;
}
.org-table img{
    height: 100% ;
    width: 100% ;
}

/*==== Style For Organisasi DEN ===*/
.org-table td.org-item-head1{
  padding:10px;
  background: #02A393;
  border:solid 0px #cdcdcd;
  width: 180px;
  color: #ffffff;
  font-size:15px;
  vertical-align:middle;
  text-align:center;
}

.org-table td.org-item-head2{
  padding:10px;
  background: #8C8C8C;
  border:solid 0px #cdcdcd;
  width: 180px;
  color: #ffffff;
  font-size:15px;
  vertical-align:middle;
  text-align:center;
}

.org-table td.org-item-head1-child-left{
  padding:5px;
  background: #EFEFEF;
  border-left:solid 1px #cdcdcd;
  border-bottom:solid 1px #cdcdcd;
  width: 180px;
  color: #756E6E;
  font-size:12px;
  font-weight:bold;
  vertical-align:middle;
  text-align:right;
}

.org-table td.org-item-head1-child-center{
  padding:5px;
  background: #EFEFEF;
  border-right:solid 0px #cdcdcd;
  border-bottom:solid 0px #cdcdcd;
  border-left:solid 0px #cdcdcd;
  width: 180px;
  color: #756E6E;
  font-size:12px;
  font-weight:bold;
  vertical-align:middle;
  text-align:center;
}

.org-table td.org-item-head1-child-center2{
  padding:5px;
  background: #EFEFEF;
  border-right:solid 0px #cdcdcd;
  border-bottom:solid 0px #cdcdcd;
  width: 180px;
  color: #756E6E;
  font-size:12px;
  font-weight:bold;
  vertical-align:middle;
  text-align:center;
}
</style>
<div class="row no-gutter gray-col-ono"><!-- row -->
    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
        <div class="col-padded_ono"><!-- inner custom column -->
            <div class="row gutter1"><!-- row -->
                <div class="col-lg-12 col-md-12">
                    <div class="news-title-meta">
                        <h1 class="page-title-detail"><?php echo strtoupper($subjudul);?></h1>
                    </div>
                </div>
            </div>
            <!-- row end -->
            <!-- row -->
            <div class="row gutter k-equal-height1">
            	<br>
<table class="org-table" width="50%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" class="org-item-head1">PIMPINAN</td>
  </tr>
  	<?php foreach ($pimpinan->result() as $row) {
    ?>
    <tr>
    <td colspan="2" class="org-item-head1-child-center"><?=$row->jabatan;
    ?> &nbsp;&nbsp;:&nbsp;&nbsp;<?=$row->nama_pejabat;
    ?></td>
    </tr>
    <?php
} ?>
  <!--<tr>
    <td width="45%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="45%">&nbsp;</td>
  </tr>-->
  <tr>
    <td colspan="2" class="org-item-head1">ANGGOTA</td>
    </tr>
  <tr>
    <td width="50%" class="org-item-head2" style="border-right:solid 1px #cdcdcd">UNSUR PEMERINTAH</td>
    <td width="50%" class="org-item-head2">UNSUR PEMANGKU KEPENTINGAN</td>
  </tr>
  <tr>
    <!-- UNSUR PEMERINTAH -->
    <td style="border-right:solid 1px #cdcdcd;padding:0 0 0 0;">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="vertical-align:top;">
          <tr style="vertical-align:top;">
            <td class="org-item-head1-child-center">
            <?php foreach ($aup->result() as $row) {
             ?>
            <a href="<?=$row->alamat_tautan;
             ?>"><?=$row->jabatan;
             ?></a>
            <br><br>&nbsp;
            <?php
            } ?>
            <br><br>&nbsp;
            </td>
          </tr>
       </table>
    </td>
    <!-- UNSUR PEMANGKU KEPENTINGAN -->
    <td style="padding:0 0 0 0;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td class="org-item-head1-child-center2 nopadding" style="vertical-align: top;">
                   <br>
                      <?php foreach ($aupk->result() as $row) {
                        ?>
                         <a class="detail-anggota" 
                            data-id-anggota="<?=$row->id_organisasi_den?>" 
                            data-jabatan="aupk" 
                            href="javascript:void(0)"
                          >
                           <?=$row->nama_pejabat;?>
                         </a>
                         <br><br>&nbsp;
                         <?php
                         }
                         ?>
                         </td>
                       </tr>
                     </table>
                 </td>
               </tr>
             </table>
             <div class="row gutter"><!-- row -->
                <div class="col-lg-12 col-md-12">
                    <div style="color: #10B0A6" id="detail-org-loading"></div>
                    <div id="detail-organisasi"></div>
                </div>
            </div>
             <div class="row gutter"><!-- row -->
                <div style="color: #10B0A6" id="detail-anggota-loading"></div>
                <div id="detail-anggota" style="display:none;">
                  <h3 id="detail-anggota-jabatan" class="text-center" style="margin:0px"><span></span></h3>
                  <h3 id="detail-anggota-nama" class="text-center" style="margin:0px; padding-bottom:5px; border-bottom:1px solid #e3e3e3;"><span></span></h3>
                  <div class="col-md-3">
                    <img style="padding-top:5px;" id="detail-anggota-foto" class="img-responsive pull-right" src="" alt="">
                  </div>
                  <div class="col-md-9">
                    <div style="overflow-y:scroll; max-height:450px" id="detail-anggota-deskripsi"></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
            </div>
          </div><!-- row end -->
        </div><!-- inner custom column end -->
      </div><!-- doc body wrapper end -->
