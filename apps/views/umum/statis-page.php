<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <?php
                    if ($konten->gambar != '') {
                        ?>
                    <figure class="news-featured-image">
                        <?=$this->custom->img_show('uploads/page_gambar_original/', $konten->gambar, 'class="img-responsive" style="width: 100%;"');
                        ?>
                    </figure>
                    <?php

                    }
                    ?>

                    <div class="news-title-meta">
                        <h1 class="page-title"><?=$konten->judul;?></h1>
                    </div>

                    <div class="news-body">
                        <?=$konten->isi_halaman;?>
                    </div>


                </div>

            </div>
            <!-- row end -->



        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->
