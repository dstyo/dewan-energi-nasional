<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $identitas->nama_website.' | '.$title; ?></title>
    <meta name="description" content="<?= $identitas->meta_deskripsi; ?>">
    <meta name="keyword" content="<?= $identitas->meta_keyword; ?>">
    <meta name="author" content="<?= $identitas->nama_website; ?>">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url(); ?>uploads/favicon/favicon.png" type="image/x-icon">
    <link rel="stylesheet" id="temptation-fonts-css" href="<?= base_url(); ?>static/umum/assets/style/css" type="text/css" media="all">
	<link rel="stylesheet" id="temptation-basic-style-css" href="<?= base_url(); ?>static/umum/assets/style/style.css" type="text/css" media="all">
	<link rel="stylesheet" id="temptation-main-style-css" href="<?= base_url(); ?>static/umum/assets/style/main.css" type="text/css" media="all">
	<link href="<?= base_url(); ?>static/umum/assets/dropdownstyle/css/theme_3_animation_3.css" rel="stylesheet">
	<link href="<?= base_url(); ?>static/umum/assets/dropdownstyle/css/theme_3_responsive.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Anaheim' rel='stylesheet' type='text/css'>
    <!-- Styles -->
    <link href="http://fonts.googleapis.com/css1?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet"
          type="text/css">
    <!-- Google web fonts -->
    <style type="text/css">
        @font-face {
            font-family: 'FontAwesome';
            src: url('<?=base_url();?>static/umum/font-awesome/fonts/fontawesome-webfont862f862f.eot?v=4.1.0');
            src: url('<?=base_url();?>static/umum/font-awesome/fonts/fontawesome-webfontd41dd41d.eot?#iefix&v=4.1.0') format('embedded-opentype'), url('<?=base_url();?>static/umum/font-awesome/fonts/fontawesome-webfont862f862f.woff?v=4.1.0') format('woff'), url('<?=base_url();?>static/umum/font-awesome/fonts/fontawesome-webfont862f862f.ttf?v=4.1.0') format('truetype'), url('<?=base_url();?>static/umum/font-awesome/fonts/fontawesome-webfont862f862f.svg?v=4.1.0#fontawesomeregular') format('svg');
            font-weight: normal;
            font-style: normal
        }
    </style>
    <link href="<?= base_url(); ?>static/umum/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- font-awesome -->
    <link href="<?= base_url(); ?>static/umum/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css">
    <!-- dropdown-menu -->
    <link href="<?= base_url(); ?>static/umum/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap -->
    <link href="<?= base_url(); ?>static/umum/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <!-- Fancybox -->
    <link href="<?= base_url(); ?>static/umum/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css">
    <!-- Audioplayer -->
    <link href="<?= base_url(); ?>static/umum/style.css" rel="stylesheet" type="text/css">
    <!-- theme styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>static/organisasi_chart.css">

</head>

<body role="document">

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=266711073463140";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- device test, don't remove. javascript needed! -->
<span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span
    class="visible-lg"></span>
<!-- device test end -->

<div id="k-head" class="container"><!-- container + head wrapper -->

    <div class="row">

        <div class="col-lg-6" style="padding: 0px;">
            <a href="<?=base_url();?>" title="Home Page">
                <img style="float: left;" src="<?=base_url();?>static/images/logo-den.png" alt="DEN" width="110" height="110" />
            </a>
            <strong style="float: left; margin: 40px 0px 0px 5px; font-size:23px; color:#10B0A6; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif">DEWAN ENERGI NASIONAL</strong>
        </div>

        <div class="col-lg-6">
            <nav class="k-functional-navig"><!-- functional navig -->

                <ul class="list-inline pull-right">
                   <?php
                    $vnm = $this->Model_data->get_data('t_menu_header', array('status' => 1), 'no_urut ASC');
                    foreach ($vnm->result() as $rcmh) {
                        $imgxx = '';
                        if ($rcmh->gambar != '') {
                            $imgxx = '<img src="'.base_url().'uploads/'.$rcmh->gambar.'" />';
                        }
                        echo '<li><a href="'.$rcmh->kategori_seo.'">'.$imgxx.' '.$rcmh->nama_kategori.'</a></li>';
                    }
                    ?>

                </ul>

            </nav>
            <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->

            <div class="hidden-xs" style="clear:both; text-align: right; margin-right: 20px;">

                <div style="margin-bottom: 10px;" id="google_translate_element"></div>
                <script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: 'id', includedLanguages: 'en,id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
                    }
                </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                <?=date('d').' '.$this->custom->cbulan(date('m')).' '.date('Y');?>
            </div>

        </div>

    </div>
    <!-- row end -->

</div>
<!-- container + head wrapper end -->


<div class="new-menu">
    <div class="container">

        <nav id="k-menu">

        <ul id="drop-down-left" class="k-dropdown-menu">

            <?php
            echo $this->custom->menu_two(0);
            ?>

        </ul>

    </nav>
    </div>
</div>

<!--
    <div class="container">

	  <nav id="k-menu" class="k-main-navig">
	
            <nav class="menu">
                <ul class='cssmenu'>
                  <?= $this->custom->menu_two(0, 0); ?>
                </ul>
            </nav>
		</nav>
    </div>
-->

<div id="k-body"><!-- content wrapper -->

    <div class="container"><!-- container -->

        <div class="row"><!-- row -->

            <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

                <form action="<?= base_url('cari/index'); ?>" id="top-searchform" method="post" role="search">
                    <div class="input-group">
                        <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off"
                               placeholder="Tulis yang ingin anda cari disini, kemudian tekan enter" value="<?= $this->session->userdata('s'); ?>"/>
                    </div>
                </form>

                <div id="bt-toggle-search" class="search-icon text-center">Search <i class="s-open fa fa-search"></i><i
                        class="s-close fa fa-times"></i></div>
                <!-- toggle search button -->

            </div>
            <!-- top search end -->

            <div class="k-breadcrumbs col-lg-12"><!-- breadcrumbs -->

                <!--
                <ol class="breadcrumb">
                    <?php
                    //foreach($breadcrumb as $rbred) {
                    //    echo '<li>'.$rbred.'</li>';
                    //}
                    ?>
                </ol>
                -->
                <marquee class="marquee" behavior="scroll" direction="right">
                    <?php
                    $din = $this->Model_data->get_data('t_berita', array('runningtext' => 1), 'id_berita DESC');
                    foreach ($din->result() as $ros) {
                        echo anchor('dinamispage/index/'.$this->custom->link_seo($ros->id_berita, $ros->judul_seo), $ros->judul);
                        echo ' , ';
                    }

                    $dinx = $this->Model_data->get_data('t_agenda', array('runningtext' => 1), 'id_agenda DESC');
                    foreach ($dinx->result() as $rox) {
                        echo anchor('dinamispage/index/'.$this->custom->link_seo($rox->id_agenda, $rox->tema_seo), $rox->tema);
                        echo ' , ';
                    }
                    ?>
                </marquee>

            </div>
            <!-- breadcrumbs end -->

        </div>
        <!-- row end -->