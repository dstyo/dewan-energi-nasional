<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->


            <h1 class="page-title"><?=$subjudul;?> <span class="label label-success"><?=number_format($totalbum);?> photo</span></h1>

            <div class="news-body">

                <div class="row gutter k-equal-height"><!-- row -->

                    <?php
                    foreach ($konten->result() as $row) {
                        ?>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <figure class="gallery-photo-thumb">
                            <a href="<?=base_url('uploads/media_foto/galeri/original/'.$row->gbr_gallery);
                        ?>" title="<?=$row->jdl_gallery;
                        ?>" data-fancybox-group="gallery-bssb" class="fancybox">
                                <img src="<?=base_url('uploads/media_foto/galeri/'.$row->gbr_gallery);
                        ?>" alt="<?=$row->jdl_gallery;
                        ?>" style="width: 100%;" />
                            </a>
                        </figure>
                        <div class="gallery-photo-description">
                            <b><?=$row->jdl_gallery;
                        ?></b>
                            <br/>
                            <?=$row->keterangan;
                        ?>
                            <br/>
                            <div class="fb-like" data-href="<?=base_url('uploads/media_foto/galeri/original/'.$row->gbr_gallery);
                        ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                        </div>
                    </div>
                    <?php

                    }
                    ?>

                </div><!-- row end -->

            </div>

        </div><!-- inner custom column end -->

    </div><!-- doc body wrapper end -->
    