<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->

            <div class="row gutter1"><!-- row -->

                <div class="col-lg-12 col-md-12">
                    <div class="news-title-meta">
                        <h1 class="page-title-detail"><?php echo strtoupper($subjudul);?></h1>

                    </div>
                </div>

            </div>

            <!-- row end -->

            <!-- row -->
            <div class="row gutter k-equal-height1">

                <?php
                foreach ($konten->result() as $row) {
                    ?>

                    <div class="news-mini-wrap col-lg-12 col-md-12"><!-- news mini-wrap -->

                        <figure class="news-featured-image">
                            <?= $this->custom->img_show('uploads/berita_gambar_original/', $row->gambar, 'style="width:100%;height:250px;" class="img-responsive"');
                    ?>
                        </figure>

                        <div class="page-title-meta">
                            <h1 class="page-title">
                                <?= anchor('dinamispage/index/'.$this->custom->link_seo($row->id_berita, $row->judul_seo), $row->judul, 'title="'.$row->judul.'"');
                    ?>
                            </h1>

                            <div class="news-meta">
                                <span
                                    class="news-meta-date"><?= $this->custom->format_tgl_text($row->tanggal);
                    ?></span>
                                <!-- <span class="news-meta-category"><?= $row->tag;
                    ?></span> -->
                                <!--<span class="news-meta-comments"><a href="#" title="3 comments">3 comments</a></span>-->
                            </div>
                        </div>

                        <div class="news-summary">
                            <p>
                                <?= substr(strip_tags($row->isi_pengumuman), 0, 150).'...';
                    ?>
                                <?= anchor('dinamispage/index/'.$this->custom->link_seo($row->id_pengumuman, $row->judul_seo), 'Selengkapnya', 'title="Baca selengkapnya" class="moretag"');
                    ?>
                            </p>
                        </div>

                    </div>
                    <!-- news mini-wrap end -->
                <?php

                }
                ?>

            </div>
            <!-- row end -->

            <!-- row -->
            <div class="row gutter">

                <div class="col-lg-12">

                    <?= $paging; ?>
                    <!-- pagination end -->

                </div>

            </div>
            <!-- row end -->

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->
