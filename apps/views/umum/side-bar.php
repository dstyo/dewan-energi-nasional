<div id="k-sidebar" class="col-lg-4 col-md-4">
  <div class="col-padded col-shaded">
    <ul class="list-unstyled clear-margins">
      <li class="widget-container widget_recent_news">
        <h1 class="title-widget">Berita Populer</h1>
        <ul class="list-unstyled">
          <?php
            foreach ($berita_populer->result() as $r1) {
          ?>
          <li class="recent-news-wrap news-no-summary">
            <div class="recent-news-content clearfix">
              <figure class="recent-news-thumb">
                <?= anchor('dinamispage/index/'.$this->custom->link_seo($r1->id_berita, $r1->judul_seo), $this->custom->img_show('uploads/berita_gambar_thumbnail/', $r1->gambar, 'class="attachment-thumbnail wp-post-image" title="'.$r1->judul.'"')); ?>
              </figure>
              <div class="recent-news-text">
                <div class="recent-news-meta">
                  <div class="recent-news-date">
                    <?= $this->custom->format_tgl_text($r1->tanggal); ?>
                  </div>
                </div>
                <p  class="title-median-sidebar">
                  <a href="<?= site_url();?>/dinamispage/index/<?= $this->custom->link_seo($r1->id_berita, $r1->judul_seo);?>"><?php echo $r1->judul; ?></a>
                </p>
              </div>
            </div>
          </li>
          <?php
            }
          ?>
        </ul>
      </li>
      <?php
        foreach ($banner->result() as $r2) {
      ?>
      <li class="widget-container widget_text">
        <?php
          if ($r2->gambar != '') {
            echo '<a href="'.$r2->link.'">'.$this->custom->img_show('uploads/banner_original/', $r2->gambar, '').'</a>';
          }
          echo $r2->konten;
        ?>
      </li>
      <?php
        }
      ?>
    </div>
  </div>
</div>
