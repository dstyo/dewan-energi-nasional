<div class="row no-gutter gray-col-ono"><!-- row -->
    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
        <div class="col-padded_ono"><!-- inner custom column -->
            <div class="row gutter"><!-- row -->
					              <div class="news-title-meta">
                        <h1 class="page-title-detail"><?= strtoupper($subjudul);?></h1>
                        <div class="news-meta">
                            <span class="news-meta-category">
                            	<i class="fa fa-home"></i> <?=anchor('', 'Beranda');?>
                            	&raquo;
								                      <?=anchor('dinamispage/organisasiden', 'Organisasi DEN');?>
                                &raquo;
                               <?=$profil->nama_pejabat;?>
                          	</span>
                        </div>
                    </div>
                    <?php
                    if ($profil->namafile != '') {
                        ?>
                    <figure class="news-featured-image">
                        <?=$this->custom->img_show('uploads/foto_anggota_original/', $profil->namafile, 'class="img-responsive" style="width: 100%;"');
                        ?>
                    </figure>
                    <?php
                    }
                    ?>
                    <div class="news-body">
                    	<b>Nama : </b><?=$profil->nama_pejabat;?>
                        <br>
                        <?=$profil->profil;?>
                    </div>
                </div>
            </div>
            <!-- row end -->
        </div>
        <!-- inner custom column end -->
    </div>
    <!-- doc body wrapper end -->

<!--Modals-->
<!--button-->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
<!--button-->
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Modal Header</h4>
    </div>
    <div class="modal-body">
      <p>Some text in the modal.</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
