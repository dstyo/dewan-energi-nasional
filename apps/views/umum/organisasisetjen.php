<style type="text/css">
.org-table h3 {
    font-size: 13px;
    margin: 0;
    line-height: 16px;
    color: #02A393;
}

.org-table, .org-inner-table {
  border-collapse: inherit;
  font-size: 11px;
  line-height: 11px;
  font-family: "Open Sans", sans-serif;
  text-align: center;
  color: #756E6E;
}

.org-inner-table-wrapper {
  border: none;
  background: none;
}

.org-inner-table {
  width: 200px;
}

.org-table tr {
    vertical-align: top;
}

.org-table td {
  height: 40px;
/*  max-width: 200px;*/
  text-align: center;
  padding: 0 10px;
}

.org-table td.org-item {
  padding: 8px;
  background: #EFEFEF;
  border:solid 1px #cdcdcd;
  width: 180px;
}

.org-table td.org-item h3 {
    padding-bottom: 10px;
}

.org-inner-td {
  background: white;
  padding: 8px;
}

.org-inner-table {
  padding: 8px;
  background: #E5E9ED;
}

.org-inner-title {
  border-top: 4px solid #E5E9ED;
  background: #386192;
}

.org-inner-title h3 {
  color: white;
}

.org-inner-subtitle {
  border-bottom: 1px solid #E5E9ED;
}

.org-inner-td:not(.org-inner-subtitle) {
  border-bottom: 8px solid #E5E9ED;
}

.org-bold {
  font-weight: bold;
}

.org-table-subtitle {
  border-bottom: none;
}

.org-border-top {
  border-top: 2px solid #cdcdcdcd;
}

.org-border-right {
  border-right: 2px solid #cdcdcd;
}

.org-border-bottom {
  border-bottom: 2px solid #cdcdcd;
}

.org-border-left {
  border-left: 2px solid #cdcdcd;
}


.org-table input[type="text"],.org-table input[type="file"] {
    text-align: center;
    border: 0;
    border-left: 1px solid #386192;
    border-bottom: 1px solid #386192;
    padding: 3px;
    margin: 2px;
    background: white;
}

.org-table .org-inner-title input[type="file"]{
    background: #386192;
    border-left: 1px solid white;
    border-bottom: 1px solid white;
}

#pass-foto {
    width: 150px !important;
    height: 200px !important;
    padding: 5px;
    margin: 1px auto;
}
.org-table img{
    height: 100% ;
    width: 100% ;
}
</style>

<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono"><!-- inner custom column -->

            <div class="row gutter1"><!-- row -->

                <div class="col-lg-12 col-md-12">
                    <div class="news-title-meta">
                        <h1 class="page-title-detail"><?php echo strtoupper($subjudul);?></h1>

                    </div>
                </div>

            </div>

            <!-- row end -->

            <!-- row -->
            <div class="row gutter k-equal-height1">
            	<br>

              <table  class="org-table" width="100%" border="1" cellspacing="0" cellpadding="0">
                <!--<tr>
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                  <td>7</td>
                  <td>8</td>
                  <td>9</td>
                  <td>10</td>
                  <td>11</td>
                </tr>-->
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3>
							<?php echo $sjd1->jabatan; ?>
                            </h3>
                            </a>
                            <?php echo $sjd1->nama_pejabat; ?>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="4" class="org-border-bottom">&nbsp;</td>
                  <td colspan="4" class="org-border-left org-border-bottom">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-right">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro1->jabatan?></h3>
                            </a>
                            <?php echo $biro1->nama_pejabat?>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro2->jabatan;?></h3>
                            </a>
                            <?php echo $biro2->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro3->jabatan;?></h3>
                            </a>
                            <?php echo $biro3->nama_pejabat;?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="org-border-right org-border-bottom">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-border-right org-border-bottom">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="org-border-right org-border-bottom">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro1a->jabatan;?></h3>
                            </a>
                            <?php echo $biro1a->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro2a->jabatan;?></h3>
                            </a>
                            <?php echo $biro2a->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro3a->jabatan;?></h3>
                            </a>
                            <?php echo $biro3a->nama_pejabat;?>
                  </td>
                </tr>
                <tr>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                </tr>

                <tr>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                 <tr>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro1b->jabatan;?></h3>
                            </a>
                            <?php echo $biro1b->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro2b->jabatan;?></h3>
                            </a>
                            <?php echo $biro2b->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro3b->jabatan;?></h3>
                            </a>
                            <?php echo $biro3b->nama_pejabat;?>
                  </td>
                </tr>
                <tr>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>

                <tr>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="org-border-left">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                            <h3><?php echo $biro1c->jabatan;?></h3>
                            </a>
                            <?php echo $biro1c->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td class="org-border-left org-border-bottom">&nbsp;</td>
                  <td colspan="2" rowspan="2" class="org-item">
                  	<a class="/organization/view/0" id="pop-up-0">
                           <h3><?php echo $biro2c->jabatan;?></h3>
                            </a>
                            <?php echo $biro2c->nama_pejabat;?>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                 <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>

              </table>



            </div>
            <!-- row end -->

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->
