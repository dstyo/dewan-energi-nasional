<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->


            <h1 class="page-title"><?=$subjudul;?></h1>

            <div class="news-body">

                <div class="row gutter k-equal-height"><!-- row -->

                    <?php
                    foreach ($konten->result() as $row) {
                        ?>
                    <div class="col-lg-5 col-md-4 col-sm-12">
                        <figure class="gallery-photo-thumb">
                            <video style="width: 100%; height: auto; position: relative; background-color:black;" controls>
                                <source
                                    src="<?= base_url('uploads/media_video/'.$row->file_video);
                        ?>"
                                    type="video/mp4"/>
                                <source
                                    src="<?= base_url('uploads/media_video/'.$row->file_video);
                        ?>"
                                    type="video/ogg"/>
                                Maaf, browser kamu tidak suport untuk memutar video ini, silahkan update
                                browsernya dulu.
                            </video>
                        </figure>
                        <div class="gallery-photo-description">
                            <?=$row->jdl_video;
                        ?>
                            <br/>
                            <div class="fb-like" data-href="<?=base_url('uploads/media_video/'.$row->file_video);
                        ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                        </div>
                    </div>
                    <?php

                    }
                    ?>

                </div><!-- row end -->

                <div class="row gutter"><!-- row -->

                    <div class="col-lg-12">

                        <?=$paging;?>

                    </div>

                </div>
                <!-- row end -->

            </div>

        </div><!-- inner custom column end -->

    </div><!-- doc body wrapper end -->

