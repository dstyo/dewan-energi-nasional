<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <!--
                    <div id="k-contact-map" class="clearfix"></div>
                    -->

                    <h1 class="page-title"><?=$subjudul;?></h1>

                    <div class="news-body">
                        <?=$sosmed->alamat;?>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="remove-margin-bottom">Telepon</h6>
                                <p class="small"><?=$sosmed->phone;?></p>

                                <h6 class="remove-margin-top remove-margin-bottom">Fax</h6>
                                <p class="small"><?=$sosmed->fax;?></p>

                                <h6 class="remove-margin-top remove-margin-bottom">Email</h6>
                                <p class="small"><a href="mailto:<?=$sosmed->email;?>"><?=$sosmed->email;?></a></p>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6 class="remove-margin-bottom">Facebook</h6>
                                <p class="small"><a href="http://facebook.com/<?=$sosmed->facebook;?>"><?=$sosmed->facebook;?></a></p>

                                <h6 class="remove-margin-top remove-margin-bottom">Twitter</h6>
                                <p class="small"><a href="http://twitter.com/<?=$sosmed->twitter;?>"><?=$sosmed->twitter;?></a></p>

                                <h6 class="remove-margin-top remove-margin-bottom">Google Plus</h6>
                                <p class="small"><a href="http://plus.google.com/<?=$sosmed->googleplus;?>"><?=$sosmed->googleplus;?></a></p>
                            </div>
                        </div>

                        <hr />

                        <h6>Tinggalkan Pesan Anda Disini</h6>

                        <div id="hasil_kirim"></div>

                        <?=form_open('kontak/proses', 'id="contactformx"');?>
                            <div class="row"><!-- starts row -->
                                <div class="form-group col-lg-12">
                                    <label for="contactName"><span class="required">*</span> Name</label>
                                    <br>
                                    <input type="text" required="true" size="30" value="" name="contactName" id="contactName" class="form-control requiredField" />
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="email"><span class="required">*</span> E-mail</label>
                                    <br>
                                    <input type="email" required="true" size="30" value="" name="email" id="email" class="form-control requiredField" />
                                </div>

                                <div class="form-group col-lg-12">
                                    <label for="contactSubject"><span class="required">*</span> Message Subject</label>
                                    <br>
                                    <input type="text" required="true" size="30" value="" name="contactSubject" id="contactSubject" class="form-control" />
                                </div>
                            <!-- starts row -->
                                <div class="form-group clearfix col-lg-12">
                                    <label for="comments"><span class="required">*</span> Message</label>
                                    <textarea required="true" rows="5" cols="45" name="comments" id="comments" class="form-control requiredField mezage"></textarea>
                                </div>

                                <div class="form-group clearfix col-lg-12 text-right remove-margin-bottom">
                                    <input type="hidden" name="submitted" id="submitted" value="true" />
                                    <input type="submit" value="Send Message" id="submitmsg" name="submit" class="btn btn-success" />
                                </div>
                            </div><!-- ends row -->
                        <?=form_close();?>

                    </div>

                </div>

            </div><!-- row end -->

        </div><!-- inner custom column end -->

    </div><!-- doc body wrapper end -->
