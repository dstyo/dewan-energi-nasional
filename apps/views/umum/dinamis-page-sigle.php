<div id="fb-root"></div>
<script>(
    function (d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
     fjs.parentNode.insertBefore(js, fjs);
     }
     (document, 'script', 'facebook-jssdk'));
     <!--Twitter-->
     twttr.widgets.createShareButton(
      "https:\/\/dev.twitter.com\/web\/tweet-button",
      document.getElementById("tweet-container"),
      {
    size: "large",
    via: "twitterdev",
    related: "twitterapi,twitter",
    text: "custom share text",
    hashtags: "example,demo"
   }
    );
    <!--end twitter-->
    <!--google plus-->
  window.___gcfg = {
    lang: 'zh-CN',
    parsetags: 'onload'
  };
  <!--end google plus-->
</script>
<div class="row no-gutter gray-col-ono">
  <div class="col-lg-8 col-md-8">
    <div class="col-padded_ono">
      <div class="row gutter">
        <div class="col-lg-12 col-md-12">
          <div class="news-title-meta">
            <h1 class="page-title-detail">
              <?=$konten->judul;?>
            </h1>
            <div class="news-meta">
              <span class="news-meta-date"><i class="fa fa-calendar"></i>
                <?=$this->custom->format_tgl_text($konten->tanggal);?>
              </span>
              <span class="news-meta-category"><i class="fa fa-tag"></i>
                <?=anchor('dinamispage/showcat', $konten->nama_kategori);?>
              </span>
            </div>
          </div>
          <?php
          if ($konten->gambar != '') {
          ?>
          <figure class="news-featured-image">
            <?=$this->custom->img_show('uploads/berita_gambar_original/', $konten->gambar, 'class="img-responsive" style="width: 100%;"'); ?>
          </figure>
          <?php
          }
          ?>
          <div class="news-body">
            <?=$konten->isi_berita;?>
          </div>
          <!--facebook-->
          <div class="fb-share-button" data-href="<?= 'dinamispage/index/'.$this->custom->link_seo($konten->id_berita, $konten->judul_seo)?>" data-layout="button_count"></div>
          <!--end facebook-->
          <!--twitter-->
          <a class="twitter-share-button"
           href="https://twitter.com/share"
           data-size="small"
           data-url="https://dev.twitter.com/web/tweet-button"
           data-via="twitterdev"
           data-related="twitterapi,twitter"
           data-hashtags="example,demo"
           data-text="custom share text">
          </a>
          <iframe
          src="https://platform.twitter.com/widgets/tweet_button.html?size=l&url=https%3A%2F%2Fdev.twitter.com%2Fweb%2Ftweet-button&via=twitterdev&related=twitterapi%2Ctwitter&text=custom%20share%20text&hashtags=example%2Cdemo"
          width="140"
          height="28"
          title="Twitter Tweet Button"
          style="border: 0; overflow: hidden;">
         </iframe>
        <!-- end twiter-->
        <!-- google plus-->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <g:plus action="share"></g:plus>
        <!-- end google plus-->
        </div>
      </div>
    </div>
  </div>
