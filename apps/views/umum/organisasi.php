<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-12 col-md-12"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <div class="news-title-meta">
                        <h1 class="page-title"><?= $subjudul; ?></h1>
                    </div>

                    <div class="news-body">

                        <div class="table-responsive">
                            <div class="treedua" style="min-width: 800px;">
                                <?php echo $this->custom->organisasi_two(0, 0); ?>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- row end -->
            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">
                    <div style="color: #10B0A6" id="detail-org-loading"></div>
                    <div id="detail-organisasi"></div>

                </div>

            </div>
            <!-- row end -->
        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->
