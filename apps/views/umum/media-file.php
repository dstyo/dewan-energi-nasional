<div class="row no-gutter gray-col-ono"><!-- row -->

    <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

        <div class="col-padded_ono" style="min-height: 1200px !important"><!-- inner custom column -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <h1 class="page-title"><?= $subjudul; ?></h1><!-- category title -->

                </div>

            </div>
            <!-- row end -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12 col-md-12">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Dokumen</th>
                            <th class="text-right">Downloader</th>
                            <th class="text-right">Download</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 0;
                        foreach ($konten->result() as $row) {
                            ++$no;
                            ?>
                        <tr>
                            <td><?=$no;
                            ?></td>
                            <td><?=$row->jdl_file;
                            ?></td>
                            <td class="text-right"><?=number_format($row->downloader);
                            ?></td>
                            <td class="text-right">
                                <?=anchor('media/download/'.$row->file.'/'.$row->id_file.'/'.$row->downloader, '<i class="fa fa-download"></i>');
                            ?>
                            </td>
                        </tr>
                        <?php

                        }
                        ?>

                        </tbody>
                    </table>


                </div>

            </div>
            <!-- row end -->

            <div class="row gutter"><!-- row -->

                <div class="col-lg-12">

                    <?=$paging;?>

                </div>

            </div>
            <!-- row end -->

        </div>
        <!-- inner custom column end -->

    </div>
    <!-- doc body wrapper end -->