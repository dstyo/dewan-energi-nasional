<div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded col-shaded"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">Berita Populer</h1>

                <ul class="list-unstyled">
                    <?php
                    foreach ($berita_populer->result() as $r1) {
                        ?>
                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <figure class="recent-news-thumb">
                                    <?= anchor('dinamispage/index/'.$this->custom->link_seo($r1->id_berita, $r1->judul_seo), $this->custom->img_show('uploads/berita_gambar_thumbnail/', $r1->gambar, 'class="attachment-thumbnail wp-post-image" title="'.$r1->judul.'"'));
                        ?>
                                </figure>
                                <div class="recent-news-text">
                                    <div class="recent-news-meta">
                                        <div
                                            class="recent-news-date"><?= $this->custom->format_tgl_text($r1->tanggal);
                        ?></div>
                                    </div>
                                    <p  class="title-median">
                                        <?= anchor('dinamispage/index/'.$this->custom->link_seo($r1->id_berita, $r1->judul_seo), $r1->judul, 'title="'.$r1->judul.'"');
                        ?>
                                    </p>
                                </div>
                            </div>

                        </li>
                    <?php

                    }
                    ?>
                </ul>

            </li>
            <!-- widgets list end -->

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">Arsip Berita</h1>

                <ul class="list-unstyled">
                    <?php
                    $xxq = $this->Model_data->get_arsip();
                    foreach ($xxq->result() as $r2) {
                        ?>
                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <div style="cursor: pointer;">
                                    <span onclick="return shoarsip('<?=substr($r2->tanggal, 0, 4);
                        ?>');"> > <?=substr($r2->tanggal, 0, 4);
                        ?></span>

                                    <div id="x<?=substr($r2->tanggal, 0, 4);
                        ?>" style="display:none;">
                                        <?php
                                        $xcv = $this->Model_data->get_arsip_detail(substr($r2->tanggal, 0, 4));
                        echo '<ul>';
                        foreach ($xcv->result() as $rxvx) {
                            echo '<li>'.anchor('dinamispage/index/'.$this->custom->link_seo($rxvx->id_berita, $rxvx->judul_seo), $rxvx->judul, 'title="'.$rxvx->judul.'"').'</li>';
                        }
                        echo '</ul>';
                        ?>
                                    </div>

                                </div>
                            </div>

                        </li>
                    <?php

                    }
                    ?>
                </ul>


            </li>
            <!-- widgets list end -->


            <?php
            if ($sosmed->facebook != '') {
                ?>
                <li class="widget-container widget_nav_menu"><!-- widget -->

                    <h1 class="title-widget">Facebook</h1>

                    <div class="fb-page" data-href="https://www.facebook.com/<?= $sosmed->facebook;
                ?>"
                         data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"></div>
                </li>
            <?php 
            } ?>

            <?php
            foreach ($banner->result() as $r2) {
                ?>
                <li class="widget-container widget_text"><!-- widget -->

                    <h1 class="title-widget"><?= $r2->judul;
                ?></h1>

                    <?php
                    if ($r2->gambar != '') {
                        echo '<a href="'.$r2->link.'">'.$this->custom->img_show('uploads/banner_original/', $r2->gambar, '').'</a>';
                    }
                echo $r2->konten;
                ?>

                </li>
            <?php 
            } ?>


            <li class="widget-container widget_text"><!-- widget -->

                <h1 class="title-widget">Statistik Pengunjung</h1>

                <div class="text-right">
                <ul class="list-unstyled">
                    <li><img src="<?=base_url('static/images/k3.gif');?>" /> Hari ini : <?=number_format($this->custom->konharini());?></li>
                    <li><img src="<?=base_url('static/images/k3.gif');?>" /> Kemarin : <?=number_format($this->custom->konkemarin());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Bulan ini : <?=number_format($this->custom->konbulan());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Tahun ini : <?=number_format($this->custom->kontahun());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Total Hits : <?=number_format($this->custom->kontotalhits());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Hits Count : <?=number_format($this->custom->konhits());?></li>
                    <li><img src="<?=base_url('static/images/k2.gif');?>" /> Now Online : <?=number_format($this->custom->nowonline());?></li>
                </ul>
                </div>
            </li>

        </ul>
        <!-- widgets end -->

    </div>
    <!-- inner custom column end -->

</div>
<!-- sidebar wrapper end -->

</div><!-- row end --><div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

    <div class="col-padded_ono col-shaded_ono"><!-- inner custom column -->

        <ul class="list-unstyled clear-margins"><!-- widgets -->

            <li class="widget-container widget_recent_news"><!-- widgets list -->

                <h1 class="title-widget">Berita Populer</h1>

                <ul class="list-unstyled">
                    <?php
                    foreach ($berita_populer->result() as $r1) {
                        ?>
                        <li class="recent-news-wrap news-no-summary">

                            <div class="recent-news-content clearfix">
                                <figure class="recent-news-thumb">
                                    <?= anchor('dinamispage/index/'.$this->custom->link_seo($r1->id_berita, $r1->judul_seo), $this->custom->img_show('uploads/berita_gambar_thumbnail/', $r1->gambar, 'class="attachment-thumbnail wp-post-image" title="'.$r1->judul.'"'));
                        ?>
                                </figure>
                                <div class="recent-news-text">
                                    <div class="recent-news-meta">
                                        <div
                                            class="recent-news-date"><?= $this->custom->format_tgl_text($r1->tanggal);
                        ?></div>
                                    </div>
                                    <p class="title-median">
                                        <?= anchor('dinamispage/index/'.$this->custom->link_seo($r1->id_berita, $r1->judul_seo), $r1->judul, 'title="'.$r1->judul.'"');
                        ?>
                                    </p>
                                </div>
                            </div>

                        </li>
                    <?php

                    }
                    ?>
                </ul>

            </li>
            <!-- widgets list end -->

            <?php
            if ($sosmed->facebook != '') {
                ?>
                <li class="widget-container widget_nav_menu"><!-- widget -->

                    <h1 class="title-widget">Facebook</h1>

                    <div class="fb-page" data-href="https://www.facebook.com/<?= $sosmed->facebook;
                ?>"
                         data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"></div>
                </li>
            <?php 
            } ?>

            <?php
            foreach ($banner->result() as $r2) {
                ?>
                <li class="widget-container widget_text"><!-- widget -->

                    <h1 class="title-widget-side"><?= $r2->judul;
                ?></h1>

                    <?php
                    if ($r2->gambar != '') {
                        echo '<a href="'.$r2->link.'">'.$this->custom->img_show('uploads/banner_original/', $r2->gambar, '').'</a>';
                    }
                echo $r2->konten;
                ?>

                </li>
            <?php 
            } ?>


            <li class="widget-container widget_text"><!-- widget -->

                <h1 class="title-widget-ono">Statistik Pengunjung</h1>

                <div class="text-left">
                <ul class="list-unstyled" style="margin:0 !important">
                    <li><img src="<?=base_url('static/images/k3.gif');?>" /> Hari ini : <?=number_format($this->custom->konharini());?></li>
                    <li><img src="<?=base_url('static/images/k3.gif');?>" /> Kemarin : <?=number_format($this->custom->konkemarin());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Bulan ini : <?=number_format($this->custom->konbulan());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Tahun ini : <?=number_format($this->custom->kontahun());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Total Hits : <?=number_format($this->custom->kontotalhits());?></li>
                    <li><img src="<?=base_url('static/images/k1.gif');?>" /> Hits Count : <?=number_format($this->custom->konhits());?></li>
                    <li><img src="<?=base_url('static/images/k2.gif');?>" /> Now Online : <?=number_format($this->custom->nowonline());?></li>
                </ul>
                </div>
            </li>

        </ul>
        <!-- widgets end -->

    </div>
    <!-- inner custom column end -->

</div>
<!-- sidebar wrapper end -->

</div><!-- row end -->