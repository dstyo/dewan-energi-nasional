<div class="row no-gutter gray-col-ono">
  <div class="col-lg-8 col-md-8">
    <div class="col-padded_ono" style="min-height: 1200px !important">
      <div class="row gutter1">
        <div class="col-lg-12 col-md-12">
          <div class="news-title-meta">
            <h1 class="page-title-detail">
              <?php echo strtoupper($subjudul);?>
            </h1>
          </div>
        </div>
      </div>
      <div class="row gutter k-equal-height1">
        <?php
          foreach ($konten->result() as $row) {
        ?>
        <div class="news-mini-wrap col-lg-6 col-md-6">
          <figure class="news-featured-image">
            <?= $this->custom->img_show('uploads/berita_gambar_original/', $row->gambar, 'style="width:100%;height:250px;" class="img-responsive"'); ?>
          </figure>
          <div class="page-title-meta">
            <h1 class="page-title">
              <?= anchor('dinamispage/index/'.$this->custom->link_seo($row->id_berita, $row->judul_seo), $row->judul, 'title="'.$row->judul.'"'); ?>
            </h1>
            <div class="news-meta">
              <span class="news-meta-date"><?= $this->custom->format_tgl_text($row->tanggal); ?></span>
              <span class="news-meta-category"><?= $row->tag; ?></span>
            </div>
          </div>
          <div class="news-summary">
            <p>
              <?= substr(strip_tags($row->isi_berita), 0, 150).'...'; ?>
              <?= anchor('dinamispage/index/'.$this->custom->link_seo($row->id_berita, $row->judul_seo), 'Selengkapnya', 'title="Baca selengkapnya" class="moretag"'); ?>
            </p>
          </div>
        </div>
        <?php
          }
        ?>
      </div>
      <div class="row gutter">
        <div class="col-lg-12">
          <?= $paging; ?>
        </div>
      </div>
    </div>
  </div>
