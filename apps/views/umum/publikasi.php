<div class="row">
  <div class="col-md-12">
    <br><br>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-heading">MENU PUBLIKASI</div>
          <!-- Nav tabs -->
          <div class="panel-body">
            <ul class="nav nav nav-pills nav-stacked" role="tablist" style="margin: 0px 0px 1.5em 0em;">
              <li role="presentation" class="active"><a href="#ExecutiveExport" aria-controls="ExecutiveExport" role="tab" data-toggle="tab">Executive Export</a></li>
              <li role="presentation"><a href="#Bookles" aria-controls="Bookles" role="tab" data-toggle="tab">Bookles</a></li>
              <li role="presentation"><a href="#Buku" aria-controls="Buku" role="tab" data-toggle="tab">Buku</a></li>
              <li role="presentation"><a href="#OutlookEnergy" aria-controls="OutlookEnergy" role="tab" data-toggle="tab">Outlook Energy</a></li>
              <li role="presentation"><a href="#Video" aria-controls="Video" role="tab" data-toggle="tab">Video</a></li>
              <li role="presentation"><a href="#Foto" aria-controls="Foto" role="tab" data-toggle="tab">Foto</a></li>
            </ul>
          </div>
      </div>
    </div>
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading">KATEGORI</div>
        <!-- Nav tabs -->
        <div class="panel-body">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" style="background-color:#FFF;" id="ExecutiveExport">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: bold;">Judul Buku</td>
                      <td style="font-weight: bold;">Tanggal</td>
                      <td style="font-weight: bold;">Status</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Pemanfaatan Energi Matahari</td>
                      <td>17 Oktober 2015</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                    <tr>
                      <td>Sosialisasi Hemat Energy 2020</td>
                      <td>05 September 2015</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                    <tr>
                      <td>Project Jatiluhur Masa Depan</td>
                      <td>23 Maret 2016</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                    <tr>
                      <td>Kincir Angin Belanda</td>
                      <td>02 April 2010</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" style="background-color:#FFF;" id="Bookles">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: bold;">Judul Buku</td>
                      <td style="font-weight: bold;">Tanggal</td>
                      <td style="font-weight: bold;">Status</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>PELUNCURAN HARI NUSANTARA TAHUN 2015</td>
                      <td>17 Oktober 2015</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td><a href="#">Lihat</a> | <a href="#">Download</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" style="background-color:#FFF;" id="Buku">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: bold;">Judul Buku</td>
                      <td style="font-weight: bold;">Tanggal</td>
                      <td style="font-weight: bold;">Status</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" style="background-color:#FFF;" id="OutlookEnergy">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: bold;">Judul Buku</td>
                      <td style="font-weight: bold;">Tanggal</td>
                      <td style="font-weight: bold;">Status</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                    <tr>
                      <td>Pemanfaatan Energy Turbin Angin</td>
                      <td>17 Oktober 2015</td>
                      <td>Lihat | Download</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" style="background-color:#FFF;" id="Video">
              <div id="gallery-video" style="display:none;">
  
              <div data-type="html5video"
                 data-title="Waimea cliff jump"
                 data-description="Waimea cliff jump description"
                 data-image="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-videomp4="sogCtOe8FFY"></div>

              
              <div data-type="html5video"
                 data-title="slip and slide -1000 feet"
                 data-description="slip and slide -1000 feet description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                 data-image="https://i.ytimg.com/vi/fvvoVD_5PHE/sddefault.jpg"
                   data-videomp4="fvvoVD_5PHE"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Cliff Slip and Slide!"
                 data-description="Cliff Slip and Slide description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                 data-image="https://i.ytimg.com/vi/sXk2AbdTe68/sddefault.jpg"
                   data-videomp4="sXk2AbdTe68"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Epic Hot Air Balloon Rope Swing"
                 data-description="Epic Hot Air Balloon Rope description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                 data-image="https://i.ytimg.com/vi/KEoXn34ilKY/sddefault.jpg"
                   data-videomp4="KEoXn34ilKY"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="BMX Rodeo at 1000 FPS"
                 data-description="BMX Rodeo at 1000 FPS description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                 data-image="https://i.ytimg.com/vi/rUi3X3G63dg/sddefault.jpg"
                   data-videomp4="rUi3X3G63dg"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Nepal - Adventures of Teamsupertramp"
                 data-description="Nepal - Adventures of description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                 data-image="https://i.ytimg.com/vi/oc1HDF2AyNE/sddefault.jpg"
                   data-videomp4="oc1HDF2AyNE"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Hoverboard in Real Life! In 4K!"
                 data-description="Hoverboard in Real Life! In 4K! description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                 data-image="https://i.ytimg.com/vi/gMaDhkNJA2g/sddefault.jpg"
                   data-videomp4="gMaDhkNJA2g"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Surfing Indoors! Flow Riding in 4K!"
                 data-description="Surfing Indoors! Flow Riding description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                 data-image="https://i.ytimg.com/vi/FWN08M0kg4c/sddefault.jpg"
                   data-videomp4="FWN08M0kg4c"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Bike Parkour -Streets of San Francisco!"
                 data-description="Bike Parkour -Streets of description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                 data-image="https://i.ytimg.com/vi/K9XCKP9KN7A/sddefault.jpg"
                   data-videomp4="K9XCKP9KN7A"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Parkour, Cops, and Donuts in 4K"
                 data-description="Parkour, Cops, and description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                 data-image="https://i.ytimg.com/vi/2dv4IP4Jd9w/sddefault.jpg"
                   data-videomp4="2dv4IP4Jd9w"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="YoYo Kid - World's Best YoYo Champion"
                 data-description="YoYo Kid - World's Best YoYo description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                 data-image="https://i.ytimg.com/vi/uHEs5JRFEUU/sddefault.jpg"
                   data-videomp4="uHEs5JRFEUU"
                   data-urlImage="" ></div>

              
              <div data-type="html5video"
                 data-title="Cliff Jumping Hawaii - Proof"
                 data-description="Cliff Jumping Hawaii - Proof description"
                 data-thumb="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                 data-image="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-videomp4="vSROSencBss"
                   data-urlImage="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg" ></div>      
          </div>
            </div>
            <div role="tabpanel" class="tab-pane" style="background-color:#FFF;" id="Foto">
              <div id="gallery" style="display:none;">
  
                <img alt="Preview Image 1"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image1.jpg"
                   data-description="Preview Image 1 Description">
                
                <img alt="Preview Image 2"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image2.jpg"
                   data-description="Preview Image 2 Description">
                   
                <img alt="Preview Image 3"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image3.jpg"
                   data-description="Preview Image 3 Description">
                   
                <img alt="Preview Image 4"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image4.jpg"
                   data-description="Preview Image 4 Description">
                
                <img alt="Preview Image 5"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image1.jpg"
                   data-description="Preview Image 5 Description">
                
                <img alt="Preview Image 6"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image2.jpg"
                   data-description="Preview Image 6 Description">
                   
                <img alt="Preview Image 7"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image3.jpg"
                   data-description="Preview Image 7 Description">
                   
                <img alt="Preview Image 8"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image4.jpg"
                   data-description="Preview Image 8 Description">
                   
                <img alt="Preview Image 9"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image1.jpg"
                   data-description="Preview Image 9 Description">
                
                <img alt="Preview Image 10"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image2.jpg"
                   data-description="Preview Image 10 Description">
                   
                <img alt="Preview Image 11"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image3.jpg"
                   data-description="Preview Image 11 Description">
                   
                <img alt="Preview Image 12"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image4.jpg"
                   data-description="Preview Image 12 Description">
                   
                <img alt="Preview Image 13"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb1.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image1.jpg"
                   data-description="Preview Image 13 Description">
                
                <img alt="Preview Image 14"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb2.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image2.jpg"
                   data-description="Preview Image 14 Description">
                   
                <img alt="Preview Image 15"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb3.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image3.jpg"
                   data-description="Preview Image 15 Description">
                   
                <img alt="Preview Image 16"
                   src="<?= base_url(); ?>uploads/media_foto/galeri/thumbs/thumb4.jpg"
                   data-image="<?= base_url(); ?>uploads/media_foto/galeri/big/image4.jpg"
                   data-description="Preview Image 16 Description">
                   
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
  })
</script>
