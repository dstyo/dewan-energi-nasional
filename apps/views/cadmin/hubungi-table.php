<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Hubungi Kami</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Hubungi Kami</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Pesan Masuk</h3>
            </div>
            <div class="box-body">

                <div class="row" style="margin-bottom: 10px;">
                    <?= form_open(cadmin.'/hubungi_table'); ?>
                    <div class="col-md-6 col-xs-6 text-left">
                        <div class="input-group">
                            <?= $this->custom->limit_form($limit); ?>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-sm pull-right inp-cari" placeholder="Cari" value="<?= $q; ?>"
                                   onchange="return submit();"/>

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>


                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Dari</th>
                        <th class="hidden-xs">Subjek</th>
                        <th class="hidden-xs">Tanggal</th>
                        <th>Status</th>
                        <th style="width: 80px;" class="text-right">Aksi</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $no = $offset;
                    foreach ($res->result() as $row):
                        $no++;
                        ?>
                        <tr>
                            <td><?= number_format($no); ?></td>
                            <td><?= $row->nama.' (<a href="mailto:'.$row->email.'">'.$row->email.'</a>)'; ?></td>
                            <td class="hidden-xs"><?= $row->subjek; ?></td>
                            <td class="hidden-xs"><?= $this->custom->format_tgl_text($row->tanggal).' '.$row->jam; ?></td>
                            <td><?= $this->custom->status_pesan($row->status); ?></td>
                            <td class="text-right">
                                <?= anchor(cadmin.'/hubungi_form/'.$row->id_hubungi, '<i class="fa fa-mail-reply"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Lihat dan Balas"'); ?>
                                <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left"
                                        title="Hapus"
                                        onclick="return confirm_alert('<?= base_url(cadmin.'/hubungi_delete/'.$row->id_hubungi); ?>');">
                                    <i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12 text-right">
                        <?= $paging; ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->