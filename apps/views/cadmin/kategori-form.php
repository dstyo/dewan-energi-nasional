<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kategori
            <small>Halaman</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/kategori_table', 'Data Kategori'); ?></li>
            <li class="active">Form Kategori</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $status = 1;
        $nama_kategori = set_value('nama_kategori');

        if ($edited) {
            $nama_kategori = $res->nama_kategori;
            $status = $res->status;
        }

        $ckcd1 = 'checked';
        $ckcd0 = '';

        if ($status == 1) {
            $ckcd0 = '';
            $ckcd1 = 'checked';
        }
        if ($status == 0) {
            $ckcd0 = 'checked';
            $ckcd1 = '';
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open(cadmin.'/kategori_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Kategori<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama_kategori" value="<?= $nama_kategori; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Status Aktif</label>

                    <div class="col-md-6">
                        <label>
                            <input type="radio" name="status" value="1" class="minimal" <?= $ckcd1; ?> />
                            Aktif
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="status" value="0" class="minimal" <?= $ckcd0; ?> />
                            Nonaktif
                        </label>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/kategori_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->