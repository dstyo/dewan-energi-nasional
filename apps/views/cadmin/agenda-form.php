<script type="text/javascript" src="<?=base_url();?>static/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.editorarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor emoticons | print preview",
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?=base_url();?>static/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Agenda</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/agenda_table', 'Data Agenda'); ?></li>
            <li class="active">Form Agenda</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $tema = set_value('tema');
        $isi_agenda = set_value('isi_agenda');
        $tempat = set_value('tempat');
        $jam = set_value('jam');
        $pengirim = set_value('pengirim');
        $tanggal = set_value('tanggal');
        $runningtext = set_value('runningtext');

        $tgl_mulai = '';
        $tgl_selesai = '';

        if ($edited) {
            $tema = $res->tema;
            $isi_agenda = $res->isi_agenda;
            $tempat = $res->tempat;
            $jam = $res->jam;
            $pengirim = $res->pengirim;
            $tgl_mulai = $res->tgl_mulai;
            $tgl_selesai = $res->tgl_selesai;
            $runningtext = $res->runningtext;
        }

        if ($tgl_mulai != '' || $tgl_selesai != '') {
            $tanggal = $this->custom->format_tgl_show($tgl_mulai).' - '.$this->custom->format_tgl_show($tgl_selesai);
        }

        $stbx = '';
        if ($runningtext == 0) {
            $stbx = 'checked';
        }
        $stox = '';
        if ($runningtext == 1) {
            $stox = 'checked';
        }

        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open(cadmin.'/agenda_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Tema<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="tema" value="<?= $tema; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Tempat<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="tempat" value="<?= $tempat; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Jam<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="jam" value="<?= $jam; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Tgl. Mulai - Selesai<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="tanggal" class="form-control pull-right daterange"
                                   value="<?= $tanggal; ?>"/>

                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Pengirim<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="pengirim" value="<?= $pengirim; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Isi Agenda</label>

                    <div class="col-md-10">
                        <textarea name='isi_agenda' class="editorarea"
                                  style='width: 100%; min-height: 400px;'><?= $isi_agenda; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Running Text </label>
                    <div class="col-md-10">
                        <label>
                            <input type="radio" value="0" <?=$stbx;?> name="runningtext" class="minimal" /> Tidak
                        </label>
                        &nbsp;&nbsp;
                        <label>
                            <input type="radio" value="1" <?=$stox;?> name="runningtext" class="minimal" /> Ya
                        </label>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/agenda_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->