<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Media
            <small>Foto</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Galeri Foto</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-6 col-xs-6 text-left">
                <h4>Galeri Foto</h4>
            </div>

            <div class="col-md-6 col-xs-6 text-right">
                <?= anchor(cadmin.'/galeri_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
            </div>
        </div>

        <div class="row" style="margin-bottom: 10px;">
            <?= form_open(cadmin.'/galeri_table'); ?>
            <div class="col-md-6 col-xs-6 text-left">
                <div class="input-group">
                    <?php
                    $list = array(4, 8, 16, 32, 48);
                    echo $this->custom->limit_form($limit, $list);
                    ?>
                </div>
            </div>

            <div class="col-md-6 col-xs-6">
                <div class="input-group">
                    <input type="text" name="q" class="form-control input-sm pull-right inp-cari" placeholder="Cari" value="<?= $q; ?>"
                           onchange="return submit();"/>

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
            <?= form_close(); ?>
        </div>

        <div class="row">
            <?php
            foreach ($res->result() as $row):
                ?>

                <div class="col-md-3">
                    <div class="box box-solid">
                        <div class="box-header">
                            <?= $this->custom->cut_text($row->jdl_gallery, 20); ?>
                            <div class="box-tools">
                                <?= anchor(cadmin.'/galeri_form/'.$row->id_gallery, '<i class="fa fa-pencil"></i>', 'class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Edit foto"'); ?>
                                <button type="button"
                                        onclick="return confirm_alert('<?= base_url(cadmin.'/galeri_delete/'.$row->id_gallery); ?>');"
                                        class="btn btn-box-tool" data-toggle="tooltip" data-placement="top"
                                        title="Hapus foto"><i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <a onclick="return img_zoom('<?=base_url().'uploads/media_foto/galeri/original/'.$row->gbr_gallery;?>', '<?=$row->jdl_gallery;?>', '<?=$row->jdl_album;?>', '<?=trim($row->keterangan);?>');" data-toggle="tooltip" data-placement="top" title="Lihat detail">
                            <?= $this->custom->img_show('uploads/media_foto/galeri/', $row->gbr_gallery, 'class="galeri-item" '); ?>
                            </a>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 text-center">
                <?= $paging; ?>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="show_image" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="image-title" class="modal-title"></h4>
            </div>
            <div class="modal-body no-padding" id="image-konten">

            </div>
            <div class="modal-footer" id="image-ket">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->