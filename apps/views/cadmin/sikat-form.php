<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Situs Terkait</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/sikat_table', 'Data Situs Terkait'); ?></li>
            <li class="active">Form Situs Terkait</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $nama = set_value('nama');
        $link = set_value('link');

        if ($edited) {
            $nama = $res->nama;
            $link = $res->link;
        }

        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/sikat_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Situs<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama" value="<?= $nama; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Link<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="link" value="<?= $link; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/sikat_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->