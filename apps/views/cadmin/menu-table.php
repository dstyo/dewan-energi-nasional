<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pengaturan
            <small>Menu Web</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Menu Web</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="row">

            <div class="col-md-4">
                <div class="box box-solid">

                    <div class="box-header with-border">
                        <i class="fa fa-sort-amount-desc"></i>
                        <h3 class="box-title">Struktur Menu Web</h3>
                    </div>

                    <div class="box-body">
                        <?=$this->custom->menu_one(0, 0);?>
                    </div>

                </div>
            </div>

            <div class="col-md-8">

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Menu Web</h3>

                        <div class="box-tools pull-right">
                            <?= anchor(cadmin.'/menu_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
                        </div>
                    </div>

                    <div class="box-body">

                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
								<th>No Urut</th>
                                <th>Menu</th>
                                <!-- <th class="hidden-xs">Link</th> -->
                                <th class="hidden-xs">Parent</th>
                                <th>Status</th>
                                <th style="width: 100px;" class="text-right">Aksi</th>
                            </tr>
                            </thead>

                            <tbody>

                            <?php
                            $no = 0;
                            foreach ($res->result() as $row):
                                $no++;
                                ?>
                                <tr>
                                    <td><?= number_format($no); ?></td>
									<td><?= $row->sort; ?></td>
                                    <td><?= $row->menu; ?></td>
                                    <!-- <td class="hidden-xs"><?= $row->link; ?></td> -->
                                    <td class="hidden-xs"><?= $this->custom->parent_menu($row->parent); ?></td>
                                    <td><?= $this->custom->status_aktif($row->aktif); ?></td>

                                    <td class="text-right">
                                        <?php
                                        if ($row->aktif == 1) {
                                            echo anchor(cadmin.'/menu_blok/'.$row->id_menu.'/0', '<i class="fa fa-ban"></i>', 'class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left" title="Nonaktif"');
                                        } else {
                                            echo anchor(cadmin.'/menu_blok/'.$row->id_menu.'/1', '<i class="fa fa-check"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Aktif"');
                                        }
                                        ?>
                                        <?= anchor(cadmin.'/menu_form/'.$row->id_menu, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"'); ?>
                                        <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left"
                                                title="Hapus"
                                                onclick="return confirm_alert('<?= site_url(cadmin.'/menu_delete/'.$row->id_menu); ?>');">
                                            <i class="fa fa-remove"></i></button>
                                    </td>
                                    
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->