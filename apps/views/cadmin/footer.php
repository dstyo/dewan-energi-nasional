<footer class="main-footer">
    <strong>Copyright &copy; 2015</strong> All rights reserved.
</footer>

<div id="confirm_alert" class="modal modal-danger">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-warning"></i> Peringatan</h4>
            </div>
            <div class="modal-body">
                <p>Anda akan melakukan penghapusan data. Anda yakin dengan hal ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" id="confirm_cancel">Close</button>
                <a href="" class="btn btn-outline" id="confirm_link"><i class="fa fa-check"></i> Ya</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- jQuery 2.1.4 -->
<script src="<?= base_url(); ?>static/cadmin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?= base_url(); ?>static/cadmin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="<?= base_url(); ?>static/cadmin/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>static/cadmin/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url(); ?>static/cadmin/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="<?= base_url(); ?>static/cadmin/plugins/select2/select2.full.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>static/cadmin/plugins/slimScroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<!-- FastClick -->
<script src='<?= base_url(); ?>static/cadmin/plugins/fastclick/fastclick.min.js'></script>

<!-- AdminLTE App -->
<script src="<?= base_url(); ?>static/cadmin/dist/js/app.min.js" type="text/javascript"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url(); ?>static/cadmin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>


<script>
    $(function () {

        $(".select2").select2();

        $(".my-colorpicker1").colorpicker();

        //Date range picker
        $('.daterange').daterangepicker({ format: 'DD/MM/YYYY' });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('#confirm_cancel').click(function () {
            $('#confirm_alert').hide();
        });

        $('#userfile').change(function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#view_img').attr('src', e.target.result).width(150);
            };

            var resspl = this.files[0].type.split("/");
            if (resspl[0] == "image") {
                reader.readAsDataURL(this.files[0]);
                $('#view_img').show();
            } else {
                $('#view_img').hide();
            }
        });

        $('.close').click(function () {
            $('.modal').hide();
        });
/*
        $('.skin-blue').on('change', '.mce-textbox', function() {
            alert('aa');
        });
*/
    });

    function confirm_alert(url) {
        $('#confirm_link').attr('href', url);
        $('#confirm_alert').show();
    }

    function lihat_vid(file, judul) {
        $('#video-title').html(judul);
        $('#video-konten').html('<video style="width: 100%; height: 350px" controls>' +
        '<source class="file-vid" src="'+file+'" type="video/mp4"/>' +
        '<source class="file-vid" src="'+file+'" type="video/ogg"/>' +
        'Maaf, browser kamu tidak suport untuk memutar video ini, silahkan update browsernya dulu.' +
        '</video>');
        $('#show_video').show();
    }

    function img_zoom(file, judul, kategori, keterangan) {
        $('#image-title').html(kategori + ' - ' + '<small>' + judul + '</small>');
        $('#image-konten').html('<img src="'+file+'" style="width:100%;max-height:350px;" />');
        $('#image-ket').html(keterangan);
        $('#show_image').show();
    }

    function preview_struktur() {
        $('#struktur_preview').show();
    }

    function show_detail_k(_id) {
        $('#tr-'+_id).fadeToggle('slow');
    }

</script>
</body>
</html>
