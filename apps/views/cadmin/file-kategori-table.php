<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kategori
            <small>File</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Kategori</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Kategori</h3>

                <div class="box-tools pull-right">
                    <?= anchor(cadmin.'/filekategori_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
                </div>
            </div>
            <div class="box-body">

                <div class="row" style="margin-bottom: 10px;">
                    <?= form_open(cadmin.'/filekategori_table'); ?>
                    <div class="col-md-6 col-xs-6 text-left">
                        <div class="input-group">
                            <?= $this->custom->limit_form($limit); ?>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-sm pull-right inp-cari"
                                   placeholder="Cari" value="<?= $q; ?>" onchange="return submit();"/>

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>


                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kategori</th>
                        <th>Link</th>
                        <th>Status</th>
                        <th style="width: 50px;" class="text-right">Aksi</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $no = $offset;
                    foreach ($res->result() as $row):
                        $no++;
                        ?>
                        <tr>
                            <td><?= number_format($no); ?></td>
                            <td><?= $row->nama_kategori; ?></td>
                            <td>media/dokumen/<?= $this->custom->link_seo($row->id_kategori_file, $row->kategori_seo); ?></td>
                            <td><?= $this->custom->status_aktif($row->status); ?></td>
                            <td class="text-right">
                                <?= anchor(cadmin.'/filekategori_form/'.$row->id_kategori_file, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12 text-right">
                        <?= $paging; ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->