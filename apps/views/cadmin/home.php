


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Dashboard</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?=number_format($tot_page);?></h3>
                        <p>Halaman Statis</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file"></i>
                    </div>
                    <?=anchor(cadmin.'/statis_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');?>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?=number_format($tot_berita);?></h3>
                        <p>Halaman Dinamis</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <?=anchor(cadmin.'/berita_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');?>
                </div>
            </div><!-- ./col -->

            <?php
            if ($this->custom->cek_level(array('A'))) {
                ?>

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?=number_format($tot_berita);
                ?></h3>
                        <p>Agenda</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-book"></i>
                    </div>
                    <?=anchor(cadmin.'/agenda_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue-active">
                    <div class="inner">
                        <h3><?=number_format($tot_hubungi);
                ?></h3>
                        <p>Pesan Masuk</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-inbox"></i>
                    </div>
                    <?=anchor(cadmin.'/hubungi_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->


            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3><?=number_format($tot_foto);
                ?></h3>
                        <p>Foto</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-image"></i>
                    </div>
                    <?=anchor(cadmin.'/galeri_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3><?=number_format($tot_video);
                ?></h3>
                        <p>Video</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-film"></i>
                    </div>
                    <?=anchor(cadmin.'/video_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua-active">
                    <div class="inner">
                        <h3><?=number_format($tot_file);
                ?></h3>
                        <p>File</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clipboard"></i>
                    </div>
                    <?=anchor(cadmin.'/file_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?=number_format($tot_user);
                ?></h3>
                        <p>Users</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <?=anchor(cadmin.'/user_table', 'Lihat <i class="fa fa-arrow-circle-right"></i>', 'class="small-box-footer"');
                ?>
                </div>
            </div><!-- ./col -->
            <?php

            }
            ?>

        </div><!-- /.row -->

        <hr/>

        <!-- Info boxes -->
        <div class="row">

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Pengunjung Hari Ini</span>
                        <span class="info-box-number"><?=number_format($this->custom->konharini());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Pengunjung Bulan Ini</span>
                        <span class="info-box-number"><?=number_format($this->custom->konbulan());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Pengunjung Tahun Ini</span>
                        <span class="info-box-number"><?=number_format($this->custom->kontahun());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->



        </div><!-- /.row -->

        <div class="row">

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Hits</span>
                        <span class="info-box-number"><?=number_format($this->custom->kontotalhits());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Hits Count</span>
                        <span class="info-box-number"><?=number_format($this->custom->konhits());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Now Online</span>
                        <span class="info-box-number"><?=number_format($this->custom->nowonline());?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->



        </div><!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->