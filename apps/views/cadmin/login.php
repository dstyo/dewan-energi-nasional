<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>DEN</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?= base_url(); ?>static/cadmin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="<?= base_url(); ?>static/cadmin/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Theme style -->
    <link href="<?= base_url(); ?>static/cadmin/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>

    <link href="<?= base_url(); ?>static/cadmin/custom.css" rel="stylesheet" type="text/css"/>

    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url(); ?>uploads/favicon/favicon.png" type="image/x-icon">

</head>
<body class="login-page">
<div class="login-box">

    <div class="login-logo">
        <a href="#"><img src="<?=base_url();?>static/images/logo1.png" style="height: 100px;" /></a>
    </div>
    <!-- /.login-logo -->

    <div class="login-box-body">
        <?php
        echo '<p class="login-box-msg">Silahkan login dengan akun anda.</p>';
        $err = validation_errors();
        if ($msg == 'invalid') {
            $msg = 'Akun tidak sesuai, atau sedang diblokir.';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert alert-danger alert-dismissable callout">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		'.$err.$msg.'</div>';
        }

        echo form_open(cadmin.'/login');
        ?>
        <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Username"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>

        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-sign-in"></i> Login
                </button>
            </div>
        </div>
        <?= form_close(); ?>


    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="<?= base_url(); ?>static/cadmin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?= base_url(); ?>static/cadmin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>
