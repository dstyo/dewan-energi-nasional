<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pengaturan
            <small>Menu Web</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/menu_table', 'Data Menu Web'); ?></li>
            <li class="active">Form Menu Web</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                    $i_alert.$err.$msg.
                '</div>';
        }

        $menu = set_value('menu');
        $link = set_value('link');
        $parent = set_value('parent');
        $sort = set_value('sort');
        $keterangan = set_value('keterangan');

        if ($edited) {
            $menu = $res->menu;
            $link = $res->link;
            $parent = $res->parent;
            $sort = $res->sort;
            $keterangan = $res->keterangan;
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open(cadmin.'/menu_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Label menu<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="menu" value="<?=$menu;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Keterangan menu<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="keterangan" value="<?=$keterangan;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Link<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="link" value="<?=$link;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Parent<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <select class="form-control" name="parent">
                            <option value="0">Main Menu</option>
                            <?php
                            foreach ($menu_list->result() as $row) {
                                if ($id != $row->id_menu) {
                                    if ($row->id_menu == $parent) {
                                        echo '<option selected value="'.$row->id_menu.'">'.$row->menu.'</option>';
                                    } else {
                                        echo '<option value="'.$row->id_menu.'">'.$row->menu.'</option>';
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">No Urut<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="number" name="sort" value="<?=$sort;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/menu_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->