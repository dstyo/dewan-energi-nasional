<script type="text/javascript" src="<?=base_url();?>static/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.editorarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor emoticons | print preview",
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?=base_url();?>static/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Banner</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/banner_table', 'Data Banner'); ?></li>
            <li class="active">Form Banner</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = str_replace('%20', ' ', $msg);
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $judul = set_value('judul');
        $link = set_value('link');
        $gambar_old = set_value('gambar_old');
        $tanggal = set_value('tanggal');
        $konten = set_value('konten');

        $tgl_mulai = '';
        $tgl_selesai = '';

        if ($edited) {
            $judul = $res->judul;
            $link = $res->link;
            $gambar_old = $res->gambar;
            $tgl_mulai = $res->tgl_mulai;
            $tgl_selesai = $res->tgl_selesai;
            $konten = $res->konten;
        }

        if ($tgl_mulai != '' || $tgl_selesai != '') {
            $tanggal = $this->custom->format_tgl_show($tgl_mulai).' - '.$this->custom->format_tgl_show($tgl_selesai);
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/banner_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Tgl. Mulai - Selesai<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="tanggal" class="form-control pull-right daterange"
                                   value="<?= $tanggal; ?>"/>

                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Judul<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="judul" value="<?= $judul; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Link</label>

                    <div class="col-md-6">
                        <input type="text" name="link" value="<?= $link; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

				<input type="hidden" name="posisi" value="side" />
				<!--
                <div class="form-group">
                    <label class="col-md-2 control-label">Poisi Banner</label>

                    <div class="col-md-6">
                        <select name="posisi" class="form-control">
                            <option value="side">Disamping</option>
                            <option value="buttom">Dibawah</option>
                        </select>
                    </div>
                </div>
				-->

                <div class="form-group">
                    <label class="col-md-2 control-label">Gambar</label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small>
                        <br/>
                        <input type="hidden" name="gambar_old" value="<?= $gambar_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gambar_old != '') {
                            $uriimg = base_url().'uploads/banner_thumbnail/'.$gambar_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px; <?= $imghide; ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Text Konten</label>

                    <div class="col-md-10">
                        <textarea name='konten' class="editorarea"
                                  style='width: 100%; min-height: 400px;'><?= $konten; ?></textarea>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/banner_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->