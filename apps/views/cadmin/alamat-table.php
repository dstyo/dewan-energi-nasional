<script type="text/javascript" src="<?=base_url();?>static/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.editorarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor emoticons | print preview",
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?=base_url();?>static/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Hubungi Kami</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Alamat dan Sosial Media</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = str_replace('%20', ' ', $msg);
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $alamat = $res->alamat;
        $facebook = $res->facebook;
        $twitter = $res->twitter;
        $googleplus = $res->googleplus;
        $email = $res->email;
        $phone = $res->phone;
        $fax = $res->fax
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/alamat_table', 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Alamat<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <textarea name='alamat' class="editorarea"
                                  style='width: 100%; min-height: 250px;'><?= $alamat; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Facebook<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="facebook" value="<?=$facebook;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Twitter<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="twitter" value="<?=$twitter;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Google Plus<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="googleplus" value="<?=$googleplus;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Email<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="email" value="<?=$email;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Phone<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="phone" value="<?=$phone;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Fax<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="fax" value="<?=$fax;?>" class="form-control" placeholder="tulis disini" />
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" name="simpan" value="simpan" />
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->