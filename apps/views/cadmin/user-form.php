<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/user_table', 'Data User'); ?></li>
            <li class="active">Form User</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                    $i_alert.$err.$msg.
                '</div>';
        }

        $username = set_value('username');
        $nama_lengkap = set_value('nama_lengkap');
        $email = set_value('email');
        $level = set_value('level');
        $no_telp = set_value('no_telp');
        $password = '';

        if ($edited) {
            $username = $res->username;
            $nama_lengkap = $res->nama_lengkap;
            $email = $res->email;
            $level = $res->level;
            $no_telp = $res->no_telp;
            $password = '';
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open(cadmin.'/user_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Lengkap<sup class="text-danger">*</sup> </label>
                    <div class="col-md-6">
                        <input type="text" name="nama_lengkap" value="<?=$nama_lengkap;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Email<sup class="text-danger">*</sup></label>
                    <div class="col-md-6">
                        <input type="text" name="email" value="<?=$email;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">No. Telp/HP</label>
                    <div class="col-md-6">
                        <input type="text" name="no_telp" value="<?=$no_telp;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Username<sup class="text-danger">*</sup></label>
                    <div class="col-md-6">
                        <input type="text" name="username" value="<?=$username;?>" class="form-control" placeholder="Tulis disini"/>
                        <input type="hidden" name="username_old" value="<?=$username;?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Password<sup class="text-danger">*</sup></label>
                    <div class="col-md-6">
                        <input type="password" name="password" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/user_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->