<div class="content-wrapper">
  <section class="content-header">
    <h1>Halaman Kategori<small>Publikasi</small></h1>
    <ol class="breadcrumb">
      <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
      <li class="active">Data Publikasi</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Publikasi</h3>
        <div class="box-tools pull-right">
          <?= anchor(cadmin.'/pengumuman_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
        </div>
      </div>
      <div class="box-body">
        <div class="row" style="margin-bottom: 10px;">
          <?= form_open(cadmin.'/pengumuman_table'); ?>
          <div class="col-md-6 col-xs-6 text-left">
            <div class="input-group">
              <?= $this->custom->limit_form($limit); ?>
            </div>
          </div>
          <div class="col-md-6 col-xs-6">
            <div class="input-group">
              <input type="text" name="q" class="form-control input-sm pull-right inp-cari" placeholder="Cari" value="<?= $q; ?>"onchange="return submit();"/>
              <div class="input-group-btn">
                <button type="submit" class="btn btn-sm btn-default">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
          </div>
          <?= form_close(); ?>
        </div>
        <table class="table table-striped table-hover table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Kategori</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = $offset;
              foreach ($res->result() as $row):
                $no++;
            ?>
            <tr>
              <td><?= number_format($no); ?></td>
              <td><?= $row->publikasi_kategori; ?></td>
              <td class="text-right">
                <?php
                  if (($this->session->userdata('level') == 'D' && $row->id_drafter == $this->session->userdata('id_user') && $row->status == 2) or ($this->session->userdata('level') != 'D')) {
                ?>
                <?= anchor(cadmin.'/kategori_publikasi_form/'.$row->id_publikasi_kategori, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"'); ?>

                <?php
                  }
                ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-12 text-right">
          <?= $paging; ?>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
