<div class="content-wrapper">
  <section class="content-header">
    <h1>Halaman <small>Banner</small></h1>
    <ol class="breadcrumb">
      <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
      <li><?= anchor(cadmin.'/banner_table', 'Data Banner'); ?></li>
      <li class="active">Form Banner</li>
    </ol>
  </section>
  <section class="content">
    <?php
      $err = validation_errors();
      $c_alert = 'alert-danger';
      $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
      if ($msg == 'error') {
        $msg = 'Prosess gagal, silahkan coba kembali.';
      } elseif ($msg == 'success') {
        $msg = 'Proses berhasil.';
        $c_alert = 'alert-success';
        $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
      } else {
        $msg = str_replace('%20', ' ', $msg);
      }
      if (strlen($msg) > 0 || strlen($err) > 0) {
        echo '<div class="alert '.$c_alert.' alert-dismissable callout">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
              $i_alert.$err.$msg.
              '</div>';
      }
      $judul = set_value('judul');
      $link = set_value('link');
      $gambar_old = set_value('gambar_old');
      $tanggal = set_value('tanggal');
      $konten = set_value('konten');
    ?>

    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><?= $subjudul; ?></h3>
      </div>
      <?= form_open_multipart(cadmin.'/banner_form2/', 'class="form-horizontal"'); ?>
      <div class="box-body">
        <div class="form-group">
          <label class="col-md-2 control-label">Tgl. Mulai - Selesai<sup class="text-danger">*</sup></label>
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" name="tanggal" class="form-control pull-right daterange" value="<?= $tanggal; ?>"/>
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
          </div>
        </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Judul<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="judul" value="<?= $judul; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Link</label>

                    <div class="col-md-6">
                        <input type="text" name="link" value="<?= $link; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Text Konten</label>

                    <div class="col-md-6">
                        <textarea name='konten' class="form-control"><?= $konten; ?></textarea>
                    </div>
                </div>

                <div class="form-group">

                    <label class="col-md-2 control-label">Warna Latar</label>

                    <div class="col-md-6">
                        <input type="text" name="bgcolor" class="form-control my-colorpicker1"/>
                    </div>

                </div>

                <div class="form-group">

                    <label class="col-md-2 control-label">Warna Tulisan</label>

                    <div class="col-md-6">
                        <input type="text" name="fgcolor" class="form-control my-colorpicker1"/>
                    </div>

                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/banner_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
