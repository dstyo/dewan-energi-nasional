<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Struktur Organisasi Setjen</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/struktur_table2', 'Data Struktur Organisasi'); ?></li>
            <li class="active">Form Struktur Organisasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                    $i_alert.$err.$msg.
                '</div>';
        }

        $nama_organisasi = set_value('nama_organisasi');
        $parent = set_value('parent');
        $sort = set_value('sort');

        if ($edited) {
            $nama_organisasi = $res->nama_organisasi;
            $parent = $res->parent;
            $sort = $res->sort;
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open(cadmin.'/struktur_form2/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Jabtan<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama_organisasi" value="<?=$nama_organisasi;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Posisi<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <select class="form-control" name="parent">
                            <option value="0">Teratas</option>
                            <?php
                            foreach ($menu_list->result() as $row) {
                                if ($id != $row->id_organisasi) {
                                    if ($row->id_organisasi == $parent) {
                                        echo '<option selected value="'.$row->id_organisasi.'">Dibawah '.$row->nama_organisasi.'</option>';
                                    } else {
                                        echo '<option value="'.$row->id_organisasi.'">Dibawah '.$row->nama_organisasi.'</option>';
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">No Urut<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="number" name="sort" value="<?=$sort;?>" class="form-control" placeholder="Tulis disini"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/struktur_table2', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->