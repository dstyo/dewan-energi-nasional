<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Struktur Organisasi Setjen <small>Anggota</small></h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/anggota_table2', 'Data Pejabat Setjen DEN'); ?></li>
            <li class="active">Form Pejabat Setjen DEN</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $id_organisasi_setjen = set_value('id_organisasi_setjen');
        $jabatan = set_value('jabatan');
        $nama_pejabat = set_value('nama_pejabat');
        $deskripsi = set_value('deskripsi');
        $gambar_old = set_value('gambar_old');

        if ($edited) {
            $id_organisasi_setjen = $res->id_organisasi_setjen;
            $jabatan = $res->jabatan;
            $nama_pejabat = $res->nama_pejabat;
            $deskripsi = $res->deskripsi;
            $gambar_old = $res->namafile;
        }

        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/anggota_form2/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Kode<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="id_organisasi_setjen" value="<?= $id_organisasi_setjen; ?>" class="form-control"
                               placeholder=""/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Jabatan<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="jabatan" value="<?= $jabatan; ?>" class="form-control"
                               placeholder=""/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Pejabat<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama_pejabat" value="<?= $nama_pejabat; ?>" class="form-control"
                               placeholder=""/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Profil</label>

                    <div class="col-md-6">
                        <textarea name='deskripsi' class="form-control" rows="5"><?= $deskripsi; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Foto</label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small><br/>
                        <input type="hidden" name="gambar_old" value="<?= $gambar_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gambar_old != '') {
                            $uriimg = base_url().'uploads/foto_anggota_thumbnail/'.$gambar_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px;<?= $imghide; ?>"/>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/anggota_table2', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->