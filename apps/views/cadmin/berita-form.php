<script type="text/javascript" src="<?=base_url();?>static/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.editorarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor emoticons | print preview",
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?=base_url();?>static/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Halaman <small>Dinamis</small></h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/berita_table', 'Data Halaman'); ?></li>
            <li class="active">Form Halaman</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $headline = 'Y';

        $nama_kategori = set_value('nama_kategori');
        $judul = set_value('judul');
        $id_kategori = set_value('id_kategori');
        $isi_berita = set_value('isi_berita');
        $headline = set_value('headline');
        $tag = set_value('tag');
        $gambar_old = set_value('gambar_old');
        $komen = set_value('komen');
        $status = set_value('status');
        $runningtext = set_value('runningtext');
        $slider = set_value('slider');
        $document = set_value('document');

        if ($edited) {
            $judul = $res->judul;
            $id_kategori = $res->id_kategori;
            $isi_berita = $res->isi_berita;
            $headline = $res->headline;
            $tag = explode(' ', $res->tag);
            $gambar_old = $res->gambar;
            $komen = $res->komen;
            $status = $res->status_berita;
            $runningtext = $res->runningtext;
            $slider = $res->slider;
            $document = $res->document;
        }

        $stb = '';
        if ($status == 1) {
            $stb = 'checked';
        }
        $sto = '';
        if ($status == 2) {
            $sto = 'checked';
        }

        $stbx = '';
        if ($runningtext == 0) {
            $stbx = 'checked';
        }
        $stox = '';
        if ($runningtext == 1) {
            $stox = 'checked';
        }

        $ckcd1 = 'checked';
        $ckcd0 = '';

        if ($headline == 'Y') {
            $ckcd0 = '';
            $ckcd1 = 'checked';
        }
        if ($headline == 'N') {
            $ckcd0 = 'checked';
            $ckcd1 = '';
        }
        $sldr1 = '';
        if ($slider == 1) {
            $sldr1 = 'checked';
        }
        $sldr0 = '';
        if ($slider == 0) {
            $sldr0 = 'checked';
        }

        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/berita_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Judul<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="judul" value="<?= $judul; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Kategori<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <select name="id_kategori" class="form-control">
                            <?php
                            echo '<option value="">Pilih</option>';
                            foreach ($kategori->result() as $row1):
                                if ($row1->id_kategori == $id_kategori) {
                                    echo '<option selected value="'.$row1->id_kategori.'">'.$row1->nama_kategori.'</option>';
                                } else {
                                    echo '<option value="'.$row1->id_kategori.'">'.$row1->nama_kategori.'</option>';
                                }
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Headline<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <label>
                            <input type="radio" name="headline" value="Y" class="minimal" <?= $ckcd1; ?> />
                            Ya
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="headline" value="N" class="minimal" <?= $ckcd0; ?> />
                            Tidak
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Isi Berita<sup class="text-danger">*</sup></label>

                    <div class="col-md-10">
                        <textarea name='isi_berita' class="editorarea"
                                  style='width: 100%; min-height: 400px;'><?= $isi_berita; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Gambar</label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small><br/>
                        <input type="hidden" name="gambar_old" value="<?= $gambar_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gambar_old != '') {
                            $uriimg = base_url().'uploads/berita_gambar_thumbnail/'.$gambar_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px;<?= $imghide; ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Tag/Label</label>

                    <div class="col-md-6">
                        <select name="tag[]" class="form-control select2" multiple="multiple"
                                data-placeholder="Isi tag/label">
                            <?php
                            echo '<option value="">Pilih</option>';
                            foreach ($tag_label->result() as $row2):
                                if (in_array($row2->tag_seo, $tag)) {
                                    echo '<option selected value="'.$row2->id_tag.'">'.$row2->nama_tag.'</option>';
                                } else {
                                    echo '<option value="'.$row2->tag_seo.'">'.$row2->nama_tag.'</option>';
                                }
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Running Text </label>
                    <div class="col-md-6">
                        <label>
                            <input type="radio" value="0" <?=$stbx;?> name="runningtext" class="minimal" /> Tidak
                        </label>
                        &nbsp;&nbsp;
                        <label>
                            <input type="radio" value="1" <?=$stox;?> name="runningtext" class="minimal" /> Ya
                        </label>
                    </div>
                </div>

                <?php
                if ($this->session->userdata('level') != 'D') {
                    ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Status </label>
                        <div class="col-md-6">
                            <label>
                                <input type="radio" value="1" <?=$stb;
                    ?> name="status" class="minimal" /> Terbit
                            </label>
                            &nbsp;&nbsp;
                            <label>
                                <input type="radio" value="2" <?=$sto;
                    ?> name="status" class="minimal" /> Tolak
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Slider<sup class="text-danger">*</sup></label>

                        <div class="col-md-6">
                            <label>
                                <input type="radio" name="slider" value="1" class="minimal" <?= $sldr1;
                    ?> />
                                Aktif
                            </label>
                            &nbsp;&nbsp;&nbsp;
                            <label>
                                <input type="radio" name="slider" value="0" class="minimal" <?= $sldr0;
                    ?> />
                                Tidak Aktif
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">File Document</label>

                        <div class="col-md-6">
                            <input type="file" id="userfile" name="" class="form-control"/>
                            <small class="text-info">*) Tipe document harus .doc/.xls/.pdf dan besar file max: 2MB.</small><br/>
                            <input type="hidden" name="" value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan </label>
                        <div class="col-md-6">
                            <input type="text" name="komen" value="<?=$komen;
                    ?>" class="form-control" placeholder="Tulis disini"/>
                        </div>
                    </div>
                <?php
                } else {
                    ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan </label>
                        <div class="col-md-6">
                            <input type="text" name="komen" readonly value="<?=$komen;
                    ?>" class="form-control" placeholder="Tulis disini"/>
                        </div>
                    </div>
                <?php

                } ?>



            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/berita_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
