<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data User</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar User</h3>

                <div class="box-tools pull-right">
                    <?= anchor(cadmin.'/user_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
                </div>
            </div>
            <div class="box-body">

                <div class="row" style="margin-bottom: 10px;">
                    <?= form_open(cadmin.'/user_table'); ?>
                    <div class="col-md-6 col-xs-6 text-left">
                        <div class="input-group">
                            <?= $this->custom->limit_form($limit); ?>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-sm pull-right inp-cari"
                                   placeholder="Cari" value="<?= $q; ?>"
                                   onchange="return submit();"/>

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>


                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th class="hidden-xs">Username</th>
                        <th>Nama Lengkap</th>
                        <th class="hidden-xs">Email</th>
                        <th class="hidden-xs">No.Telp/HP</th>
                        <th>Level</th>
                        <th>Blokir</th>
                        <th style="width: 100px;" class="text-right">Aksi</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $no = $offset;
                    foreach ($res->result() as $row):
                        $no++;
                        ?>
                        <tr>
                            <td><?= number_format($no); ?></td>
                            <td class="hidden-xs"><?= $row->username; ?></td>
                            <td><?= $row->nama_lengkap; ?></td>
                            <td class="hidden-xs"><a href="mailto:<?= $row->email; ?>"><?= $row->email; ?></a></td>
                            <td class="hidden-xs"><?= $row->no_telp; ?></td>
                            <td><?= $this->custom->level_user($row->level); ?></td>
                            <td><?= $this->custom->blokir($row->blokir); ?></td>
                            <td class="text-right">
                                <?php
                                if ($row->level != 'A') {
                                    if ($row->level == 'D') {
                                        echo anchor(cadmin.'/user_editor/'.$row->id_user, '<i class="fa fa-user"></i>', 'class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" title="Jadikan editor"');
                                    } else {
                                        echo '&nbsp;';
                                    }
                                    echo '&nbsp;';
                                    if ($row->blokir == 'N') {
                                        echo anchor(cadmin.'/user_blok/'.$row->id_user.'/Y', '<i class="fa fa-ban"></i>', 'class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left" title="Blokir"');
                                    } else {
                                        echo anchor(cadmin.'/user_blok/'.$row->id_user.'/N', '<i class="fa fa-check"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Unblokir"');
                                    }
                                    echo '&nbsp;';
                                    echo anchor(cadmin.'/user_form/'.$row->id_user, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"');
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12 text-right">
                        <?= $paging; ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->