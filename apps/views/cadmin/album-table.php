<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Media
            <small>Foto</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Album Foto</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-6 col-xs-6 text-left">
                <h4>Daftar Album Foto</h4>
            </div>

            <div class="col-md-6 col-xs-6 text-right">
                <?= anchor(cadmin.'/album_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
            </div>
        </div>

        <div class="row" style="margin-bottom: 10px;">
            <?= form_open(cadmin.'/album_table'); ?>
            <div class="col-md-6 col-xs-6 text-left">
                <div class="input-group">
                    <?php
                    $list = array(4, 8, 16, 32, 48);
                    echo $this->custom->limit_form($limit, $list);
                    ?>
                </div>
            </div>

            <div class="col-md-6 col-xs-6">
                <div class="input-group">
                    <input type="text" name="q" class="form-control input-sm pull-right inp-cari" placeholder="Cari" value="<?= $q; ?>"
                           onchange="return submit();"/>

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
            <?= form_close(); ?>
        </div>

        <div class="row">
            <?php
            foreach ($res->result() as $row):
                ?>

                <div class="col-md-3">
                    <div class="box box-solid">
                        <div class="box-header">
                            <?= $this->custom->cut_text($row->jdl_album, 15); ?>
                            <div class="box-tools">
                                <?php
                                if ($row->aktif == 1) {
                                    echo anchor(cadmin.'/album_noni/'.$row->id_album.'/0', '<i class="fa fa-ban"></i>', 'class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Nonaktifkan"');
                                } else {
                                    echo anchor(cadmin.'/album_noni/'.$row->id_album.'/1', '<i class="fa fa-check"></i>', 'class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Aktifkan"');
                                }
                                ?>

                                <?= anchor(cadmin.'/album_form/'.$row->id_album, '<i class="fa fa-pencil"></i>', 'class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Edit album"'); ?>
                                <button type="button"
                                        onclick="return confirm_alert('<?= base_url(cadmin.'/album_delete/'.$row->id_album); ?>');"
                                        class="btn btn-box-tool" data-toggle="tooltip" data-placement="top"
                                        title="Hapus album"><i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <a href="<?= base_url(cadmin.'/foto_album/'.$row->jdl_album); ?>" data-toggle="tooltip"
                               data-placement="top" title="Lihat galeri foto">
                                <?= $this->custom->img_show('uploads/media_foto/album/', $row->gbr_album, 'class="cover-album"'); ?>
                            </a>
                        </div>
                        <div class="box-footer">
                            <label><?= number_format($this->custom->get_count_item('t_fotogaleri', array('id_album' => $row->id_album))); ?>
                                Foto</label>
                            <label class="pull-right"><?= $this->custom->status_aktif($row->aktif); ?></label>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 text-center">
                <?= $paging; ?>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->