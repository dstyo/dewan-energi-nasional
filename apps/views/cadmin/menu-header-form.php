<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Menu
            <small>Header</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/menu_header_table', 'Data Menu'); ?></li>
            <li class="active">Form Menu</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $status = 1;
        $nama_kategori = set_value('nama_kategori');
        $kategori_seo = set_value('kategori_seo');
        $no_urut = set_value('no_urut');
        $gambar_old = set_value('gambar_old');

        if ($edited) {
            $nama_kategori = $res->nama_kategori;
            $kategori_seo = $res->kategori_seo;
            $status = $res->status;
            $no_urut = $res->no_urut;
            $gambar_old = $res->gambar;
        }

        $ckcd1 = 'checked';
        $ckcd0 = '';

        if ($status == 1) {
            $ckcd0 = '';
            $ckcd1 = 'checked';
        }
        if ($status == 0) {
            $ckcd0 = 'checked';
            $ckcd1 = '';
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/menu_header_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Judul<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama_kategori" value="<?= $nama_kategori; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Link<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="kategori_seo" value="<?= $kategori_seo; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">No Urut<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="no_urut" value="<?= $no_urut; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Status Aktif</label>

                    <div class="col-md-6">
                        <label>
                            <input type="radio" name="status" value="1" class="minimal" <?= $ckcd1; ?> />
                            Aktif
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="status" value="0" class="minimal" <?= $ckcd0; ?> />
                            Nonaktif
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Gambar<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small><br/>
                        <small class="text-info">*) Sebaiknya gunakan gamabar yang dengan ukuran wide 24x24.</small><br/>
                        <input type="hidden" name="gambar_old" value="<?= $gambar_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gambar_old != '') {
                            $uriimg = base_url().'uploads/'.$gambar_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px; <?= $imghide; ?>"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/menu_header_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->