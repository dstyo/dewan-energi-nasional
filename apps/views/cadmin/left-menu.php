<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url(); ?>static/images/user.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata('nama_lengkap'); ?></p>
                <a href="#"><i
                        class="fa fa-circle text-success"></i> <?= $this->custom->level_user($this->session->userdata('level')); ?>
                </a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGASI MENU</li>
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> <span>Dashboard</span>'); ?></li>
            <li><?= anchor(cadmin.'/statis_table', '<i class="fa fa-file"></i> <span>Halaman Statis</span>'); ?></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>Halaman Dinamis</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><?= anchor(cadmin.'/kategori_table', '<i class="fa fa-circle-o"></i> Kategori Berita'); ?></li>
                    <li><?= anchor(cadmin.'/berita_table', '<i class="fa fa-circle-o"></i> Berita'); ?></li>
                    <li><?= anchor(cadmin.'/kategori_pengumuman', '<i class="fa fa-circle-o"></i> Kategori Pengumuman'); ?></li>
                    <li><?= anchor(cadmin.'/pengumuman_table', '<i class="fa fa-circle-o"></i> Pengumuman'); ?></li>
                    <li><?= anchor(cadmin.'/tag_table', '<i class="fa fa-circle-o"></i> Tag/Label'); ?></li>
                </ul>
            </li>
            <?php
            if ($this->custom->cek_level(array('A'))) {
                ?>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-image"></i>
                        <span>Media</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                <span>Foto</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><?= anchor(cadmin.'/album_table', '<i class="fa fa-circle-o"></i> Album Foto');
                ?></li>
                                <li><?= anchor(cadmin.'/galeri_table', '<i class="fa fa-circle-o"></i> Galeri Foto');
                ?></li>
                            </ul>
                        </li>
                        <li><?= anchor(cadmin.'/video_table', '<i class="fa fa-circle-o"></i> Video');
                ?></li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                <span>File</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><?= anchor(cadmin.'/filekategori_table', '<i class="fa fa-circle-o"></i> Kategori File');
                ?></li>
                                <li><?= anchor(cadmin.'/file_table', '<i class="fa fa-circle-o"></i> File');
                ?></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li><?= anchor(cadmin.'/agenda_table', '<i class="fa fa-book"></i> <span>Agenda</span>');
                ?></li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-phone"></i> <span>Publikasi</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><?= anchor(cadmin.'/kategori_publikasi', '<i class="fa fa-circle-o"></i> Kategori Publikasi');
                ?></li>
                        <li><?= anchor(cadmin.'/alamat_table', '<i class="fa fa-circle-o"></i> Publikasi');
                ?></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-phone"></i> <span>Hubungi Kami</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><?= anchor(cadmin.'/hubungi_table', '<i class="fa fa-circle-o"></i> Pesan Masuk');
                ?></li>
                        <li><?= anchor(cadmin.'/alamat_table', '<i class="fa fa-circle-o"></i> Alamat & Sosmed');
                ?></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-sitemap"></i>
                        <span>Struktur Organisasi</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <?= anchor(cadmin.'/anggota_table', '<i class="fa fa-circle-o"></i> DEN');
                ?>
                        </li>
                        <li>
                        	<?= anchor(cadmin.'/anggota_table2', '<i class="fa fa-circle-o"></i> SETJEN');
                ?>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span>Pengaturan Web</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><?= anchor(cadmin.'/identitas', '<i class="fa fa-circle-o"></i> Identitas Web');
                ?></li>
                        <li><?= anchor(cadmin.'/menu_table', '<i class="fa fa-circle-o"></i> Menu Web');
                ?></li>
                        <li><?= anchor(cadmin.'/menu_header_table', '<i class="fa fa-circle-o"></i> Menu Header');
                ?></li>
                        <li><?= anchor(cadmin.'/slider_table', '<i class="fa fa-circle-o"></i> Slider');
                ?></li>
                        <li><?= anchor(cadmin.'/banner_table', '<i class="fa fa-circle-o"></i> Banner');
                ?></li>
                        <li><?= anchor(cadmin.'/sikat_table', '<i class="fa fa-circle-o"></i> Situs Terkait');
                ?></li>
                    </ul>
                </li>
                <li><?= anchor(cadmin.'/user_table', '<i class="fa fa-user"></i> <span>Manajemen User</span>');
                ?></li>

            <?php

            }
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
