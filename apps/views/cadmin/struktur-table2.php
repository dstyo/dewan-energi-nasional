<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Struktur Organisasi Setjen</h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Struktur Organisasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Struktur Organisasi</h3>

                <div class="box-tools pull-right">
                    <?= anchor(cadmin.'/struktur_form2', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
                </div>
            </div>

            <div class="box-body">

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Jabatan</th>
                        <th class="hidden-xs">Posisi</th>
                        <th>Status</th>
                        <th style="width: 100px;" class="text-right">Aksi</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $no = 0;
                    foreach ($res->result() as $row):
                        $no++;
                        ?>
                        <tr>
                            <td><?= number_format($no); ?></td>
                            <td><?= $row->nama_organisasi; ?></td>
                            <td class="hidden-xs"><?= $this->custom->posisi_organisasi($row->parent); ?></td>
                            <td><?= $this->custom->status_aktif($row->aktif); ?></td>

                            <td class="text-right">
                                <?php
                                if ($row->aktif == 1) {
                                    echo anchor(cadmin.'/struktur_blok2/'.$row->id_organisasi.'/0', '<i class="fa fa-ban"></i>', 'class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left" title="Nonaktif"');
                                } else {
                                    echo anchor(cadmin.'/struktur_blok2/'.$row->id_organisasi.'/1', '<i class="fa fa-check"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Aktif"');
                                }
                                ?>
                                <?= anchor(cadmin.'/struktur_form2/'.$row->id_organisasi, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"'); ?>
                                <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left"
                                        title="Hapus"
                                        onclick="return confirm_alert('<?= base_url(cadmin.'/struktur_delete2/'.$row->id_organisasi); ?>');">
                                    <i class="fa fa-remove"></i></button>
                            </td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

            <div class="box-footer">
                <button onclick="return preview_struktur();" type="button" class="btn btn-sm"><i class="fa fa-sitemap"></i> Lihat Struktur</button>
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="struktur_preview" class="modal modal-solid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-sitemap"></i> Struktur Organisasi</h4>
            </div>
            <div class="modal-body text-center">

                <iframe style="width:100%; height: 350px; border: 0px;" src="<?=base_url(cadmin.'/strogr2');?>"></iframe>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->