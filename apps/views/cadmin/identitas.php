<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pengaturan
            <small>Identitas Web</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Form Identitas</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = str_replace('%20', ' ', $msg);
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $nama_website = $res->nama_website;
        $meta_deskripsi = $res->meta_deskripsi;
        $meta_keyword = $res->meta_keyword;
        $favicon_old = $res->favicon;
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/identitas', 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Website<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="nama_website" value="<?= $nama_website; ?>" class="form-control"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Meta Deskripsi<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <textarea rows="3" name="meta_deskripsi" class="form-control"><?= $meta_deskripsi; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Meta Keyword<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <textarea rows="3" name="meta_keyword" class="form-control"><?= $meta_keyword; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Favicon</label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus PNG dan besar file max: 2MB.</small><br/>
                        <input type="hidden" name="favicon_old" value="<?= $favicon_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($favicon_old != '') {
                            $uriimg = base_url().'uploads/favicon/'.$favicon_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px;<?= $imghide; ?>"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->