
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Struktur Organisasi DEN <small>Anggota</small></h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/anggota_table', 'Data Anggota DEN'); ?></li>
            <li class="active">Form Anggota DEN</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = '';
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $nama = set_value('nama');
        $id_organisasi = set_value('id_organisasi');
        $keterangan = set_value('keterangan');
        $gambar_old = set_value('gambar_old');

        $jabatan = set_value('jabatan');
        $nama_pejabat = set_value('nama_pejabat');
        $kategori_jabatan = set_value('kategori_jabatan');
        $urutan = set_value('urutan');
        $periode = set_value('periode');
        $alamat_tautan = set_value('alamat_tautan');
        $profil = set_value('profil');

        if ($edited) {
            $jabatan = $res->jabatan;
            $nama_pejabat = $res->nama_pejabat;
            $kategori_jabatan = $res->kategori_jabatan;
            $urutan = $res->urutan;
            $periode = $res->periode;
            $alamat_tautan = $res->alamat_tautan;
            $profil = $res->profil;
        }

        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/anggota_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

               <div class="form-group">
                    <label class="col-md-2 control-label">Jabatan<sup class="text-danger">*</sup> </label>
    
                    <div class="col-md-6">
                        <input type="text" name="jabatan" value="<?= $jabatan; ?>" class="form-control" placeholder=""/>
                    </div>
                </div>
               <div class="form-group">
                    <label class="col-md-2 control-label">Nama Pejabat<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="nama_pejabat" value="<?= $nama_pejabat; ?>" class="form-control" placeholder=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Kategori Jabatan<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <select name="kategori_jabatan" class="form-control">
                            <option value="">Pilih</option>
                            <?php if ($kategori_jabatan == 'PIMPINAN') {
    ?>
                            <option value="PIMPINAN" selected>Pimpinan</option>
                            <option value="AUP">AUP</option>
                            <option value="AUPK">AUPK</option>
                            <?php 
} elseif ($kategori_jabatan == 'AUP') {
    ?>
                            <option value="PIMPINAN">Pimpinan</option>
                            <option value="AUP" selected>AUP</option>
                            <option value="AUPK">AUPK</option>
                            <?php 
} else {
    ?>
                            <option value="PIMPINAN">Pimpinan</option>
                            <option value="AUP">AUP</option>
                            <option value="AUPK" selected>AUPK</option>
                            <?php 
} ?>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Urutan<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="urutan" value="<?= $urutan; ?>" class="form-control" placeholder=""/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Periode<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="periode" value="<?= $periode; ?>" class="form-control" placeholder=""/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Alamat Tautan<sup class="text-danger">*</sup> </label>

                    <div class="col-md-6">
                        <input type="text" name="alamat_tautan" value="<?= $alamat_tautan; ?>" class="form-control" placeholder=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Profil</label>

                    <div class="col-md-10">
                        <textarea  id="mytextarea" name='profil' class="form-control" rows="5" style="width: auto; height: 350px;"><?= $profil; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Foto</label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small><br/>
                        <input type="hidden" name="gambar_old" value="<?= $gambar_old; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gambar_old != '') {
                            $uriimg = base_url().'uploads/foto_anggota_thumbnail/'.$gambar_old;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px;<?= $imghide; ?>"/>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/anggota_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
	tinymce.init({
		selector: "#mytextarea"
	});
</script>