<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>DEN</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url(); ?>uploads/favicon/favicon.png" type="image/x-icon">

    <!-- Bootstrap 3.3.4 -->
    <link href="<?= base_url(); ?>static/cadmin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="<?= base_url(); ?>static/cadmin/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="<?= base_url(); ?>static/cadmin/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="<?= base_url(); ?>static/cadmin/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- daterange picker -->
    <link href="<?= base_url(); ?>static/cadmin/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?= base_url(); ?>static/cadmin/plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="<?= base_url(); ?>static/cadmin/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>
    <!-- Theme style -->
    <link href="<?= base_url(); ?>static/cadmin/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins -->
    <link href="<?= base_url(); ?>static/cadmin/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>
	<!-- WYSIWYG TinyMCE -->
    <script type="text/javascript" src="<?=base_url();?>static/tinymce/tinymce.min.js"></script>
    
    <link href="<?= base_url(); ?>static/cadmin/custom.css" rel="stylesheet" type="text/css"/>


</head>
<body class="skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?=base_url();?>static/images/logo3.png" /></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?=base_url();?>static/images/logo2.png" /></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li data-toggle="tooltip" data-placement="left" title="Update Akun">
                        <?= anchor(cadmin.'/akun', 'Hi, '.$this->session->userdata('username')); ?>
                    </li>
                    <li data-toggle="tooltip" data-placement="left" title="Logout">
                        <?= anchor(cadmin.'/logout', '<i class="fa fa-sign-out"></i>'); ?>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->