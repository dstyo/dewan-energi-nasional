<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Media
            <small>Video</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li class="active">Data Media Video</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Video</h3>

                <div class="box-tools pull-right">
                    <?= anchor(cadmin.'/video_form', '<i class="fa fa-plus-circle"></i> Tambah Baru', 'class="btn btn-success btn-sm"'); ?>
                </div>
            </div>
            <div class="box-body">

                <div class="row" style="margin-bottom: 10px;">
                    <?= form_open(cadmin.'/video_table'); ?>
                    <div class="col-md-6 col-xs-6 text-left">
                        <div class="input-group">
                            <?= $this->custom->limit_form($limit); ?>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-sm pull-right inp-cari"
                                   placeholder="Cari" value="<?= $q; ?>"
                                   onchange="return submit();"/>

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>


                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th class="text-right hidden-xs">Viewer</th>
                        <th class="text-right hidden-xs">Downloader</th>
                        <th style="width: 100px;" class="text-right">Aksi</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    $no = $offset;
                    foreach ($res->result() as $row):
                        $no++;
                        ?>
                        <tr>
                            <td><?= number_format($no); ?></td>
                            <td><?= $row->jdl_video; ?></td>
                            <td class="text-right hidden-xs"><?= number_format($row->viewer); ?></td>
                            <td class="text-right hidden-xs"><?= number_format($row->downloader); ?></td>
                            <td class="text-right">
                                <button class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left"
                                        title="Lihat"
                                        onclick="lihat_vid('<?= base_url('uploads/media_video/'.$row->file_video); ?>', '<?= $this->custom->cut_text($row->jdl_video, 50); ?>');">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <?= anchor(cadmin.'/video_form/'.$row->id_video, '<i class="fa fa-pencil"></i>', 'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Edit"'); ?>
                                <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left"
                                        title="Hapus"
                                        onclick="return confirm_alert('<?= base_url(cadmin.'/video_delete/'.$row->id_video); ?>');">
                                    <i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12 text-right">
                        <?= $paging; ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div id="show_video" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 id="video-title" class="modal-title"></h4>
            </div>
            <div class="modal-body no-padding" id="video-konten">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->