<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Media
            <small>Foto</small>
        </h1>
        <ol class="breadcrumb">
            <li><?= anchor(cadmin.'/home', '<i class="fa fa-dashboard"></i> Dashboard'); ?></li>
            <li><?= anchor(cadmin.'/galeri_table', 'Galeri Foto'); ?></li>
            <li class="active">Form Foto</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php
        $err = validation_errors();
        $c_alert = 'alert-danger';
        $i_alert = '<h4><i class="icon fa fa-warning"></i> Terjadi kesalahan!</h4>';
        if ($msg == 'error') {
            $msg = 'Prosess gagal, silahkan coba kembali.';
        } elseif ($msg == 'success') {
            $msg = 'Proses berhasil.';
            $c_alert = 'alert-success';
            $i_alert = '<h4><i class="icon fa fa-check"></i> Sukses!</h4>';
        } else {
            $msg = str_replace('%20', ' ', $msg);
        }

        if (strlen($msg) > 0 || strlen($err) > 0) {
            echo '<div class="alert '.$c_alert.' alert-dismissable callout">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $i_alert.$err.$msg.
                '</div>';
        }

        $jdl_gallery = set_value('jdl_gallery');
        $gbr_gallery = set_value('gbr_gallery_old');
        $keterangan = set_value('keterangan');
        $id_album = set_value('id_album');

        if ($edited) {
            $jdl_gallery = $res->jdl_gallery;
            $gbr_gallery = $res->gbr_gallery;
            $keterangan = $res->keterangan;
            $id_album = $res->id_album;
        }
        ?>

        <!-- Default box -->
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"><?= $subjudul; ?></h3>
            </div>

            <?= form_open_multipart(cadmin.'/galeri_form/'.$id, 'class="form-horizontal"'); ?>
            <div class="box-body">

                <div class="form-group">
                    <label class="col-md-2 control-label">Album<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <select name="id_album" class="form-control select2">
                            <option value="">Pilih</option>
                            <?php
                            foreach ($album->result() as $rr) {
                                if ($id_album == $rr->id_album) {
                                    echo '<option selected value="'.$rr->id_album.'">'.$rr->jdl_album.'</option>';
                                } else {
                                    echo '<option value="'.$rr->id_album.'">'.$rr->jdl_album.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Judul Foto<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="text" name="jdl_gallery" class="form-control" value="<?= $jdl_gallery; ?>"
                               placeholder="Tulis disini"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Keterangan</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="keterangan" rows="3"><?=$keterangan;?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Foto<sup class="text-danger">*</sup></label>

                    <div class="col-md-6">
                        <input type="file" id="userfile" name="userfile" class="form-control"/>
                        <small class="text-info">*) Tipe gambar harus JPG/PNG/GIF dan besar file max: 2MB.</small><br/>
                        <input type="hidden" name="gbr_gallery_old" value="<?= $gbr_gallery; ?>"/>
                        <?php
                        $imghide = 'display: none;';
                        $uriimg = '';
                        if ($gbr_gallery != '') {
                            $uriimg = base_url().'uploads/media_foto/galeri/'.$gbr_gallery;
                            $imghide = '';
                        }
                        ?>
                        <img id="view_img" src="<?= $uriimg; ?>" style="margin-top:10px;<?= $imghide; ?>"/>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= anchor(cadmin.'/galeri_table', 'Batal', 'class="btn btn-default btn-sm"'); ?>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <!-- /.box-footer-->
            <?= form_close(); ?>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->